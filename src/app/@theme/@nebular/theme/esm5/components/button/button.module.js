/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbButtonComponent } from './button.component';
var NB_BUTTON_COMPONENTS = [
    NbButtonComponent,
];
var NbButtonModule = /** @class */ (function () {
    function NbButtonModule() {
    }
    NbButtonModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
            ],
            declarations: __spreadArrays(NB_BUTTON_COMPONENTS),
            exports: __spreadArrays(NB_BUTTON_COMPONENTS),
        })
    ], NbButtonModule);
    return NbButtonModule;
}());
export { NbButtonModule };
//# sourceMappingURL=button.module.js.map