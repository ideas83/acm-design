/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbOverlayModule } from '../cdk/overlay/overlay.module';
import { NbDialogService } from './dialog.service';
import { NbDialogContainerComponent } from './dialog-container';
import { NB_DIALOG_CONFIG } from './dialog-config';
var NbDialogModule = /** @class */ (function () {
    function NbDialogModule() {
    }
    NbDialogModule_1 = NbDialogModule;
    NbDialogModule.forRoot = function (dialogConfig) {
        if (dialogConfig === void 0) { dialogConfig = {}; }
        return {
            ngModule: NbDialogModule_1,
            providers: [
                NbDialogService,
                { provide: NB_DIALOG_CONFIG, useValue: dialogConfig },
            ],
        };
    };
    NbDialogModule.forChild = function (dialogConfig) {
        if (dialogConfig === void 0) { dialogConfig = {}; }
        return {
            ngModule: NbDialogModule_1,
            providers: [
                NbDialogService,
                { provide: NB_DIALOG_CONFIG, useValue: dialogConfig },
            ],
        };
    };
    var NbDialogModule_1;
    NbDialogModule = NbDialogModule_1 = __decorate([
        NgModule({
            imports: [NbSharedModule, NbOverlayModule],
            declarations: [NbDialogContainerComponent],
            entryComponents: [NbDialogContainerComponent],
        })
    ], NbDialogModule);
    return NbDialogModule;
}());
export { NbDialogModule };
//# sourceMappingURL=dialog.module.js.map