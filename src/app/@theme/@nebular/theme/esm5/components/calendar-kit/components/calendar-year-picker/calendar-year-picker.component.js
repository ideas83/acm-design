/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, Type, } from '@angular/core';
import { NbCalendarSize } from '../../model';
import { NbCalendarYearCellComponent } from './calendar-year-cell.component';
import { NbDateService } from '../../services/date.service';
import { NbCalendarYearModelService } from '../../services/calendar-year-model.service';
var NbCalendarYearPickerComponent = /** @class */ (function () {
    function NbCalendarYearPickerComponent(dateService, yearModelService) {
        this.dateService = dateService;
        this.yearModelService = yearModelService;
        this.cellComponent = NbCalendarYearCellComponent;
        this.size = NbCalendarSize.MEDIUM;
        this.yearChange = new EventEmitter();
    }
    Object.defineProperty(NbCalendarYearPickerComponent.prototype, "_cellComponent", {
        set: function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearPickerComponent.prototype, "large", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarYearPickerComponent.prototype.ngOnChanges = function () {
        this.years = this.yearModelService.getViewYears(this.year);
    };
    NbCalendarYearPickerComponent.prototype.onSelect = function (year) {
        this.yearChange.emit(year);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarYearPickerComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarYearPickerComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarYearPickerComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Function)
    ], NbCalendarYearPickerComponent.prototype, "filter", void 0);
    __decorate([
        Input('cellComponent'),
        __metadata("design:type", Type),
        __metadata("design:paramtypes", [Type])
    ], NbCalendarYearPickerComponent.prototype, "_cellComponent", null);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarYearPickerComponent.prototype, "size", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarYearPickerComponent.prototype, "year", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], NbCalendarYearPickerComponent.prototype, "yearChange", void 0);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbCalendarYearPickerComponent.prototype, "large", null);
    NbCalendarYearPickerComponent = __decorate([
        Component({
            selector: 'nb-calendar-year-picker',
            template: "\n    <nb-calendar-picker\n      [data]=\"years\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [selectedValue]=\"date\"\n      [visibleDate]=\"year\"\n      [cellComponent]=\"cellComponent\"\n      [size]=\"size\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [NbDateService,
            NbCalendarYearModelService])
    ], NbCalendarYearPickerComponent);
    return NbCalendarYearPickerComponent;
}());
export { NbCalendarYearPickerComponent };
//# sourceMappingURL=calendar-year-picker.component.js.map