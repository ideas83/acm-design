/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { range, batch } from '../helpers';
import { NbDateService } from './date.service';
var NbCalendarYearModelService = /** @class */ (function () {
    function NbCalendarYearModelService(dateService) {
        this.dateService = dateService;
        this.yearsInView = 12;
        this.yearsInRow = 4;
    }
    NbCalendarYearModelService.prototype.getYearsInView = function () {
        return this.yearsInView;
    };
    NbCalendarYearModelService.prototype.getYearsInRow = function () {
        return this.yearsInRow;
    };
    NbCalendarYearModelService.prototype.getViewYears = function (viewYear) {
        var _this = this;
        var year = this.dateService.getYear(viewYear);
        var viewStartYear;
        if (year >= 0) {
            viewStartYear = year - (year % this.yearsInView);
        }
        else {
            viewStartYear = year - (year % this.yearsInView + this.yearsInView);
        }
        var years = range(this.yearsInView).map(function (i) { return _this.copyWithYear(viewStartYear + i, viewYear); });
        return batch(years, this.yearsInRow);
    };
    NbCalendarYearModelService.prototype.copyWithYear = function (year, date) {
        return this.dateService.createDate(year, this.dateService.getMonth(date), this.dateService.getDate(date));
    };
    NbCalendarYearModelService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarYearModelService);
    return NbCalendarYearModelService;
}());
export { NbCalendarYearModelService };
//# sourceMappingURL=calendar-year-model.service.js.map