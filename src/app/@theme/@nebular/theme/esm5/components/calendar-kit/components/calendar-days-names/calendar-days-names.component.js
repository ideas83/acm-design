/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input, HostBinding } from '@angular/core';
import { NbCalendarSize } from '../../model';
import { NbDateService } from '../../services/date.service';
var NbCalendarDaysNamesComponent = /** @class */ (function () {
    function NbCalendarDaysNamesComponent(dateService) {
        this.dateService = dateService;
    }
    Object.defineProperty(NbCalendarDaysNamesComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDaysNamesComponent.prototype.ngOnInit = function () {
        var days = this.createDaysNames();
        this.days = this.shiftStartOfWeek(days);
    };
    NbCalendarDaysNamesComponent.prototype.createDaysNames = function () {
        return this.dateService.getDayOfWeekNames()
            .map(this.markIfHoliday);
    };
    NbCalendarDaysNamesComponent.prototype.shiftStartOfWeek = function (days) {
        for (var i = 0; i < this.dateService.getFirstDayOfWeek(); i++) {
            days.push(days.shift());
        }
        return days;
    };
    NbCalendarDaysNamesComponent.prototype.markIfHoliday = function (name, i) {
        return { name: name, isHoliday: i % 6 === 0 };
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarDaysNamesComponent.prototype, "size", void 0);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarDaysNamesComponent.prototype, "isLarge", null);
    NbCalendarDaysNamesComponent = __decorate([
        Component({
            selector: 'nb-calendar-days-names',
            template: "\n    <div class=\"day\" *ngFor=\"let day of days\" [class.holiday]=\"day.isHoliday\">{{ day.name }}</div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush,
            styles: [":host{display:flex;justify-content:space-between}:host .day{display:flex;align-items:center;justify-content:center}\n"]
        }),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarDaysNamesComponent);
    return NbCalendarDaysNamesComponent;
}());
export { NbCalendarDaysNamesComponent };
//# sourceMappingURL=calendar-days-names.component.js.map