/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, Type, HostBinding } from '@angular/core';
import { NbCalendarSize } from '../../model';
var NbCalendarPickerComponent = /** @class */ (function () {
    function NbCalendarPickerComponent() {
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter();
    }
    Object.defineProperty(NbCalendarPickerComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], NbCalendarPickerComponent.prototype, "data", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarPickerComponent.prototype, "visibleDate", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarPickerComponent.prototype, "selectedValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Type)
    ], NbCalendarPickerComponent.prototype, "cellComponent", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarPickerComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarPickerComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Function)
    ], NbCalendarPickerComponent.prototype, "filter", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarPickerComponent.prototype, "size", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], NbCalendarPickerComponent.prototype, "select", void 0);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarPickerComponent.prototype, "isLarge", null);
    NbCalendarPickerComponent = __decorate([
        Component({
            selector: 'nb-calendar-picker',
            template: "\n    <nb-calendar-picker-row\n      *ngFor=\"let row of data\"\n      [row]=\"row\"\n      [visibleDate]=\"visibleDate\"\n      [selectedValue]=\"selectedValue\"\n      [component]=\"cellComponent\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [size]=\"size\"\n      (select)=\"select.emit($event)\">\n    </nb-calendar-picker-row>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], NbCalendarPickerComponent);
    return NbCalendarPickerComponent;
}());
export { NbCalendarPickerComponent };
//# sourceMappingURL=calendar-picker.component.js.map