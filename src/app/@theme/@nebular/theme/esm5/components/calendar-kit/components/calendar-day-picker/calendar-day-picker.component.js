/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, Type, } from '@angular/core';
import { NbCalendarMonthModelService } from '../../services/calendar-month-model.service';
import { NbCalendarDayCellComponent } from './calendar-day-cell.component';
import { NbCalendarSize } from '../../model';
import { convertToBoolProperty } from '../../../helpers';
/**
 * Provides capability pick days.
 * */
var NbCalendarDayPickerComponent = /** @class */ (function () {
    function NbCalendarDayPickerComponent(monthModel) {
        this.monthModel = monthModel;
        /**
         * Defines if we should render previous and next months
         * in the current month view.
         * */
        this.boundingMonths = true;
        this.cellComponent = NbCalendarDayCellComponent;
        /**
         * Size of the component.
         * Can be 'medium' which is default or 'large'.
         * */
        this.size = NbCalendarSize.MEDIUM;
        this._showWeekNumber = false;
        /**
         * Fires newly selected date.
         * */
        this.dateChange = new EventEmitter();
    }
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "setCellComponent", {
        /**
         * Custom day cell component. Have to implement `NbCalendarCell` interface.
         * */
        set: function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "showWeekNumber", {
        /**
         * Determines should we show week numbers column.
         * False by default.
         * */
        get: function () {
            return this._showWeekNumber;
        },
        set: function (value) {
            this._showWeekNumber = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "large", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDayPickerComponent.prototype.ngOnChanges = function (_a) {
        var visibleDate = _a.visibleDate;
        if (visibleDate) {
            this.weeks = this.monthModel.createDaysGrid(this.visibleDate, this.boundingMonths);
        }
    };
    NbCalendarDayPickerComponent.prototype.onSelect = function (day) {
        this.dateChange.emit(day);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarDayPickerComponent.prototype, "visibleDate", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], NbCalendarDayPickerComponent.prototype, "boundingMonths", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarDayPickerComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarDayPickerComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Function)
    ], NbCalendarDayPickerComponent.prototype, "filter", void 0);
    __decorate([
        Input('cellComponent'),
        __metadata("design:type", Type),
        __metadata("design:paramtypes", [Type])
    ], NbCalendarDayPickerComponent.prototype, "setCellComponent", null);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarDayPickerComponent.prototype, "size", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarDayPickerComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], NbCalendarDayPickerComponent.prototype, "showWeekNumber", null);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarDayPickerComponent.prototype, "weekNumberSymbol", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], NbCalendarDayPickerComponent.prototype, "dateChange", void 0);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbCalendarDayPickerComponent.prototype, "large", null);
    NbCalendarDayPickerComponent = __decorate([
        Component({
            selector: 'nb-calendar-day-picker',
            template: "\n    <nb-calendar-week-numbers *ngIf=\"showWeekNumber\"\n                              [weeks]=\"weeks\"\n                              [size]=\"size\"\n                              [weekNumberSymbol]=\"weekNumberSymbol\">\n    </nb-calendar-week-numbers>\n    <div class=\"days-container\">\n      <nb-calendar-days-names [size]=\"size\"></nb-calendar-days-names>\n      <nb-calendar-picker\n          [data]=\"weeks\"\n          [visibleDate]=\"visibleDate\"\n          [selectedValue]=\"date\"\n          [cellComponent]=\"cellComponent\"\n          [min]=\"min\"\n          [max]=\"max\"\n          [filter]=\"filter\"\n          [size]=\"size\"\n          (select)=\"onSelect($event)\">\n      </nb-calendar-picker>\n    </div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush,
            styles: [":host{display:flex}.days-container{width:100%}\n"]
        }),
        __metadata("design:paramtypes", [NbCalendarMonthModelService])
    ], NbCalendarDayPickerComponent);
    return NbCalendarDayPickerComponent;
}());
export { NbCalendarDayPickerComponent };
//# sourceMappingURL=calendar-day-picker.component.js.map