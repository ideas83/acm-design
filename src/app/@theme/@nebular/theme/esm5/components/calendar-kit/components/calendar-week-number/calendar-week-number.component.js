/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata, __spreadArrays } from "tslib";
import { Component, Input, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { NbDateService } from '../../services/date.service';
import { NbCalendarSize } from '../../model';
var NbCalendarWeekNumberComponent = /** @class */ (function () {
    function NbCalendarWeekNumberComponent(dateService) {
        this.dateService = dateService;
    }
    Object.defineProperty(NbCalendarWeekNumberComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarWeekNumberComponent.prototype.ngOnChanges = function (changes) {
        if (changes.weeks) {
            this.weekNumbers = this.getWeeks();
        }
    };
    NbCalendarWeekNumberComponent.prototype.getWeeks = function () {
        var _this = this;
        return this.weeks.map(function (week) {
            // Find last defined day as week could contain null days in case
            // boundingMonth set to false
            var lastDay = __spreadArrays(week).reverse().find(function (day) { return !!day; });
            // Use last day of the week to determine week number.
            // This way weeks which span between sibling years is marked first
            return _this.dateService.getWeekNumber(lastDay);
        });
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], NbCalendarWeekNumberComponent.prototype, "weeks", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarWeekNumberComponent.prototype, "size", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarWeekNumberComponent.prototype, "weekNumberSymbol", void 0);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbCalendarWeekNumberComponent.prototype, "isLarge", null);
    NbCalendarWeekNumberComponent = __decorate([
        Component({
            selector: 'nb-calendar-week-numbers',
            template: "\n    <div class=\"sign-container\">\n      <div class=\"sign\">{{ weekNumberSymbol }}</div>\n    </div>\n    <div class=\"week-number\" *ngFor=\"let weekNumber of weekNumbers\">{{ weekNumber }}</div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush,
            styles: [":host{display:flex;flex-direction:column}\n"]
        }),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarWeekNumberComponent);
    return NbCalendarWeekNumberComponent;
}());
export { NbCalendarWeekNumberComponent };
//# sourceMappingURL=calendar-week-number.component.js.map