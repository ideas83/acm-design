/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { TranslationWidth } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { NbCalendarViewMode } from '../../model';
import { NbCalendarYearModelService } from '../../services/calendar-year-model.service';
import { NbDateService } from '../../services/date.service';
var NbCalendarViewModeComponent = /** @class */ (function () {
    function NbCalendarViewModeComponent(dateService, yearModelService) {
        this.dateService = dateService;
        this.yearModelService = yearModelService;
        this.viewMode = NbCalendarViewMode.DATE;
        this.changeMode = new EventEmitter(true);
    }
    NbCalendarViewModeComponent.prototype.getText = function () {
        if (!this.date) {
            return '';
        }
        switch (this.viewMode) {
            case NbCalendarViewMode.DATE: {
                var month = this.dateService.getMonthName(this.date, TranslationWidth.Wide);
                var year = this.dateService.getYear(this.date);
                return month + " " + year;
            }
            case NbCalendarViewMode.MONTH:
                return "" + this.dateService.getYear(this.date);
            case NbCalendarViewMode.YEAR:
                return this.getFirstYear() + " - " + this.getLastYear();
        }
    };
    NbCalendarViewModeComponent.prototype.getIcon = function () {
        if (this.viewMode === NbCalendarViewMode.DATE) {
            return 'chevron-down-outline';
        }
        return 'chevron-up-outline';
    };
    NbCalendarViewModeComponent.prototype.getFirstYear = function () {
        var years = this.yearModelService.getViewYears(this.date);
        return this.dateService.getYear(years[0][0]).toString();
    };
    NbCalendarViewModeComponent.prototype.getLastYear = function () {
        var years = this.yearModelService.getViewYears(this.date);
        var lastRow = years[years.length - 1];
        var lastYear = lastRow[lastRow.length - 1];
        return this.dateService.getYear(lastYear).toString();
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarViewModeComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarViewModeComponent.prototype, "viewMode", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], NbCalendarViewModeComponent.prototype, "changeMode", void 0);
    NbCalendarViewModeComponent = __decorate([
        Component({
            selector: 'nb-calendar-view-mode',
            template: "\n    <button nbButton (click)=\"changeMode.emit()\" ghost status=\"basic\">\n      {{ getText() }}\n      <nb-icon [icon]=\"getIcon()\" pack=\"nebular-essentials\"></nb-icon>\n    </button>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [NbDateService,
            NbCalendarYearModelService])
    ], NbCalendarViewModeComponent);
    return NbCalendarViewModeComponent;
}());
export { NbCalendarViewModeComponent };
//# sourceMappingURL=calendar-view-mode.component.js.map