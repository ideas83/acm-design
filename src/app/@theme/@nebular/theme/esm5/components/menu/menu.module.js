/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbMenuComponent, NbMenuItemComponent } from './menu.component';
import { NbMenuService, NbMenuInternalService } from './menu.service';
import { NbIconModule } from '../icon/icon.module';
var nbMenuComponents = [NbMenuComponent, NbMenuItemComponent];
var NB_MENU_PROVIDERS = [NbMenuService, NbMenuInternalService];
var NbMenuModule = /** @class */ (function () {
    function NbMenuModule() {
    }
    NbMenuModule_1 = NbMenuModule;
    NbMenuModule.forRoot = function () {
        return {
            ngModule: NbMenuModule_1,
            providers: __spreadArrays(NB_MENU_PROVIDERS),
        };
    };
    var NbMenuModule_1;
    NbMenuModule = NbMenuModule_1 = __decorate([
        NgModule({
            imports: [NbSharedModule, NbIconModule],
            declarations: __spreadArrays(nbMenuComponents),
            exports: __spreadArrays(nbMenuComponents),
        })
    ], NbMenuModule);
    return NbMenuModule;
}());
export { NbMenuModule };
//# sourceMappingURL=menu.module.js.map