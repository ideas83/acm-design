/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
/*
 * Class used as injection token to provide form element.
 **/
var NbFormFieldControl = /** @class */ (function () {
    function NbFormFieldControl() {
    }
    NbFormFieldControl = __decorate([
        Injectable()
    ], NbFormFieldControl);
    return NbFormFieldControl;
}());
export { NbFormFieldControl };
/*
 * Optional config to be provided on NbFormFieldControl to alter default settings.
 **/
var NbFormFieldControlConfig = /** @class */ (function () {
    function NbFormFieldControlConfig() {
        this.supportsPrefix = true;
        this.supportsSuffix = true;
    }
    NbFormFieldControlConfig = __decorate([
        Injectable()
    ], NbFormFieldControlConfig);
    return NbFormFieldControlConfig;
}());
export { NbFormFieldControlConfig };
//# sourceMappingURL=form-field-control.js.map