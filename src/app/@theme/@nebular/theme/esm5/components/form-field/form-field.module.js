/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbFormFieldComponent } from './form-field.component';
import { NbPrefixDirective } from './prefix.directive';
import { NbSuffixDirective } from './suffix.directive';
var COMPONENTS = [
    NbFormFieldComponent,
    NbPrefixDirective,
    NbSuffixDirective,
];
var NbFormFieldModule = /** @class */ (function () {
    function NbFormFieldModule() {
    }
    NbFormFieldModule = __decorate([
        NgModule({
            imports: [CommonModule],
            declarations: __spreadArrays(COMPONENTS),
            exports: __spreadArrays(COMPONENTS),
        })
    ], NbFormFieldModule);
    return NbFormFieldModule;
}());
export { NbFormFieldModule };
//# sourceMappingURL=form-field.module.js.map