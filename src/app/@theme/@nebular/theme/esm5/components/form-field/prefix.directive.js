import { __decorate } from "tslib";
import { Directive } from '@angular/core';
var NbPrefixDirective = /** @class */ (function () {
    function NbPrefixDirective() {
    }
    NbPrefixDirective = __decorate([
        Directive({
            selector: '[nbPrefix]',
        })
    ], NbPrefixDirective);
    return NbPrefixDirective;
}());
export { NbPrefixDirective };
//# sourceMappingURL=prefix.directive.js.map