import { __decorate } from "tslib";
import { Directive } from '@angular/core';
var NbSuffixDirective = /** @class */ (function () {
    function NbSuffixDirective() {
    }
    NbSuffixDirective = __decorate([
        Directive({
            selector: '[nbSuffix]',
        })
    ], NbSuffixDirective);
    return NbSuffixDirective;
}());
export { NbSuffixDirective };
//# sourceMappingURL=suffix.directive.js.map