/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Component, Input } from '@angular/core';
import { NbPositionedContainer } from '../cdk/overlay/overlay-container';
/**
 * Context menu component used as content within NbContextMenuDirective.
 *
 * @styles
 *
 * context-menu-background-color:
 * context-menu-border-color:
 * context-menu-border-style:
 * context-menu-border-width:
 * context-menu-border-radius:
 * context-menu-text-align:
 * context-menu-min-width:
 * context-menu-max-width:
 * context-menu-shadow:
 * */
var NbContextMenuComponent = /** @class */ (function (_super) {
    __extends(NbContextMenuComponent, _super);
    function NbContextMenuComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.items = [];
        _this.context = { items: [] };
        return _this;
    }
    /**
     * The method is empty since we don't need to do anything additionally
     * render is handled by change detection
     */
    NbContextMenuComponent.prototype.renderContent = function () { };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], NbContextMenuComponent.prototype, "items", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbContextMenuComponent.prototype, "tag", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbContextMenuComponent.prototype, "context", void 0);
    NbContextMenuComponent = __decorate([
        Component({
            selector: 'nb-context-menu',
            template: "\n    <nb-menu class=\"context-menu\" [items]=\"context.items\" [tag]=\"context.tag\"></nb-menu>\n  "
        })
    ], NbContextMenuComponent);
    return NbContextMenuComponent;
}(NbPositionedContainer));
export { NbContextMenuComponent };
//# sourceMappingURL=context-menu.component.js.map