/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbRouteTabsetComponent } from './route-tabset.component';
import { NbIconModule } from '../icon/icon.module';
var NbRouteTabsetModule = /** @class */ (function () {
    function NbRouteTabsetModule() {
    }
    NbRouteTabsetModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
                NbIconModule,
            ],
            declarations: [
                NbRouteTabsetComponent,
            ],
            exports: [
                NbRouteTabsetComponent,
            ],
        })
    ], NbRouteTabsetModule);
    return NbRouteTabsetModule;
}());
export { NbRouteTabsetModule };
//# sourceMappingURL=route-tabset.module.js.map