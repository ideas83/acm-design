/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbBadgeComponent } from './badge.component';
var NbBadgeModule = /** @class */ (function () {
    function NbBadgeModule() {
    }
    NbBadgeModule = __decorate([
        NgModule({
            exports: [NbBadgeComponent],
            declarations: [NbBadgeComponent],
        })
    ], NbBadgeModule);
    return NbBadgeModule;
}());
export { NbBadgeModule };
//# sourceMappingURL=badge.module.js.map