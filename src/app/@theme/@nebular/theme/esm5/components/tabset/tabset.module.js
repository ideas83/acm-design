/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbTabsetComponent, NbTabComponent } from './tabset.component';
import { NbBadgeModule } from '../badge/badge.module';
import { NbIconModule } from '../icon/icon.module';
var NB_TABSET_COMPONENTS = [
    NbTabsetComponent,
    NbTabComponent,
];
var NbTabsetModule = /** @class */ (function () {
    function NbTabsetModule() {
    }
    NbTabsetModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
                NbBadgeModule,
                NbIconModule,
            ],
            declarations: __spreadArrays(NB_TABSET_COMPONENTS),
            exports: __spreadArrays(NB_TABSET_COMPONENTS),
        })
    ], NbTabsetModule);
    return NbTabsetModule;
}());
export { NbTabsetModule };
//# sourceMappingURL=tabset.module.js.map