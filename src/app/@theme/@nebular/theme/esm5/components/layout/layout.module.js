import { __decorate, __spreadArrays } from "tslib";
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbLayoutComponent, NbLayoutColumnComponent, NbLayoutFooterComponent, NbLayoutHeaderComponent, } from './layout.component';
import { NbRestoreScrollTopHelper } from './restore-scroll-top.service';
var NB_LAYOUT_COMPONENTS = [
    NbLayoutComponent,
    NbLayoutColumnComponent,
    NbLayoutFooterComponent,
    NbLayoutHeaderComponent,
];
var NbLayoutModule = /** @class */ (function () {
    function NbLayoutModule() {
    }
    NbLayoutModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
            ],
            declarations: __spreadArrays(NB_LAYOUT_COMPONENTS),
            providers: [
                NbRestoreScrollTopHelper,
            ],
            exports: __spreadArrays(NB_LAYOUT_COMPONENTS),
        })
    ], NbLayoutModule);
    return NbLayoutModule;
}());
export { NbLayoutModule };
//# sourceMappingURL=layout.module.js.map