/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbBaseCalendarRangeCell = /** @class */ (function () {
    function NbBaseCalendarRangeCell() {
    }
    Object.defineProperty(NbBaseCalendarRangeCell.prototype, "hasRange", {
        get: function () {
            return !!(this.selectedValue && this.selectedValue.start && this.selectedValue.end);
        },
        enumerable: true,
        configurable: true
    });
    return NbBaseCalendarRangeCell;
}());
export { NbBaseCalendarRangeCell };
//# sourceMappingURL=base-calendar-range-cell.js.map