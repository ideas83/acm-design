/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
var NbCalendarRangeDayCellComponent = /** @class */ (function (_super) {
    __extends(NbCalendarRangeDayCellComponent, _super);
    function NbCalendarRangeDayCellComponent(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.size = NbCalendarSize.MEDIUM;
        _this.select = new EventEmitter(true);
        _this.rangeCellClass = true;
        _this.dayCellClass = true;
        return _this;
    }
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "inRange", {
        get: function () {
            if (this.date && this.hasRange) {
                return this.isInRange(this.date, this.selectedValue);
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "start", {
        get: function () {
            return this.date && this.hasRange && this.dateService.isSameDay(this.date, this.selectedValue.start);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "end", {
        get: function () {
            return this.date && this.hasRange && this.dateService.isSameDay(this.date, this.selectedValue.end);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "today", {
        get: function () {
            return this.date && this.dateService.isSameDay(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "boundingMonth", {
        get: function () {
            return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "selected", {
        get: function () {
            if (this.inRange) {
                return true;
            }
            if (this.selectedValue) {
                return this.dateService.isSameDay(this.date, this.selectedValue.start);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "empty", {
        get: function () {
            return !this.date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "day", {
        get: function () {
            return this.date && this.dateService.getDate(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeDayCellComponent.prototype.onClick = function () {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeDayCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    };
    NbCalendarRangeDayCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    };
    NbCalendarRangeDayCellComponent.prototype.dontFitFilter = function () {
        return this.date && this.filter && !this.filter(this.date);
    };
    NbCalendarRangeDayCellComponent.prototype.isInRange = function (date, _a) {
        var start = _a.start, end = _a.end;
        var isGreaterThanStart = this.dateService.compareDates(this.date, start) >= 0;
        var isLessThanEnd = this.dateService.compareDates(this.date, end) <= 0;
        return isGreaterThanStart && isLessThanEnd;
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "selectedValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "visibleDate", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Function)
    ], NbCalendarRangeDayCellComponent.prototype, "filter", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarRangeDayCellComponent.prototype, "size", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], NbCalendarRangeDayCellComponent.prototype, "select", void 0);
    __decorate([
        HostBinding('class.in-range'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "inRange", null);
    __decorate([
        HostBinding('class.start'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "start", null);
    __decorate([
        HostBinding('class.end'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "end", null);
    __decorate([
        HostBinding('class.range-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "rangeCellClass", void 0);
    __decorate([
        HostBinding('class.day-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeDayCellComponent.prototype, "dayCellClass", void 0);
    __decorate([
        HostBinding('class.today'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "today", null);
    __decorate([
        HostBinding('class.bounding-month'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "boundingMonth", null);
    __decorate([
        HostBinding('class.selected'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "selected", null);
    __decorate([
        HostBinding('class.empty'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "empty", null);
    __decorate([
        HostBinding('class.disabled'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "disabled", null);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeDayCellComponent.prototype, "isLarge", null);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbCalendarRangeDayCellComponent.prototype, "onClick", null);
    NbCalendarRangeDayCellComponent = __decorate([
        Component({
            selector: 'nb-calendar-range-day-cell',
            template: "\n    <div class=\"cell-content\">{{ day }}</div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarRangeDayCellComponent);
    return NbCalendarRangeDayCellComponent;
}(NbBaseCalendarRangeCell));
export { NbCalendarRangeDayCellComponent };
//# sourceMappingURL=calendar-range-day-cell.component.js.map