/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
var NbCalendarRangeYearCellComponent = /** @class */ (function (_super) {
    __extends(NbCalendarRangeYearCellComponent, _super);
    function NbCalendarRangeYearCellComponent(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.size = NbCalendarSize.MEDIUM;
        _this.select = new EventEmitter(true);
        _this.yearCellClass = true;
        _this.rangeCellClass = true;
        return _this;
    }
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "inRange", {
        get: function () {
            return this.hasRange && this.isInRange(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "rangeStart", {
        get: function () {
            return this.hasRange && this.dateService.isSameYear(this.date, this.selectedValue.start);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "rangeEnd", {
        get: function () {
            return this.hasRange && this.dateService.isSameYear(this.date, this.selectedValue.end);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "selected", {
        get: function () {
            if (this.inRange) {
                return true;
            }
            if (this.selectedValue) {
                return this.dateService.isSameYear(this.date, this.selectedValue.start);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameYear(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "year", {
        get: function () {
            return this.dateService.getYear(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeYearCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    };
    NbCalendarRangeYearCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    };
    NbCalendarRangeYearCellComponent.prototype.yearStart = function () {
        return this.dateService.getYearStart(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.yearEnd = function () {
        return this.dateService.getYearEnd(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.isInRange = function (date, _a) {
        var start = _a.start, end = _a.end;
        if (start && end) {
            var cellYear = this.dateService.getYear(date);
            var startYear = this.dateService.getYear(start);
            var endYear = this.dateService.getYear(end);
            return cellYear >= startYear && cellYear <= endYear;
        }
        return this.dateService.isSameYear(date, start);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "selectedValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarRangeYearCellComponent.prototype, "size", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], NbCalendarRangeYearCellComponent.prototype, "select", void 0);
    __decorate([
        HostBinding('class.in-range'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "inRange", null);
    __decorate([
        HostBinding('class.start'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "rangeStart", null);
    __decorate([
        HostBinding('class.end'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "rangeEnd", null);
    __decorate([
        HostBinding('class.selected'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "selected", null);
    __decorate([
        HostBinding('class.today'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "today", null);
    __decorate([
        HostBinding('class.disabled'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "disabled", null);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeYearCellComponent.prototype, "isLarge", null);
    __decorate([
        HostBinding('class.year-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "yearCellClass", void 0);
    __decorate([
        HostBinding('class.range-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeYearCellComponent.prototype, "rangeCellClass", void 0);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbCalendarRangeYearCellComponent.prototype, "onClick", null);
    NbCalendarRangeYearCellComponent = __decorate([
        Component({
            selector: 'nb-calendar-range-year-cell',
            template: "\n    <div class=\"cell-content\">\n      {{ year }}\n    </div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarRangeYearCellComponent);
    return NbCalendarRangeYearCellComponent;
}(NbBaseCalendarRangeCell));
export { NbCalendarRangeYearCellComponent };
//# sourceMappingURL=calendar-range-year-cell.component.js.map