/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
var NbCalendarRangeMonthCellComponent = /** @class */ (function (_super) {
    __extends(NbCalendarRangeMonthCellComponent, _super);
    function NbCalendarRangeMonthCellComponent(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.size = NbCalendarSize.MEDIUM;
        _this.select = new EventEmitter(true);
        _this.monthCellClass = true;
        _this.rangeCellClass = true;
        return _this;
    }
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "month", {
        get: function () {
            return this.dateService.getMonthName(this.date);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "selected", {
        get: function () {
            if (this.inRange) {
                return true;
            }
            if (this.selectedValue) {
                return this.dateService.isSameMonth(this.date, this.selectedValue.start);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "inRange", {
        get: function () {
            if (this.hasRange) {
                return this.isInRage(this.date, this.selectedValue);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "rangeStart", {
        get: function () {
            if (this.hasRange) {
                return this.dateService.isSameMonth(this.date, this.selectedValue.start);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "rangeEnd", {
        get: function () {
            if (this.hasRange) {
                return this.dateService.isSameMonth(this.date, this.selectedValue.end);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameMonthSafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeMonthCellComponent.prototype, "isLarge", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeMonthCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeMonthCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.monthEnd(), this.min) < 0;
    };
    NbCalendarRangeMonthCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.monthStart(), this.max) > 0;
    };
    NbCalendarRangeMonthCellComponent.prototype.monthStart = function () {
        return this.dateService.getMonthStart(this.date);
    };
    NbCalendarRangeMonthCellComponent.prototype.monthEnd = function () {
        return this.dateService.getMonthEnd(this.date);
    };
    NbCalendarRangeMonthCellComponent.prototype.isInRage = function (date, range) {
        if (range.start && range.end) {
            var cellDate = this.dateService.getMonthStart(date);
            var start = this.dateService.getMonthStart(range.start);
            var end = this.dateService.getMonthStart(range.end);
            var isGreaterThanStart = this.dateService.compareDates(cellDate, start) >= 0;
            var isLessThanEnd = this.dateService.compareDates(cellDate, end) <= 0;
            return isGreaterThanStart && isLessThanEnd;
        }
        return this.dateService.isSameMonth(date, range.start);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "visibleDate", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "selectedValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "min", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbCalendarRangeMonthCellComponent.prototype, "size", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], NbCalendarRangeMonthCellComponent.prototype, "select", void 0);
    __decorate([
        HostBinding('class.month-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "monthCellClass", void 0);
    __decorate([
        HostBinding('class.range-cell'),
        __metadata("design:type", Object)
    ], NbCalendarRangeMonthCellComponent.prototype, "rangeCellClass", void 0);
    __decorate([
        HostBinding('class.selected'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "selected", null);
    __decorate([
        HostBinding('class.in-range'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "inRange", null);
    __decorate([
        HostBinding('class.start'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "rangeStart", null);
    __decorate([
        HostBinding('class.end'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "rangeEnd", null);
    __decorate([
        HostBinding('class.today'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "today", null);
    __decorate([
        HostBinding('class.disabled'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "disabled", null);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbCalendarRangeMonthCellComponent.prototype, "isLarge", null);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbCalendarRangeMonthCellComponent.prototype, "onClick", null);
    NbCalendarRangeMonthCellComponent = __decorate([
        Component({
            selector: 'nb-calendar-range-month-cell',
            template: "\n    <div class=\"cell-content\">\n      {{ month }}\n    </div>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [NbDateService])
    ], NbCalendarRangeMonthCellComponent);
    return NbCalendarRangeMonthCellComponent;
}(NbBaseCalendarRangeCell));
export { NbCalendarRangeMonthCellComponent };
//# sourceMappingURL=calendar-range-month-cell.component.js.map