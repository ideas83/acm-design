import { __decorate, __metadata } from "tslib";
import { NbStepperComponent } from './stepper.component';
import { Directive, HostBinding, HostListener, Input } from '@angular/core';
var NbStepperNextDirective = /** @class */ (function () {
    function NbStepperNextDirective(stepper) {
        this.stepper = stepper;
        this.type = 'submit';
    }
    NbStepperNextDirective.prototype.onClick = function () {
        this.stepper.next();
    };
    __decorate([
        Input(), HostBinding('attr.type'),
        __metadata("design:type", String)
    ], NbStepperNextDirective.prototype, "type", void 0);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbStepperNextDirective.prototype, "onClick", null);
    NbStepperNextDirective = __decorate([
        Directive({
            selector: 'button[nbStepperNext]',
        }),
        __metadata("design:paramtypes", [NbStepperComponent])
    ], NbStepperNextDirective);
    return NbStepperNextDirective;
}());
export { NbStepperNextDirective };
var NbStepperPreviousDirective = /** @class */ (function () {
    function NbStepperPreviousDirective(stepper) {
        this.stepper = stepper;
        this.type = 'button';
    }
    NbStepperPreviousDirective.prototype.onClick = function () {
        this.stepper.previous();
    };
    __decorate([
        Input(), HostBinding('attr.type'),
        __metadata("design:type", String)
    ], NbStepperPreviousDirective.prototype, "type", void 0);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbStepperPreviousDirective.prototype, "onClick", null);
    NbStepperPreviousDirective = __decorate([
        Directive({
            selector: 'button[nbStepperPrevious]',
        }),
        __metadata("design:paramtypes", [NbStepperComponent])
    ], NbStepperPreviousDirective);
    return NbStepperPreviousDirective;
}());
export { NbStepperPreviousDirective };
//# sourceMappingURL=stepper-button.directive.js.map