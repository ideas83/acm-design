/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbActionComponent, NbActionsComponent } from './actions.component';
import { NbBadgeModule } from '../badge/badge.module';
import { NbIconModule } from '../icon/icon.module';
var NB_ACTIONS_COMPONENTS = [
    NbActionComponent,
    NbActionsComponent,
];
var NbActionsModule = /** @class */ (function () {
    function NbActionsModule() {
    }
    NbActionsModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
                NbBadgeModule,
                NbIconModule,
            ],
            declarations: __spreadArrays(NB_ACTIONS_COMPONENTS),
            exports: __spreadArrays(NB_ACTIONS_COMPONENTS),
        })
    ], NbActionsModule);
    return NbActionsModule;
}());
export { NbActionsModule };
//# sourceMappingURL=actions.module.js.map