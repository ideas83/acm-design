/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbOverlayModule } from '../cdk/overlay/overlay.module';
import { NbCardModule } from '../card/card.module';
import { NbAutocompleteComponent } from './autocomplete.component';
import { NbAutocompleteDirective } from './autocomplete.directive';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NbOptionModule } from '../option/option-list.module';
var NB_AUTOCOMPLETE_COMPONENTS = [
    NbAutocompleteComponent,
    NbAutocompleteDirective,
];
var NbAutocompleteModule = /** @class */ (function () {
    function NbAutocompleteModule() {
    }
    NbAutocompleteModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                NbOverlayModule,
                NbCardModule,
                NbOptionModule,
            ],
            exports: __spreadArrays(NB_AUTOCOMPLETE_COMPONENTS, [
                NbOptionModule,
            ]),
            declarations: __spreadArrays(NB_AUTOCOMPLETE_COMPONENTS),
        })
    ], NbAutocompleteModule);
    return NbAutocompleteModule;
}());
export { NbAutocompleteModule };
//# sourceMappingURL=autocomplete.module.js.map