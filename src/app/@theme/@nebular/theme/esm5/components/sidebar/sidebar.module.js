/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbSidebarComponent, NbSidebarFooterComponent, NbSidebarHeaderComponent, } from './sidebar.component';
import { NbSidebarService } from './sidebar.service';
var NB_SIDEBAR_COMPONENTS = [
    NbSidebarComponent,
    NbSidebarFooterComponent,
    NbSidebarHeaderComponent,
];
var NB_SIDEBAR_PROVIDERS = [
    NbSidebarService,
];
var NbSidebarModule = /** @class */ (function () {
    function NbSidebarModule() {
    }
    NbSidebarModule_1 = NbSidebarModule;
    NbSidebarModule.forRoot = function () {
        return {
            ngModule: NbSidebarModule_1,
            providers: __spreadArrays(NB_SIDEBAR_PROVIDERS),
        };
    };
    var NbSidebarModule_1;
    NbSidebarModule = NbSidebarModule_1 = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
            ],
            declarations: __spreadArrays(NB_SIDEBAR_COMPONENTS),
            exports: __spreadArrays(NB_SIDEBAR_COMPONENTS),
        })
    ], NbSidebarModule);
    return NbSidebarModule;
}());
export { NbSidebarModule };
//# sourceMappingURL=sidebar.module.js.map