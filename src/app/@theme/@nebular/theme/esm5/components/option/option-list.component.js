import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input, HostBinding } from '@angular/core';
import { NbPosition } from '../cdk/overlay/overlay-position';
/**
 * The `NbOptionListComponent` is container component for `NbOptionGroupComponent` and`NbOptionComponent` list.
 *
 * @styles
 *
 * option-list-max-height:
 * option-list-shadow:
 * option-list-background-color:
 * option-list-border-style:
 * option-list-border-width:
 * option-list-border-color:
 * option-list-border-radius:
 * option-list-adjacent-border-color:
 * option-list-adjacent-border-style:
 * option-list-adjacent-border-width:
 * */
var NbOptionListComponent = /** @class */ (function () {
    function NbOptionListComponent() {
        this.size = 'medium';
    }
    Object.defineProperty(NbOptionListComponent.prototype, "positionTop", {
        get: function () {
            return this.position === NbPosition.TOP;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "positionBottom", {
        get: function () {
            return this.position === NbPosition.BOTTOM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "sizeTiny", {
        get: function () {
            return this.size === 'tiny';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "sizeSmall", {
        get: function () {
            return this.size === 'small';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "sizeMedium", {
        get: function () {
            return this.size === 'medium';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "sizeLarge", {
        get: function () {
            return this.size === 'large';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionListComponent.prototype, "sizeGiant", {
        get: function () {
            return this.size === 'giant';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbOptionListComponent.prototype, "size", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbOptionListComponent.prototype, "position", void 0);
    __decorate([
        HostBinding('class.position-top'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "positionTop", null);
    __decorate([
        HostBinding('class.position-bottom'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "positionBottom", null);
    __decorate([
        HostBinding('class.size-tiny'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "sizeTiny", null);
    __decorate([
        HostBinding('class.size-small'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "sizeSmall", null);
    __decorate([
        HostBinding('class.size-medium'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "sizeMedium", null);
    __decorate([
        HostBinding('class.size-large'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "sizeLarge", null);
    __decorate([
        HostBinding('class.size-giant'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbOptionListComponent.prototype, "sizeGiant", null);
    NbOptionListComponent = __decorate([
        Component({
            selector: 'nb-option-list',
            template: "\n    <ul class=\"option-list\">\n      <ng-content></ng-content>\n    </ul>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], NbOptionListComponent);
    return NbOptionListComponent;
}());
export { NbOptionListComponent };
//# sourceMappingURL=option-list.component.js.map