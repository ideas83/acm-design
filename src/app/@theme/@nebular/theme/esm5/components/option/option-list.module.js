import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbOptionComponent } from './option.component';
import { NbOptionGroupComponent } from './option-group.component';
import { NbOptionListComponent } from './option-list.component';
import { NbCheckboxModule } from '../checkbox/checkbox.module';
var NB_OPTION_LIST_COMPONENTS = [
    NbOptionListComponent,
    NbOptionComponent,
    NbOptionGroupComponent,
];
var NbOptionModule = /** @class */ (function () {
    function NbOptionModule() {
    }
    NbOptionModule = __decorate([
        NgModule({
            declarations: __spreadArrays(NB_OPTION_LIST_COMPONENTS),
            imports: [
                CommonModule,
                NbCheckboxModule,
            ],
            exports: __spreadArrays(NB_OPTION_LIST_COMPONENTS),
        })
    ], NbOptionModule);
    return NbOptionModule;
}());
export { NbOptionModule };
//# sourceMappingURL=option-list.module.js.map