/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbOverlayModule } from '../cdk/overlay/overlay.module';
import { NbPopoverDirective } from './popover.directive';
import { NbPopoverComponent } from './popover.component';
var NbPopoverModule = /** @class */ (function () {
    function NbPopoverModule() {
    }
    NbPopoverModule = __decorate([
        NgModule({
            imports: [NbOverlayModule],
            declarations: [NbPopoverDirective, NbPopoverComponent],
            exports: [NbPopoverDirective],
            entryComponents: [NbPopoverComponent],
        })
    ], NbPopoverModule);
    return NbPopoverModule;
}());
export { NbPopoverModule };
//# sourceMappingURL=popover.module.js.map