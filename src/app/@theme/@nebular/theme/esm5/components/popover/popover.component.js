/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Component, ComponentFactoryResolver, Input, TemplateRef, Type, ViewChild, } from '@angular/core';
import { NbComponentPortal, NbTemplatePortal } from '../cdk/overlay/mapping';
import { NbOverlayContainerComponent, NbPositionedContainer, } from '../cdk/overlay/overlay-container';
/**
 * Overlay container.
 * Renders provided content inside.
 *
 * @styles
 *
 * popover-text-color:
 * popover-text-font-family:
 * popover-text-font-size:
 * popover-text-font-weight:
 * popover-text-line-height:
 * popover-background-color:
 * popover-border-width:
 * popover-border-color:
 * popover-border-radius:
 * popover-shadow:
 * popover-arrow-size:
 * popover-padding:
 * */
var NbPopoverComponent = /** @class */ (function (_super) {
    __extends(NbPopoverComponent, _super);
    function NbPopoverComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPopoverComponent.prototype.renderContent = function () {
        this.detachContent();
        this.attachContent();
    };
    NbPopoverComponent.prototype.detachContent = function () {
        this.overlayContainer.detach();
    };
    NbPopoverComponent.prototype.attachContent = function () {
        if (this.content instanceof TemplateRef) {
            this.attachTemplate();
        }
        else if (this.content instanceof Type) {
            this.attachComponent();
        }
        else {
            this.attachString();
        }
    };
    NbPopoverComponent.prototype.attachTemplate = function () {
        this.overlayContainer
            .attachTemplatePortal(new NbTemplatePortal(this.content, null, { $implicit: this.context }));
    };
    NbPopoverComponent.prototype.attachComponent = function () {
        var portal = new NbComponentPortal(this.content, null, null, this.cfr);
        var ref = this.overlayContainer.attachComponentPortal(portal, this.context);
        ref.changeDetectorRef.detectChanges();
    };
    NbPopoverComponent.prototype.attachString = function () {
        this.overlayContainer.attachStringContent(this.content);
    };
    __decorate([
        ViewChild(NbOverlayContainerComponent),
        __metadata("design:type", NbOverlayContainerComponent)
    ], NbPopoverComponent.prototype, "overlayContainer", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbPopoverComponent.prototype, "content", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbPopoverComponent.prototype, "context", void 0);
    __decorate([
        Input(),
        __metadata("design:type", ComponentFactoryResolver)
    ], NbPopoverComponent.prototype, "cfr", void 0);
    NbPopoverComponent = __decorate([
        Component({
            selector: 'nb-popover',
            template: "\n    <span class=\"arrow\"></span>\n    <nb-overlay-container></nb-overlay-container>\n  ",
            styles: [":host .arrow{position:absolute;width:0;height:0}\n"]
        })
    ], NbPopoverComponent);
    return NbPopoverComponent;
}(NbPositionedContainer));
export { NbPopoverComponent };
//# sourceMappingURL=popover.component.js.map