import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbOverlayModule } from '../cdk/overlay/overlay.module';
import { NbCardModule } from '../card/card.module';
import { NbIconModule } from '../icon/icon.module';
import { NbButtonModule } from '../button/button.module';
import { NbWindowService } from './window.service';
import { NbWindowsContainerComponent } from './windows-container.component';
import { NbWindowComponent } from './window.component';
import { NB_WINDOW_CONFIG } from './window.options';
var NbWindowModule = /** @class */ (function () {
    function NbWindowModule() {
    }
    NbWindowModule_1 = NbWindowModule;
    NbWindowModule.forRoot = function (defaultConfig) {
        return {
            ngModule: NbWindowModule_1,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    NbWindowModule.forChild = function (defaultConfig) {
        return {
            ngModule: NbWindowModule_1,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    var NbWindowModule_1;
    NbWindowModule = NbWindowModule_1 = __decorate([
        NgModule({
            imports: [CommonModule, NbOverlayModule, NbCardModule, NbIconModule, NbButtonModule],
            declarations: [
                NbWindowsContainerComponent,
                NbWindowComponent,
            ],
            entryComponents: [NbWindowsContainerComponent, NbWindowComponent],
        })
    ], NbWindowModule);
    return NbWindowModule;
}());
export { NbWindowModule };
//# sourceMappingURL=window.module.js.map