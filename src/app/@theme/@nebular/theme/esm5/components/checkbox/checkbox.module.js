/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbIconModule } from '../icon/icon.module';
import { NbCheckboxComponent } from './checkbox.component';
var NbCheckboxModule = /** @class */ (function () {
    function NbCheckboxModule() {
    }
    NbCheckboxModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
                NbIconModule,
            ],
            declarations: [NbCheckboxComponent],
            exports: [NbCheckboxComponent],
        })
    ], NbCheckboxModule);
    return NbCheckboxModule;
}());
export { NbCheckboxModule };
//# sourceMappingURL=checkbox.module.js.map