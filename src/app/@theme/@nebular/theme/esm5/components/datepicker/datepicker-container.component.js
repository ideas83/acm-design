/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { NbOverlayContainerComponent, NbPositionedContainer } from '../cdk/overlay/overlay-container';
var NbDatepickerContainerComponent = /** @class */ (function (_super) {
    __extends(NbDatepickerContainerComponent, _super);
    function NbDatepickerContainerComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbDatepickerContainerComponent.prototype.attach = function (portal) {
        return this.overlayContainer.attachComponentPortal(portal);
    };
    __decorate([
        ViewChild(NbOverlayContainerComponent, { static: true }),
        __metadata("design:type", NbOverlayContainerComponent)
    ], NbDatepickerContainerComponent.prototype, "overlayContainer", void 0);
    NbDatepickerContainerComponent = __decorate([
        Component({
            selector: 'nb-datepicker-container',
            template: "\n    <nb-overlay-container></nb-overlay-container>\n  "
        })
    ], NbDatepickerContainerComponent);
    return NbDatepickerContainerComponent;
}(NbPositionedContainer));
export { NbDatepickerContainerComponent };
//# sourceMappingURL=datepicker-container.component.js.map