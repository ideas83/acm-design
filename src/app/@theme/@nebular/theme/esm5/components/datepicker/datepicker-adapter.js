/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { NbDatepickerComponent, NbRangepickerComponent } from './datepicker.component';
import { NbDatepickerAdapter } from './datepicker.directive';
import { NbDateService } from '../calendar-kit/services/date.service';
var NbDateAdapterService = /** @class */ (function (_super) {
    __extends(NbDateAdapterService, _super);
    function NbDateAdapterService(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.picker = NbDatepickerComponent;
        return _this;
    }
    NbDateAdapterService.prototype.parse = function (date, format) {
        return this.dateService.parse(date, format);
    };
    NbDateAdapterService.prototype.format = function (date, format) {
        return this.dateService.format(date, format);
    };
    NbDateAdapterService.prototype.isValid = function (date, format) {
        return this.dateService.isValidDateString(date, format);
    };
    NbDateAdapterService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NbDateService])
    ], NbDateAdapterService);
    return NbDateAdapterService;
}(NbDatepickerAdapter));
export { NbDateAdapterService };
var NbRangeAdapterService = /** @class */ (function (_super) {
    __extends(NbRangeAdapterService, _super);
    function NbRangeAdapterService(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.picker = NbRangepickerComponent;
        return _this;
    }
    NbRangeAdapterService.prototype.parse = function (range, format) {
        var _a = range.split('-').map(function (subDate) { return subDate.trim(); }), start = _a[0], end = _a[1];
        return {
            start: this.dateService.parse(start, format),
            end: this.dateService.parse(end, format),
        };
    };
    NbRangeAdapterService.prototype.format = function (range, format) {
        if (!range) {
            return '';
        }
        var start = this.dateService.format(range.start, format);
        var isStartValid = this.dateService.isValidDateString(start, format);
        if (!isStartValid) {
            return '';
        }
        var end = this.dateService.format(range.end, format);
        var isEndValid = this.dateService.isValidDateString(end, format);
        if (isEndValid) {
            return start + " - " + end;
        }
        else {
            return start;
        }
    };
    NbRangeAdapterService.prototype.isValid = function (range, format) {
        var _a = range.split('-').map(function (subDate) { return subDate.trim(); }), start = _a[0], end = _a[1];
        return this.dateService.isValidDateString(start, format) && this.dateService.isValidDateString(end, format);
    };
    NbRangeAdapterService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NbDateService])
    ], NbRangeAdapterService);
    return NbRangeAdapterService;
}(NbDatepickerAdapter));
export { NbRangeAdapterService };
//# sourceMappingURL=datepicker-adapter.js.map