/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, Input, HostBinding, forwardRef, ChangeDetectorRef, Output, EventEmitter, ChangeDetectionStrategy, Renderer2, ElementRef, NgZone, } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NbLayoutDirectionService } from '../../services/direction.service';
import { convertToBoolProperty, emptyStatusWarning } from '../helpers';
/**
 * Toggle is a control representing `on` and `off` states.
 *
 * @stacked-example(Showcase, toggle/toggle-showcase.component)
 *
 * ### Installation
 *
 * Import `NbToggleComponent` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *     // ...
 *     NbToggleModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Toggle may have one of the following statuses: `basic`, `primary`, `success`, `warning`, `danger`, `info`, `control`
 *
 * @stacked-example(Toggle status, toggle/toggle-status.component)
 *
 * Toggle can be disabled via `disabled` input.
 *
 * @stacked-example(Disabled Toggles, toggle/toggle-disabled.component)
 *
 * Toggle may have a label with following positions: `left`, `right`, `start`, `end` (default)
 *
 * @stacked-example(Toggles With Labels, toggle/toggle-label-position.component.ts)
 *
 * You can set control state via `checked` binding:
 *
 * ```html
 * <nb-toggle [(checked)]="checked"></nb-toggle>
 * ```
 *
 * Or it could be set via reactive forms or ngModel bindings:
 *
 * @stacked-example(Toggle form binding, toggle/toggle-form.component)
 *
 * @styles
 *
 * toggle-height:
 * toggle-width:
 * toggle-border-width:
 * toggle-border-radius:
 * toggle-outline-width:
 * toggle-outline-color:
 * toggle-switcher-size:
 * toggle-switcher-icon-size:
 * toggle-text-font-family:
 * toggle-text-font-size:
 * toggle-text-font-weight:
 * toggle-text-line-height:
 * toggle-cursor:
 * toggle-disabled-cursor:
 * toggle-basic-text-color:
 * toggle-basic-background-color:
 * toggle-basic-border-color:
 * toggle-basic-checked-background-color:
 * toggle-basic-checked-border-color:
 * toggle-basic-checked-switcher-background-color:
 * toggle-basic-checked-switcher-checkmark-color:
 * toggle-basic-focus-background-color:
 * toggle-basic-focus-border-color:
 * toggle-basic-focus-checked-background-color:
 * toggle-basic-focus-checked-border-color:
 * toggle-basic-hover-background-color:
 * toggle-basic-hover-border-color:
 * toggle-basic-hover-checked-background-color:
 * toggle-basic-hover-checked-border-color:
 * toggle-basic-active-background-color:
 * toggle-basic-active-border-color:
 * toggle-basic-active-checked-background-color:
 * toggle-basic-active-checked-border-color:
 * toggle-basic-disabled-background-color:
 * toggle-basic-disabled-border-color:
 * toggle-basic-disabled-switcher-background-color:
 * toggle-basic-disabled-checked-switcher-checkmark-color:
 * toggle-basic-disabled-text-color:
 * toggle-primary-text-color:
 * toggle-primary-background-color:
 * toggle-primary-border-color:
 * toggle-primary-checked-background-color:
 * toggle-primary-checked-border-color:
 * toggle-primary-checked-switcher-background-color:
 * toggle-primary-checked-switcher-checkmark-color:
 * toggle-primary-focus-background-color:
 * toggle-primary-focus-border-color:
 * toggle-primary-focus-checked-background-color:
 * toggle-primary-focus-checked-border-color:
 * toggle-primary-hover-background-color:
 * toggle-primary-hover-border-color:
 * toggle-primary-hover-checked-background-color:
 * toggle-primary-hover-checked-border-color:
 * toggle-primary-active-background-color:
 * toggle-primary-active-border-color:
 * toggle-primary-active-checked-background-color:
 * toggle-primary-active-checked-border-color:
 * toggle-primary-disabled-background-color:
 * toggle-primary-disabled-border-color:
 * toggle-primary-disabled-switcher-background-color:
 * toggle-primary-disabled-checked-switcher-checkmark-color:
 * toggle-primary-disabled-text-color:
 * toggle-success-text-color:
 * toggle-success-background-color:
 * toggle-success-border-color:
 * toggle-success-checked-background-color:
 * toggle-success-checked-border-color:
 * toggle-success-checked-switcher-background-color:
 * toggle-success-checked-switcher-checkmark-color:
 * toggle-success-focus-background-color:
 * toggle-success-focus-border-color:
 * toggle-success-focus-checked-background-color:
 * toggle-success-focus-checked-border-color:
 * toggle-success-hover-background-color:
 * toggle-success-hover-border-color:
 * toggle-success-hover-checked-background-color:
 * toggle-success-hover-checked-border-color:
 * toggle-success-active-background-color:
 * toggle-success-active-border-color:
 * toggle-success-active-checked-background-color:
 * toggle-success-active-checked-border-color:
 * toggle-success-disabled-background-color:
 * toggle-success-disabled-border-color:
 * toggle-success-disabled-switcher-background-color:
 * toggle-success-disabled-checked-switcher-checkmark-color:
 * toggle-success-disabled-text-color:
 * toggle-info-text-color:
 * toggle-info-background-color:
 * toggle-info-border-color:
 * toggle-info-checked-background-color:
 * toggle-info-checked-border-color:
 * toggle-info-checked-switcher-background-color:
 * toggle-info-checked-switcher-checkmark-color:
 * toggle-info-focus-background-color:
 * toggle-info-focus-border-color:
 * toggle-info-focus-checked-background-color:
 * toggle-info-focus-checked-border-color:
 * toggle-info-hover-background-color:
 * toggle-info-hover-border-color:
 * toggle-info-hover-checked-background-color:
 * toggle-info-hover-checked-border-color:
 * toggle-info-active-background-color:
 * toggle-info-active-border-color:
 * toggle-info-active-checked-background-color:
 * toggle-info-active-checked-border-color:
 * toggle-info-disabled-background-color:
 * toggle-info-disabled-border-color:
 * toggle-info-disabled-switcher-background-color:
 * toggle-info-disabled-checked-switcher-checkmark-color:
 * toggle-info-disabled-text-color:
 * toggle-warning-text-color:
 * toggle-warning-background-color:
 * toggle-warning-border-color:
 * toggle-warning-checked-background-color:
 * toggle-warning-checked-border-color:
 * toggle-warning-checked-switcher-background-color:
 * toggle-warning-checked-switcher-checkmark-color:
 * toggle-warning-focus-background-color:
 * toggle-warning-focus-border-color:
 * toggle-warning-focus-checked-background-color:
 * toggle-warning-focus-checked-border-color:
 * toggle-warning-hover-background-color:
 * toggle-warning-hover-border-color:
 * toggle-warning-hover-checked-background-color:
 * toggle-warning-hover-checked-border-color:
 * toggle-warning-active-background-color:
 * toggle-warning-active-border-color:
 * toggle-warning-active-checked-background-color:
 * toggle-warning-active-checked-border-color:
 * toggle-warning-disabled-background-color:
 * toggle-warning-disabled-border-color:
 * toggle-warning-disabled-switcher-background-color:
 * toggle-warning-disabled-checked-switcher-checkmark-color:
 * toggle-warning-disabled-text-color:
 * toggle-danger-text-color:
 * toggle-danger-background-color:
 * toggle-danger-border-color:
 * toggle-danger-checked-background-color:
 * toggle-danger-checked-border-color:
 * toggle-danger-checked-switcher-background-color:
 * toggle-danger-checked-switcher-checkmark-color:
 * toggle-danger-focus-background-color:
 * toggle-danger-focus-border-color:
 * toggle-danger-focus-checked-background-color:
 * toggle-danger-focus-checked-border-color:
 * toggle-danger-hover-background-color:
 * toggle-danger-hover-border-color:
 * toggle-danger-hover-checked-background-color:
 * toggle-danger-hover-checked-border-color:
 * toggle-danger-active-background-color:
 * toggle-danger-active-border-color:
 * toggle-danger-active-checked-background-color:
 * toggle-danger-active-checked-border-color:
 * toggle-danger-disabled-background-color:
 * toggle-danger-disabled-border-color:
 * toggle-danger-disabled-switcher-background-color:
 * toggle-danger-disabled-checked-switcher-checkmark-color:
 * toggle-danger-disabled-text-color:
 * toggle-control-text-color:
 * toggle-control-background-color:
 * toggle-control-border-color:
 * toggle-control-checked-background-color:
 * toggle-control-checked-border-color:
 * toggle-control-checked-switcher-background-color:
 * toggle-control-checked-switcher-checkmark-color:
 * toggle-control-focus-background-color:
 * toggle-control-focus-border-color:
 * toggle-control-focus-checked-background-color:
 * toggle-control-focus-checked-border-color:
 * toggle-control-hover-background-color:
 * toggle-control-hover-border-color:
 * toggle-control-hover-checked-background-color:
 * toggle-control-hover-checked-border-color:
 * toggle-control-active-background-color:
 * toggle-control-active-border-color:
 * toggle-control-active-checked-background-color:
 * toggle-control-active-checked-border-color:
 * toggle-control-disabled-background-color:
 * toggle-control-disabled-border-color:
 * toggle-control-disabled-switcher-background-color:
 * toggle-control-disabled-checked-switcher-checkmark-color:
 * toggle-control-disabled-text-color:
 */
var NbToggleComponent = /** @class */ (function () {
    function NbToggleComponent(changeDetector, layoutDirection, renderer, hostElement, zone) {
        this.changeDetector = changeDetector;
        this.layoutDirection = layoutDirection;
        this.renderer = renderer;
        this.hostElement = hostElement;
        this.zone = zone;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.destroy$ = new Subject();
        this._checked = false;
        this._disabled = false;
        this._status = 'basic';
        /**
         * Toggle label position.
         * Possible values are: `left`, `right`, `start`, `end` (default)
         */
        this.labelPosition = 'end';
        /**
         * Output when checked state is changed by a user
         * @type EventEmitter<boolean>
         */
        this.checkedChange = new EventEmitter();
    }
    NbToggleComponent_1 = NbToggleComponent;
    Object.defineProperty(NbToggleComponent.prototype, "checked", {
        /**
         * Toggle checked
         * @type {boolean}
         */
        get: function () {
            return this._checked;
        },
        set: function (value) {
            this._checked = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "disabled", {
        /**
         * Controls input disabled state
         */
        get: function () {
            return this._disabled;
        },
        set: function (value) {
            this._disabled = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "status", {
        /**
         * Toggle status.
         * Possible values are: `basic`, `primary`, `success`, `warning`, `danger`, `info`, `control`.
         */
        get: function () {
            return this._status;
        },
        set: function (value) {
            if (value === '') {
                emptyStatusWarning('NbToggle');
                value = 'basic';
            }
            this._status = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "primary", {
        get: function () {
            return this.status === 'primary';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "success", {
        get: function () {
            return this.status === 'success';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "warning", {
        get: function () {
            return this.status === 'warning';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "danger", {
        get: function () {
            return this.status === 'danger';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "info", {
        get: function () {
            return this.status === 'info';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "basic", {
        get: function () {
            return this.status === 'basic';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "control", {
        get: function () {
            return this.status === 'control';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "labelLeft", {
        get: function () {
            return this.labelPosition === 'left';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "labelRight", {
        get: function () {
            return this.labelPosition === 'right';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "labelStart", {
        get: function () {
            return this.labelPosition === 'start';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToggleComponent.prototype, "labelEnd", {
        get: function () {
            return this.labelPosition === 'end';
        },
        enumerable: true,
        configurable: true
    });
    NbToggleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutDirection.onDirectionChange()
            .pipe(takeUntil(this.destroy$))
            .subscribe(function () { return _this.changeDetector.detectChanges(); });
    };
    NbToggleComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // TODO: #2254
        this.zone.runOutsideAngular(function () { return setTimeout(function () {
            _this.renderer.addClass(_this.hostElement.nativeElement, 'nb-transition');
        }); });
    };
    NbToggleComponent.prototype.ngOnDestroy = function () {
        this.destroy$.next();
        this.destroy$.complete();
    };
    NbToggleComponent.prototype.checkState = function () {
        if (this.checked) {
            return this.layoutDirection.isLtr() ? 'right' : 'left';
        }
        return this.layoutDirection.isLtr() ? 'left' : 'right';
    };
    NbToggleComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbToggleComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    NbToggleComponent.prototype.writeValue = function (val) {
        this.checked = val;
        this.changeDetector.markForCheck();
    };
    NbToggleComponent.prototype.setDisabledState = function (val) {
        this.disabled = convertToBoolProperty(val);
        this.changeDetector.markForCheck();
    };
    NbToggleComponent.prototype.updateValue = function (event) {
        var input = event.target;
        this.checked = input.checked;
        this.checkedChange.emit(this.checked);
        this.onChange(this.checked);
    };
    NbToggleComponent.prototype.onInputClick = function (event) {
        event.stopPropagation();
    };
    var NbToggleComponent_1;
    __decorate([
        Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], NbToggleComponent.prototype, "checked", null);
    __decorate([
        Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], NbToggleComponent.prototype, "disabled", null);
    __decorate([
        Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], NbToggleComponent.prototype, "status", null);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbToggleComponent.prototype, "labelPosition", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], NbToggleComponent.prototype, "checkedChange", void 0);
    __decorate([
        HostBinding('class.status-primary'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "primary", null);
    __decorate([
        HostBinding('class.status-success'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "success", null);
    __decorate([
        HostBinding('class.status-warning'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "warning", null);
    __decorate([
        HostBinding('class.status-danger'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "danger", null);
    __decorate([
        HostBinding('class.status-info'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "info", null);
    __decorate([
        HostBinding('class.status-basic'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "basic", null);
    __decorate([
        HostBinding('class.status-control'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "control", null);
    __decorate([
        HostBinding('class.toggle-label-left'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "labelLeft", null);
    __decorate([
        HostBinding('class.toggle-label-right'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "labelRight", null);
    __decorate([
        HostBinding('class.toggle-label-start'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "labelStart", null);
    __decorate([
        HostBinding('class.toggle-label-end'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], NbToggleComponent.prototype, "labelEnd", null);
    NbToggleComponent = NbToggleComponent_1 = __decorate([
        Component({
            selector: 'nb-toggle',
            animations: [
                trigger('position', [
                    state('right', style({ right: 0, left: '*' })),
                    state('left', style({ left: 0, right: '*' })),
                    transition(':enter', [animate(0)]),
                    transition('right <=> left', [animate('0.15s')]),
                ]),
            ],
            template: "\n    <label class=\"toggle-label\">\n      <input type=\"checkbox\"\n             class=\"native-input visually-hidden\"\n             role=\"switch\"\n             [attr.aria-checked]=\"checked\"\n             [disabled]=\"disabled\"\n             [checked]=\"checked\"\n             (change)=\"updateValue($event)\"\n             (blur)=\"onTouched()\"\n             (click)=\"onInputClick($event)\">\n      <div class=\"toggle\" [class.checked]=\"checked\">\n        <span [@position]=\"checkState()\" class=\"toggle-switcher\">\n          <nb-icon *ngIf=\"checked\" icon=\"checkmark-bold-outline\" pack=\"nebular-essentials\"></nb-icon>\n        </span>\n      </div>\n      <span class=\"text\">\n        <ng-content></ng-content>\n      </span>\n    </label>\n  ",
            providers: [{
                    provide: NG_VALUE_ACCESSOR,
                    useExisting: forwardRef(function () { return NbToggleComponent_1; }),
                    multi: true,
                }],
            changeDetection: ChangeDetectionStrategy.OnPush,
            styles: [":host{display:inline-flex;outline:none}:host(.toggle-label-left) .text:not(:empty){padding-right:0.6875rem}[dir=ltr] :host(.toggle-label-left) .text:not(:empty){order:-1}[dir=rtl] :host(.toggle-label-left) .text:not(:empty){order:1}:host(.toggle-label-right) .text:not(:empty){padding-left:0.6875rem}[dir=ltr] :host(.toggle-label-right) .text:not(:empty){order:1}[dir=rtl] :host(.toggle-label-right) .text:not(:empty){order:-1}:host(.toggle-label-start) .toggle-label{flex-direction:row-reverse}[dir=ltr] :host(.toggle-label-start) .toggle-label .text:not(:empty){padding-right:.6875rem}[dir=rtl] :host(.toggle-label-start) .toggle-label .text:not(:empty){padding-left:.6875rem}[dir=ltr] :host(.toggle-label-end) .text:not(:empty){padding-left:.6875rem}[dir=rtl] :host(.toggle-label-end) .text:not(:empty){padding-right:.6875rem}:host(.nb-transition) .toggle{transition-duration:0.15s;transition-property:background-color,border,box-shadow;transition-timing-function:ease-in}.toggle-label{position:relative;display:inline-flex;align-items:center}.toggle{position:relative;display:inline-flex;box-sizing:content-box}.toggle-switcher{position:absolute;border-radius:50%;margin:1px}.toggle-switcher nb-icon{position:absolute;top:50%;left:50%;transform:translate(-50%, -50%)}\n"]
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef,
            NbLayoutDirectionService,
            Renderer2,
            ElementRef,
            NgZone])
    ], NbToggleComponent);
    return NbToggleComponent;
}());
export { NbToggleComponent };
//# sourceMappingURL=toggle.component.js.map