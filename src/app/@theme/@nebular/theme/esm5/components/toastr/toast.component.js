/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output, Renderer2, } from '@angular/core';
import { NbToast } from './model';
/**
 * The `NbToastComponent` is responsible for rendering each toast with appropriate styles.
 *
 * @styles
 *
 * toastr-border-style:
 * toastr-border-width:
 * toastr-border-radius:
 * toastr-padding:
 * toastr-shadow:
 * toastr-text-font-family:
 * toastr-text-font-size:
 * toastr-text-font-weight:
 * toastr-text-line-height:
 * toastr-title-text-font-family:
 * toastr-title-text-font-size:
 * toastr-title-text-font-weight:
 * toastr-title-text-line-height:
 * toastr-basic-background-color:
 * toastr-basic-border-color:
 * toastr-basic-text-color:
 * toastr-icon-basic-background-color:
 * toastr-icon-basic-color:
 * toastr-destroyable-hover-basic-background-color:
 * toastr-destroyable-hover-basic-border-color:
 * toastr-primary-background-color:
 * toastr-primary-border-color:
 * toastr-primary-text-color:
 * toastr-icon-primary-background-color:
 * toastr-icon-primary-color:
 * toastr-destroyable-hover-primary-background-color:
 * toastr-destroyable-hover-primary-border-color:
 * toastr-success-background-color:
 * toastr-success-border-color:
 * toastr-success-text-color:
 * toastr-icon-success-background-color:
 * toastr-icon-success-color:
 * toastr-destroyable-hover-success-background-color:
 * toastr-destroyable-hover-success-border-color:
 * toastr-info-background-color:
 * toastr-info-border-color:
 * toastr-info-text-color:
 * toastr-icon-info-background-color:
 * toastr-icon-info-color:
 * toastr-destroyable-hover-info-background-color:
 * toastr-destroyable-hover-info-border-color:
 * toastr-warning-background-color:
 * toastr-warning-border-color:
 * toastr-warning-text-color:
 * toastr-icon-warning-background-color:
 * toastr-icon-warning-color:
 * toastr-destroyable-hover-warning-background-color:
 * toastr-destroyable-hover-warning-border-color:
 * toastr-danger-background-color:
 * toastr-danger-border-color:
 * toastr-danger-text-color:
 * toastr-icon-danger-background-color:
 * toastr-icon-danger-color:
 * toastr-destroyable-hover-danger-background-color:
 * toastr-destroyable-hover-danger-border-color:
 * toastr-control-background-color:
 * toastr-control-border-color:
 * toastr-control-text-color:
 * toastr-icon-control-background-color:
 * toastr-icon-control-color:
 * toastr-destroyable-hover-control-background-color:
 * toastr-destroyable-hover-control-border-color:
 * */
var NbToastComponent = /** @class */ (function () {
    function NbToastComponent(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.destroy = new EventEmitter();
    }
    Object.defineProperty(NbToastComponent.prototype, "success", {
        get: function () {
            return this.toast.config.status === 'success';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "info", {
        get: function () {
            return this.toast.config.status === 'info';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "warning", {
        get: function () {
            return this.toast.config.status === 'warning';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "primary", {
        get: function () {
            return this.toast.config.status === 'primary';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "danger", {
        get: function () {
            return this.toast.config.status === 'danger';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "basic", {
        get: function () {
            return this.toast.config.status === 'basic';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "control", {
        get: function () {
            return this.toast.config.status === 'control';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "destroyByClick", {
        get: function () {
            return this.toast.config.destroyByClick;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "hasIcon", {
        get: function () {
            var icon = this.toast.config.icon;
            if (typeof icon === 'string') {
                return true;
            }
            return !!(icon && icon.icon);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "customIcon", {
        get: function () {
            return !!this.icon;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "icon", {
        get: function () {
            return this.toast.config.icon;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "iconPack", {
        /* @deprecated Use pack property of icon config */
        get: function () {
            return this.toast.config.iconPack;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "iconConfig", {
        /*
          @breaking-change 5 remove
          @deprecated
        */
        get: function () {
            var toastConfig = this.toast.config;
            var isIconName = typeof this.icon === 'string';
            if (!isIconName) {
                return toastConfig.icon;
            }
            var iconConfig = { icon: toastConfig.icon };
            if (toastConfig.iconPack) {
                iconConfig.pack = toastConfig.iconPack;
            }
            return iconConfig;
        },
        enumerable: true,
        configurable: true
    });
    NbToastComponent.prototype.onClick = function () {
        this.destroy.emit();
    };
    NbToastComponent.prototype.ngOnInit = function () {
        if (this.toast.config.toastClass) {
            this.renderer.addClass(this.elementRef.nativeElement, this.toast.config.toastClass);
        }
    };
    __decorate([
        Input(),
        __metadata("design:type", NbToast)
    ], NbToastComponent.prototype, "toast", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], NbToastComponent.prototype, "destroy", void 0);
    __decorate([
        HostBinding('class.status-success'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "success", null);
    __decorate([
        HostBinding('class.status-info'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "info", null);
    __decorate([
        HostBinding('class.status-warning'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "warning", null);
    __decorate([
        HostBinding('class.status-primary'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "primary", null);
    __decorate([
        HostBinding('class.status-danger'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "danger", null);
    __decorate([
        HostBinding('class.status-basic'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "basic", null);
    __decorate([
        HostBinding('class.status-control'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "control", null);
    __decorate([
        HostBinding('class.destroy-by-click'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "destroyByClick", null);
    __decorate([
        HostBinding('class.has-icon'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "hasIcon", null);
    __decorate([
        HostBinding('class.custom-icon'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], NbToastComponent.prototype, "customIcon", null);
    __decorate([
        HostListener('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], NbToastComponent.prototype, "onClick", null);
    NbToastComponent = __decorate([
        Component({
            selector: 'nb-toast',
            template: "<div class=\"icon-container\" *ngIf=\"hasIcon && icon\">\n  <nb-icon [config]=\"iconConfig\"></nb-icon>\n</div>\n<div class=\"content-container\">\n  <span class=\"title subtitle\">{{ toast.title }}</span>\n  <div class=\"message\">{{ toast.message }}</div>\n</div>\n",
            styles: [":host{display:flex;align-items:center;width:25rem;margin:0.5rem}:host .title{margin-right:0.25rem}:host.default .content-container,:host:not(.has-icon) .content-container{display:flex;flex-direction:row}:host.destroy-by-click{cursor:pointer}:host nb-icon{font-size:2.5rem}:host svg{width:2.5rem;height:2.5rem}\n"]
        }),
        __metadata("design:paramtypes", [Renderer2, ElementRef])
    ], NbToastComponent);
    return NbToastComponent;
}());
export { NbToastComponent };
//# sourceMappingURL=toast.component.js.map