/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, Input, QueryList, ViewChildren } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NbToastComponent } from './toast.component';
import { NbLayoutDirectionService } from '../../services/direction.service';
import { NbPositionHelper } from '../cdk/overlay/position-helper';
var voidState = style({
    transform: 'translateX({{ direction }}110%)',
    height: 0,
    marginLeft: '0',
    marginRight: '0',
    marginTop: '0',
    marginBottom: '0',
});
var defaultOptions = { params: { direction: '' } };
var NbToastrContainerComponent = /** @class */ (function () {
    function NbToastrContainerComponent(layoutDirection, positionHelper) {
        this.layoutDirection = layoutDirection;
        this.positionHelper = positionHelper;
        this.destroy$ = new Subject();
        this.content = [];
    }
    NbToastrContainerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutDirection.onDirectionChange()
            .pipe(takeUntil(this.destroy$))
            .subscribe(function () { return _this.onDirectionChange(); });
    };
    NbToastrContainerComponent.prototype.ngOnDestroy = function () {
        this.destroy$.next();
        this.destroy$.complete();
    };
    NbToastrContainerComponent.prototype.onDirectionChange = function () {
        var direction = this.positionHelper.isRightPosition(this.position) ? '' : '-';
        this.fadeIn = { value: '', params: { direction: direction } };
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], NbToastrContainerComponent.prototype, "content", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], NbToastrContainerComponent.prototype, "context", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbToastrContainerComponent.prototype, "position", void 0);
    __decorate([
        ViewChildren(NbToastComponent),
        __metadata("design:type", QueryList)
    ], NbToastrContainerComponent.prototype, "toasts", void 0);
    NbToastrContainerComponent = __decorate([
        Component({
            selector: 'nb-toastr-container',
            template: "\n    <nb-toast [@fadeIn]=\"fadeIn\" *ngFor=\"let toast of content\" [toast]=\"toast\"></nb-toast>",
            animations: [
                trigger('fadeIn', [
                    transition(':enter', [voidState, animate(100)], defaultOptions),
                    transition(':leave', [animate(100, voidState)], defaultOptions),
                ]),
            ]
        }),
        __metadata("design:paramtypes", [NbLayoutDirectionService,
            NbPositionHelper])
    ], NbToastrContainerComponent);
    return NbToastrContainerComponent;
}());
export { NbToastrContainerComponent };
//# sourceMappingURL=toastr-container.component.js.map