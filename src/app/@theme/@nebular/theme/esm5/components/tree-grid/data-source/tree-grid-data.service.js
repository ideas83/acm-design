/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __assign, __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { NB_DEFAULT_ROW_LEVEL, NbTreeGridPresentationNode } from './tree-grid.model';
var NbTreeGridDataService = /** @class */ (function () {
    function NbTreeGridDataService() {
        this.defaultGetters = {
            dataGetter: function (node) { return node.data; },
            childrenGetter: function (d) { return d.children || undefined; },
            expandedGetter: function (d) { return !!d.expanded; },
        };
    }
    NbTreeGridDataService.prototype.toPresentationNodes = function (nodes, customGetters, level) {
        if (level === void 0) { level = NB_DEFAULT_ROW_LEVEL; }
        var getters = __assign(__assign({}, this.defaultGetters), customGetters);
        return this.mapNodes(nodes, getters, level);
    };
    NbTreeGridDataService.prototype.mapNodes = function (nodes, getters, level) {
        var _this = this;
        var dataGetter = getters.dataGetter, childrenGetter = getters.childrenGetter, expandedGetter = getters.expandedGetter;
        return nodes.map(function (node) {
            var childrenNodes = childrenGetter(node);
            var children;
            if (childrenNodes) {
                children = _this.toPresentationNodes(childrenNodes, getters, level + 1);
            }
            return new NbTreeGridPresentationNode(dataGetter(node), children, expandedGetter(node), level);
        });
    };
    NbTreeGridDataService.prototype.flattenExpanded = function (nodes) {
        var _this = this;
        return nodes.reduce(function (res, node) {
            res.push(node);
            if (node.expanded && node.hasChildren()) {
                res.push.apply(res, _this.flattenExpanded(node.children));
            }
            return res;
        }, []);
    };
    NbTreeGridDataService.prototype.copy = function (nodes) {
        var _this = this;
        return nodes.map(function (node) {
            var children;
            if (node.hasChildren()) {
                children = _this.copy(node.children);
            }
            return new NbTreeGridPresentationNode(node.data, children, node.expanded, node.level);
        });
    };
    NbTreeGridDataService = __decorate([
        Injectable()
    ], NbTreeGridDataService);
    return NbTreeGridDataService;
}());
export { NbTreeGridDataService };
//# sourceMappingURL=tree-grid-data.service.js.map