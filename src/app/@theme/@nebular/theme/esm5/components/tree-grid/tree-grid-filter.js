/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __extends, __metadata } from "tslib";
import { Directive, HostListener, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
var NbFilterDirective = /** @class */ (function () {
    function NbFilterDirective() {
    }
    NbFilterDirective.prototype.filter = function (filterRequest) {
        this.filterable.filter(filterRequest);
    };
    __decorate([
        Input('nbFilter'),
        __metadata("design:type", Object)
    ], NbFilterDirective.prototype, "filterable", void 0);
    NbFilterDirective = __decorate([
        Directive({ selector: '[nbFilter]' })
    ], NbFilterDirective);
    return NbFilterDirective;
}());
export { NbFilterDirective };
/**
 * Helper directive to trigger data source's filter method when user types in input
 */
var NbFilterInputDirective = /** @class */ (function (_super) {
    __extends(NbFilterInputDirective, _super);
    function NbFilterInputDirective() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.search$ = new Subject();
        _this.destroy$ = new Subject();
        /**
         * Debounce time before triggering filter method. Set in milliseconds.
         * Default 200.
         */
        _this.debounceTime = 200;
        return _this;
    }
    NbFilterInputDirective_1 = NbFilterInputDirective;
    NbFilterInputDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.search$
            .pipe(debounceTime(this.debounceTime), takeUntil(this.destroy$))
            .subscribe(function (query) {
            _super.prototype.filter.call(_this, query);
        });
    };
    NbFilterInputDirective.prototype.ngOnDestroy = function () {
        this.destroy$.next();
        this.destroy$.complete();
        this.search$.complete();
    };
    NbFilterInputDirective.prototype.filter = function (event) {
        this.search$.next(event.target.value);
    };
    var NbFilterInputDirective_1;
    __decorate([
        Input('nbFilterInput'),
        __metadata("design:type", Object)
    ], NbFilterInputDirective.prototype, "filterable", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Number)
    ], NbFilterInputDirective.prototype, "debounceTime", void 0);
    __decorate([
        HostListener('input', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NbFilterInputDirective.prototype, "filter", null);
    NbFilterInputDirective = NbFilterInputDirective_1 = __decorate([
        Directive({
            selector: '[nbFilterInput]',
            providers: [{ provide: NbFilterDirective, useExisting: NbFilterInputDirective_1 }],
        })
    ], NbFilterInputDirective);
    return NbFilterInputDirective;
}(NbFilterDirective));
export { NbFilterInputDirective };
//# sourceMappingURL=tree-grid-filter.js.map