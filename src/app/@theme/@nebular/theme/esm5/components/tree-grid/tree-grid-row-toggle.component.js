/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, HostListener, Input } from '@angular/core';
import { NbTreeGridCellDirective } from './tree-grid-cell.component';
/**
 * NbTreeGridRowToggleComponent
 */
var NbTreeGridRowToggleComponent = /** @class */ (function () {
    function NbTreeGridRowToggleComponent(cell) {
        this.cell = cell;
    }
    Object.defineProperty(NbTreeGridRowToggleComponent.prototype, "expanded", {
        get: function () {
            return this.expandedValue;
        },
        set: function (value) {
            this.expandedValue = value;
        },
        enumerable: true,
        configurable: true
    });
    NbTreeGridRowToggleComponent.prototype.toggleRow = function ($event) {
        this.cell.toggleRow();
        $event.stopPropagation();
    };
    __decorate([
        Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], NbTreeGridRowToggleComponent.prototype, "expanded", null);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NbTreeGridRowToggleComponent.prototype, "toggleRow", null);
    NbTreeGridRowToggleComponent = __decorate([
        Component({
            selector: 'nb-tree-grid-row-toggle',
            template: "\n    <button class=\"row-toggle-button\" [attr.aria-label]=\"expanded ? 'collapse' : 'expand'\">\n      <nb-icon [icon]=\"expanded ? 'chevron-down-outline' : 'chevron-right-outline'\"\n               pack=\"nebular-essentials\"\n               aria-hidden=\"true\">\n      </nb-icon>\n    </button>\n  ",
            styles: ["\n    button {\n      background: transparent;\n      border: none;\n      padding: 0;\n    }\n  "]
        }),
        __metadata("design:paramtypes", [NbTreeGridCellDirective])
    ], NbTreeGridRowToggleComponent);
    return NbTreeGridRowToggleComponent;
}());
export { NbTreeGridRowToggleComponent };
//# sourceMappingURL=tree-grid-row-toggle.component.js.map