/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbIconModule } from '../icon/icon.module';
import { NbAccordionComponent } from './accordion.component';
import { NbAccordionItemComponent } from './accordion-item.component';
import { NbAccordionItemHeaderComponent } from './accordion-item-header.component';
import { NbAccordionItemBodyComponent } from './accordion-item-body.component';
var NB_ACCORDION_COMPONENTS = [
    NbAccordionComponent,
    NbAccordionItemComponent,
    NbAccordionItemHeaderComponent,
    NbAccordionItemBodyComponent,
];
var NbAccordionModule = /** @class */ (function () {
    function NbAccordionModule() {
    }
    NbAccordionModule = __decorate([
        NgModule({
            imports: [CommonModule, NbIconModule],
            exports: __spreadArrays(NB_ACCORDION_COMPONENTS),
            declarations: __spreadArrays(NB_ACCORDION_COMPONENTS),
            providers: [],
        })
    ], NbAccordionModule);
    return NbAccordionModule;
}());
export { NbAccordionModule };
//# sourceMappingURL=accordion.module.js.map