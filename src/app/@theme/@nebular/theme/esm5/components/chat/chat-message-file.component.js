/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __assign, __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Chat message component.
 */
var NbChatMessageFileComponent = /** @class */ (function () {
    function NbChatMessageFileComponent(cd, domSanitizer) {
        this.cd = cd;
        this.domSanitizer = domSanitizer;
        /**
         * Message send date format, default 'shortTime'
         * @type {string}
         */
        this.dateFormat = 'shortTime';
    }
    Object.defineProperty(NbChatMessageFileComponent.prototype, "files", {
        /**
         * Message file path
         * @type {Date}
         */
        set: function (files) {
            var _this = this;
            this.readyFiles = (files || []).map(function (file) {
                var isImage = _this.isImage(file);
                return __assign(__assign({}, file), { urlStyle: isImage && _this.domSanitizer.bypassSecurityTrustStyle("url(" + file.url + ")"), isImage: isImage });
            });
            this.cd.detectChanges();
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageFileComponent.prototype.isImage = function (file) {
        var type = file.type;
        if (type) {
            return ['image/png', 'image/jpeg', 'image/gif'].includes(type);
        }
        return false;
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageFileComponent.prototype, "message", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageFileComponent.prototype, "sender", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Date)
    ], NbChatMessageFileComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageFileComponent.prototype, "dateFormat", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], NbChatMessageFileComponent.prototype, "files", null);
    NbChatMessageFileComponent = __decorate([
        Component({
            selector: 'nb-chat-message-file',
            template: "\n    <nb-chat-message-text [sender]=\"sender\" [date]=\"date\" [dateFormat]=\"dateFormat\" [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n\n    <ng-container *ngIf=\"readyFiles?.length > 1\">\n      <div class=\"message-content-group\">\n        <a *ngFor=\"let file of readyFiles\" [href]=\"file.url\" target=\"_blank\">\n          <nb-icon [icon]=\"file.icon\" *ngIf=\"!file.urlStyle && file.icon\"></nb-icon>\n          <div *ngIf=\"file.urlStyle\" [style.background-image]=\"file.urlStyle\"></div>\n        </a>\n      </div>\n    </ng-container>\n\n    <ng-container *ngIf=\"readyFiles?.length === 1\">\n      <a [href]=\"readyFiles[0].url\" target=\"_blank\">\n        <nb-icon [icon]=\"readyFiles[0].icon\" *ngIf=\"!readyFiles[0].urlStyle && readyFiles[0].icon\"></nb-icon>\n        <div *ngIf=\"readyFiles[0].urlStyle\" [style.background-image]=\"readyFiles[0].urlStyle\"></div>\n      </a>\n    </ng-container>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef, DomSanitizer])
    ], NbChatMessageFileComponent);
    return NbChatMessageFileComponent;
}());
export { NbChatMessageFileComponent };
//# sourceMappingURL=chat-message-file.component.js.map