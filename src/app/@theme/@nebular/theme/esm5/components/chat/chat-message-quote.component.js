/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
/**
 * Chat message component.
 */
var NbChatMessageQuoteComponent = /** @class */ (function () {
    function NbChatMessageQuoteComponent() {
        /**
          * Message send date format, default 'shortTime'
          * @type {string}
          */
        this.dateFormat = 'shortTime';
    }
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageQuoteComponent.prototype, "message", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageQuoteComponent.prototype, "sender", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Date)
    ], NbChatMessageQuoteComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageQuoteComponent.prototype, "dateFormat", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageQuoteComponent.prototype, "quote", void 0);
    NbChatMessageQuoteComponent = __decorate([
        Component({
            selector: 'nb-chat-message-quote',
            template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date | date: dateFormat }}</time></p>\n    <p class=\"quote\">\n      {{ quote }}\n    </p>\n    <nb-chat-message-text [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], NbChatMessageQuoteComponent);
    return NbChatMessageQuoteComponent;
}());
export { NbChatMessageQuoteComponent };
//# sourceMappingURL=chat-message-quote.component.js.map