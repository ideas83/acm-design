/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
/**
 * Chat message component.
 */
var NbChatMessageTextComponent = /** @class */ (function () {
    function NbChatMessageTextComponent() {
        /**
         * Message send date format, default 'shortTime'
         * @type {string}
         */
        this.dateFormat = 'shortTime';
    }
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageTextComponent.prototype, "sender", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageTextComponent.prototype, "message", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Date)
    ], NbChatMessageTextComponent.prototype, "date", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NbChatMessageTextComponent.prototype, "dateFormat", void 0);
    NbChatMessageTextComponent = __decorate([
        Component({
            selector: 'nb-chat-message-text',
            template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date  | date: dateFormat }}</time></p>\n    <p class=\"text\" *ngIf=\"message\">{{ message }}</p>\n  ",
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], NbChatMessageTextComponent);
    return NbChatMessageTextComponent;
}());
export { NbChatMessageTextComponent };
//# sourceMappingURL=chat-message-text.component.js.map