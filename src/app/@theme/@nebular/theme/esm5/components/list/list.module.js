import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbListComponent, NbListItemComponent } from './list.component';
import { NbListPageTrackerDirective } from './list-page-tracker.directive';
import { NbInfiniteListDirective } from './infinite-list.directive';
var components = [
    NbListComponent,
    NbListItemComponent,
    NbListPageTrackerDirective,
    NbInfiniteListDirective,
];
var NbListModule = /** @class */ (function () {
    function NbListModule() {
    }
    NbListModule = __decorate([
        NgModule({
            declarations: components,
            exports: components,
        })
    ], NbListModule);
    return NbListModule;
}());
export { NbListModule };
//# sourceMappingURL=list.module.js.map