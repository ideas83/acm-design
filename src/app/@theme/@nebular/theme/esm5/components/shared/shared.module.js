/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
var NbSharedModule = /** @class */ (function () {
    function NbSharedModule() {
    }
    NbSharedModule = __decorate([
        NgModule({
            exports: [
                CommonModule,
                // TODO: probably we don't need FormsModule in SharedModule
                FormsModule,
                RouterModule,
            ],
        })
    ], NbSharedModule);
    return NbSharedModule;
}());
export { NbSharedModule };
//# sourceMappingURL=shared.module.js.map