/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbUserComponent, } from './user.component';
import { NbBadgeModule } from '../badge/badge.module';
var NB_USER_COMPONENTS = [
    NbUserComponent,
];
var NbUserModule = /** @class */ (function () {
    function NbUserModule() {
    }
    NbUserModule = __decorate([
        NgModule({
            imports: [
                NbSharedModule,
                NbBadgeModule,
            ],
            declarations: __spreadArrays(NB_USER_COMPONENTS),
            exports: __spreadArrays(NB_USER_COMPONENTS),
        })
    ], NbUserModule);
    return NbUserModule;
}());
export { NbUserModule };
//# sourceMappingURL=user.module.js.map