import { __decorate, __spreadArrays } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../../shared/shared.module';
import { NbA11yModule } from '../a11y/a11y.module';
import { NbCdkMappingModule } from './mapping';
import { NbPositionBuilderService } from './overlay-position';
import { NbOverlayContainerComponent, NbPositionedContainer } from './overlay-container';
import { NbOverlayService } from './overlay-service';
import { NbCdkAdapterModule } from '../adapter/adapter.module';
import { NbPositionHelper } from './position-helper';
import { NbTriggerStrategyBuilderService } from './overlay-trigger';
var NbOverlayModule = /** @class */ (function () {
    function NbOverlayModule() {
    }
    NbOverlayModule_1 = NbOverlayModule;
    NbOverlayModule.forRoot = function () {
        return {
            ngModule: NbOverlayModule_1,
            providers: __spreadArrays([
                NbPositionBuilderService,
                NbTriggerStrategyBuilderService,
                NbOverlayService,
                NbPositionHelper
            ], NbCdkMappingModule.forRoot().providers, NbCdkAdapterModule.forRoot().providers, NbA11yModule.forRoot().providers),
        };
    };
    var NbOverlayModule_1;
    NbOverlayModule = NbOverlayModule_1 = __decorate([
        NgModule({
            imports: [
                NbCdkMappingModule,
                NbSharedModule,
            ],
            declarations: [
                NbPositionedContainer,
                NbOverlayContainerComponent,
            ],
            exports: [
                NbCdkMappingModule,
                NbCdkAdapterModule,
                NbOverlayContainerComponent,
            ],
        })
    ], NbOverlayModule);
    return NbOverlayModule;
}());
export { NbOverlayModule };
//# sourceMappingURL=overlay.module.js.map