import { __extends } from "tslib";
import { FocusKeyManager } from '@angular/cdk/a11y';
var NbFocusKeyManager = /** @class */ (function (_super) {
    __extends(NbFocusKeyManager, _super);
    function NbFocusKeyManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbFocusKeyManager;
}(FocusKeyManager));
export { NbFocusKeyManager };
var NbFocusKeyManagerFactoryService = /** @class */ (function () {
    function NbFocusKeyManagerFactoryService() {
    }
    NbFocusKeyManagerFactoryService.prototype.create = function (items) {
        return new NbFocusKeyManager(items);
    };
    return NbFocusKeyManagerFactoryService;
}());
export { NbFocusKeyManagerFactoryService };
//# sourceMappingURL=focus-key-manager.js.map