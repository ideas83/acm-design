import { __decorate, __extends } from "tslib";
import { NgModule } from '@angular/core';
import { BidiModule, Directionality } from '@angular/cdk/bidi';
import { NbDirectionality } from './bidi-service';
var NbBidiModule = /** @class */ (function (_super) {
    __extends(NbBidiModule, _super);
    function NbBidiModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbBidiModule = __decorate([
        NgModule({
            providers: [
                { provide: NbDirectionality, useExisting: Directionality },
            ],
        })
    ], NbBidiModule);
    return NbBidiModule;
}(BidiModule));
export { NbBidiModule };
//# sourceMappingURL=bidi.module.js.map