import { __decorate, __extends, __metadata, __param, __spreadArrays } from "tslib";
import { Attribute, ChangeDetectorRef, ElementRef, Inject, IterableDiffers, NgModule, Component } from '@angular/core';
import { CdkTable, CdkTableModule } from '@angular/cdk/table';
import { NbBidiModule } from '../bidi/bidi.module';
import { NbDirectionality } from '../bidi/bidi-service';
import { NbPlatform } from '../platform/platform-service';
import { NB_DOCUMENT } from '../../../theme.options';
import { NbCellDefDirective, NbCellDirective, NbColumnDefDirective, NbFooterCellDefDirective, NbFooterCellDirective, NbHeaderCellDefDirective, NbHeaderCellDirective, } from './cell';
import { NbCellOutletDirective, NbDataRowOutletDirective, NbFooterRowOutletDirective, NbHeaderRowOutletDirective, NbFooterRowComponent, NbFooterRowDefDirective, NbHeaderRowComponent, NbHeaderRowDefDirective, NbRowComponent, NbRowDefDirective, } from './row';
export var NB_TABLE_TEMPLATE = "\n  <ng-container nbHeaderRowOutlet></ng-container>\n  <ng-container nbRowOutlet></ng-container>\n  <ng-container nbFooterRowOutlet></ng-container>";
var NbTable = /** @class */ (function (_super) {
    __extends(NbTable, _super);
    function NbTable(differs, changeDetectorRef, elementRef, role, dir, document, platform) {
        return _super.call(this, differs, changeDetectorRef, elementRef, role, dir, document, platform) || this;
    }
    NbTable = __decorate([
        Component({
            selector: 'nb-table-not-implemented',
            template: ""
        })
        // tslint:disable-next-line:component-class-suffix
        ,
        __param(3, Attribute('role')),
        __param(5, Inject(NB_DOCUMENT)),
        __metadata("design:paramtypes", [IterableDiffers,
            ChangeDetectorRef,
            ElementRef, String, NbDirectionality, Object, NbPlatform])
    ], NbTable);
    return NbTable;
}(CdkTable));
export { NbTable };
var COMPONENTS = [
    NbTable,
    // Template defs
    NbHeaderCellDefDirective,
    NbHeaderRowDefDirective,
    NbColumnDefDirective,
    NbCellDefDirective,
    NbRowDefDirective,
    NbFooterCellDefDirective,
    NbFooterRowDefDirective,
    // Outlets
    NbDataRowOutletDirective,
    NbHeaderRowOutletDirective,
    NbFooterRowOutletDirective,
    NbCellOutletDirective,
    // Cell directives
    NbHeaderCellDirective,
    NbCellDirective,
    NbFooterCellDirective,
    // Row directives
    NbHeaderRowComponent,
    NbRowComponent,
    NbFooterRowComponent,
];
var NbTableModule = /** @class */ (function (_super) {
    __extends(NbTableModule, _super);
    function NbTableModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbTableModule = __decorate([
        NgModule({
            imports: [NbBidiModule],
            declarations: __spreadArrays(COMPONENTS),
            exports: __spreadArrays(COMPONENTS),
        })
    ], NbTableModule);
    return NbTableModule;
}(CdkTableModule));
export { NbTableModule };
//# sourceMappingURL=table.module.js.map