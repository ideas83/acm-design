import { __decorate, __extends } from "tslib";
import { NgModule, Injectable } from '@angular/core';
import { NbFocusTrapFactoryService } from './focus-trap';
import { NbFocusKeyManagerFactoryService } from './focus-key-manager';
import { NbActiveDescendantKeyManagerFactoryService } from './descendant-key-manager';
import { FocusMonitor } from '@angular/cdk/a11y';
var NbFocusMonitor = /** @class */ (function (_super) {
    __extends(NbFocusMonitor, _super);
    function NbFocusMonitor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbFocusMonitor = __decorate([
        Injectable()
    ], NbFocusMonitor);
    return NbFocusMonitor;
}(FocusMonitor));
export { NbFocusMonitor };
var NbA11yModule = /** @class */ (function () {
    function NbA11yModule() {
    }
    NbA11yModule_1 = NbA11yModule;
    NbA11yModule.forRoot = function () {
        return {
            ngModule: NbA11yModule_1,
            providers: [
                NbFocusTrapFactoryService,
                NbFocusKeyManagerFactoryService,
                NbActiveDescendantKeyManagerFactoryService,
                { provide: NbFocusMonitor, useClass: FocusMonitor },
            ],
        };
    };
    var NbA11yModule_1;
    NbA11yModule = NbA11yModule_1 = __decorate([
        NgModule({})
    ], NbA11yModule);
    return NbA11yModule;
}());
export { NbA11yModule };
//# sourceMappingURL=a11y.module.js.map