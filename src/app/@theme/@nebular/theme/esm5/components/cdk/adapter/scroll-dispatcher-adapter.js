import { __decorate, __extends, __metadata } from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { NbPlatform } from '../platform/platform-service';
import { NbLayoutScrollService } from '../../../services/scroll.service';
var NbScrollDispatcherAdapter = /** @class */ (function (_super) {
    __extends(NbScrollDispatcherAdapter, _super);
    function NbScrollDispatcherAdapter(ngZone, platform, scrollService) {
        var _this = _super.call(this, ngZone, platform) || this;
        _this.scrollService = scrollService;
        return _this;
    }
    NbScrollDispatcherAdapter.prototype.scrolled = function (auditTimeInMs) {
        return this.scrollService.onScroll();
    };
    NbScrollDispatcherAdapter = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NgZone, NbPlatform, NbLayoutScrollService])
    ], NbScrollDispatcherAdapter);
    return NbScrollDispatcherAdapter;
}(ScrollDispatcher));
export { NbScrollDispatcherAdapter };
//# sourceMappingURL=scroll-dispatcher-adapter.js.map