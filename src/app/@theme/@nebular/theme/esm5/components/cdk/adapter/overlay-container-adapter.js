import { __decorate, __extends } from "tslib";
import { Injectable } from '@angular/core';
import { NbOverlayContainer } from '../overlay/mapping';
/**
 * Provides nb-layout as overlay container.
 * Container has to be cleared when layout destroys.
 * Another way previous version of the container will be used
 * but it isn't inserted in DOM and exists in memory only.
 * This case important only if you switch between multiple layouts.
 * */
var NbOverlayContainerAdapter = /** @class */ (function (_super) {
    __extends(NbOverlayContainerAdapter, _super);
    function NbOverlayContainerAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayContainerAdapter.prototype.setContainer = function (container) {
        this.container = container;
    };
    NbOverlayContainerAdapter.prototype.clearContainer = function () {
        this.container = null;
        this._containerElement = null;
    };
    NbOverlayContainerAdapter.prototype._createContainer = function () {
        var container = this._document.createElement('div');
        container.classList.add('cdk-overlay-container');
        this.container.appendChild(container);
        this._containerElement = container;
    };
    NbOverlayContainerAdapter = __decorate([
        Injectable()
    ], NbOverlayContainerAdapter);
    return NbOverlayContainerAdapter;
}(NbOverlayContainer));
export { NbOverlayContainerAdapter };
//# sourceMappingURL=overlay-container-adapter.js.map