import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { OverlayContainer, ScrollDispatcher, ScrollStrategyOptions } from '@angular/cdk/overlay';
import { NbOverlayContainer } from '../overlay/mapping';
import { NbOverlayContainerAdapter } from './overlay-container-adapter';
import { NbScrollDispatcherAdapter } from './scroll-dispatcher-adapter';
import { NbViewportRulerAdapter } from './viewport-ruler-adapter';
import { NbBlockScrollStrategyAdapter, NbScrollStrategyOptions } from './block-scroll-strategy-adapter';
var NbCdkAdapterModule = /** @class */ (function () {
    function NbCdkAdapterModule() {
    }
    NbCdkAdapterModule_1 = NbCdkAdapterModule;
    NbCdkAdapterModule.forRoot = function () {
        return {
            ngModule: NbCdkAdapterModule_1,
            providers: [
                NbViewportRulerAdapter,
                NbOverlayContainerAdapter,
                NbBlockScrollStrategyAdapter,
                NbScrollDispatcherAdapter,
                NbScrollStrategyOptions,
                { provide: OverlayContainer, useExisting: NbOverlayContainerAdapter },
                { provide: NbOverlayContainer, useExisting: NbOverlayContainerAdapter },
                { provide: ScrollDispatcher, useExisting: NbScrollDispatcherAdapter },
                { provide: ScrollStrategyOptions, useExisting: NbScrollStrategyOptions },
            ],
        };
    };
    var NbCdkAdapterModule_1;
    NbCdkAdapterModule = NbCdkAdapterModule_1 = __decorate([
        NgModule({})
    ], NbCdkAdapterModule);
    return NbCdkAdapterModule;
}());
export { NbCdkAdapterModule };
//# sourceMappingURL=adapter.module.js.map