import { __decorate, __extends, __spreadArrays } from "tslib";
import { Directive, Injectable, NgModule, } from '@angular/core';
import { CdkPortal, CdkPortalOutlet, ComponentPortal, PortalInjector, PortalModule, TemplatePortal, } from '@angular/cdk/portal';
import { FlexibleConnectedPositionStrategy, Overlay, OverlayContainer, OverlayModule, OverlayPositionBuilder, } from '@angular/cdk/overlay';
var NbPortalDirective = /** @class */ (function (_super) {
    __extends(NbPortalDirective, _super);
    function NbPortalDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPortalDirective = __decorate([
        Directive({ selector: '[nbPortal]' })
    ], NbPortalDirective);
    return NbPortalDirective;
}(CdkPortal));
export { NbPortalDirective };
var NbPortalOutletDirective = /** @class */ (function (_super) {
    __extends(NbPortalOutletDirective, _super);
    function NbPortalOutletDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPortalOutletDirective = __decorate([
        Directive({ selector: '[nbPortalOutlet]' })
    ], NbPortalOutletDirective);
    return NbPortalOutletDirective;
}(CdkPortalOutlet));
export { NbPortalOutletDirective };
var NbComponentPortal = /** @class */ (function (_super) {
    __extends(NbComponentPortal, _super);
    function NbComponentPortal() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbComponentPortal;
}(ComponentPortal));
export { NbComponentPortal };
var NbOverlay = /** @class */ (function (_super) {
    __extends(NbOverlay, _super);
    function NbOverlay() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlay = __decorate([
        Injectable()
    ], NbOverlay);
    return NbOverlay;
}(Overlay));
export { NbOverlay };
var NbOverlayPositionBuilder = /** @class */ (function (_super) {
    __extends(NbOverlayPositionBuilder, _super);
    function NbOverlayPositionBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayPositionBuilder = __decorate([
        Injectable()
    ], NbOverlayPositionBuilder);
    return NbOverlayPositionBuilder;
}(OverlayPositionBuilder));
export { NbOverlayPositionBuilder };
var NbTemplatePortal = /** @class */ (function (_super) {
    __extends(NbTemplatePortal, _super);
    function NbTemplatePortal(template, viewContainerRef, context) {
        return _super.call(this, template, viewContainerRef, context) || this;
    }
    return NbTemplatePortal;
}(TemplatePortal));
export { NbTemplatePortal };
var NbOverlayContainer = /** @class */ (function (_super) {
    __extends(NbOverlayContainer, _super);
    function NbOverlayContainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayContainer = __decorate([
        Injectable()
    ], NbOverlayContainer);
    return NbOverlayContainer;
}(OverlayContainer));
export { NbOverlayContainer };
var NbFlexibleConnectedPositionStrategy = /** @class */ (function (_super) {
    __extends(NbFlexibleConnectedPositionStrategy, _super);
    function NbFlexibleConnectedPositionStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbFlexibleConnectedPositionStrategy;
}(FlexibleConnectedPositionStrategy));
export { NbFlexibleConnectedPositionStrategy };
var NbPortalInjector = /** @class */ (function (_super) {
    __extends(NbPortalInjector, _super);
    function NbPortalInjector() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbPortalInjector;
}(PortalInjector));
export { NbPortalInjector };
var CDK_MODULES = [OverlayModule, PortalModule];
/**
 * This module helps us to keep all angular/cdk deps inside our cdk module via providing aliases.
 * Approach will help us move cdk in separate npm package and refactor nebular/theme code.
 * */
var NbCdkMappingModule = /** @class */ (function () {
    function NbCdkMappingModule() {
    }
    NbCdkMappingModule_1 = NbCdkMappingModule;
    NbCdkMappingModule.forRoot = function () {
        return {
            ngModule: NbCdkMappingModule_1,
            providers: [
                NbOverlay,
                NbOverlayPositionBuilder,
            ],
        };
    };
    var NbCdkMappingModule_1;
    NbCdkMappingModule = NbCdkMappingModule_1 = __decorate([
        NgModule({
            imports: __spreadArrays(CDK_MODULES),
            exports: __spreadArrays(CDK_MODULES, [
                NbPortalDirective,
                NbPortalOutletDirective,
            ]),
            declarations: [NbPortalDirective, NbPortalOutletDirective],
        })
    ], NbCdkMappingModule);
    return NbCdkMappingModule;
}());
export { NbCdkMappingModule };
//# sourceMappingURL=mapping.js.map