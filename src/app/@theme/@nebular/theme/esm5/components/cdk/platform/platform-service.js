import { __decorate, __extends } from "tslib";
import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/platform";
var NbPlatform = /** @class */ (function (_super) {
    __extends(NbPlatform, _super);
    function NbPlatform() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPlatform.ɵprov = i0.ɵɵdefineInjectable({ factory: function NbPlatform_Factory() { return new i1.Platform(i0.ɵɵinject(i0.PLATFORM_ID, 8)); }, token: NbPlatform, providedIn: "root" });
    NbPlatform = __decorate([
        Injectable({
            providedIn: 'root',
            useClass: Platform,
        })
    ], NbPlatform);
    return NbPlatform;
}(Platform));
export { NbPlatform };
//# sourceMappingURL=platform-service.js.map