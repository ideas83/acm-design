import { __decorate, __extends } from "tslib";
import { Injectable } from '@angular/core';
import { Directionality } from '@angular/cdk/bidi';
var NbDirectionality = /** @class */ (function (_super) {
    __extends(NbDirectionality, _super);
    function NbDirectionality() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbDirectionality = __decorate([
        Injectable()
    ], NbDirectionality);
    return NbDirectionality;
}(Directionality));
export { NbDirectionality };
//# sourceMappingURL=bidi-service.js.map