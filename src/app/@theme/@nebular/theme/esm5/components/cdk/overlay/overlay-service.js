import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { NbComponentPortal, NbOverlay, } from './mapping';
import { NbLayoutDirectionService } from '../../../services/direction.service';
export function patch(container, containerContext) {
    Object.assign(container.instance, containerContext);
    container.changeDetectorRef.detectChanges();
    return container;
}
export function createContainer(ref, container, context, componentFactoryResolver) {
    var containerRef = ref.attach(new NbComponentPortal(container, null, null, componentFactoryResolver));
    patch(containerRef, context);
    return containerRef;
}
var NbOverlayService = /** @class */ (function () {
    function NbOverlayService(overlay, layoutDirection) {
        this.overlay = overlay;
        this.layoutDirection = layoutDirection;
    }
    Object.defineProperty(NbOverlayService.prototype, "scrollStrategies", {
        get: function () {
            return this.overlay.scrollStrategies;
        },
        enumerable: true,
        configurable: true
    });
    NbOverlayService.prototype.create = function (config) {
        var overlayRef = this.overlay.create(config);
        this.layoutDirection.onDirectionChange()
            .subscribe(function (dir) { return overlayRef.setDirection(dir); });
        return overlayRef;
    };
    NbOverlayService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NbOverlay, NbLayoutDirectionService])
    ], NbOverlayService);
    return NbOverlayService;
}());
export { NbOverlayService };
//# sourceMappingURL=overlay-service.js.map