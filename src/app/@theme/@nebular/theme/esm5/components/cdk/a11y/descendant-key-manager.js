import { __extends } from "tslib";
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
var NbActiveDescendantKeyManager = /** @class */ (function (_super) {
    __extends(NbActiveDescendantKeyManager, _super);
    function NbActiveDescendantKeyManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbActiveDescendantKeyManager;
}(ActiveDescendantKeyManager));
export { NbActiveDescendantKeyManager };
var NbActiveDescendantKeyManagerFactoryService = /** @class */ (function () {
    function NbActiveDescendantKeyManagerFactoryService() {
    }
    NbActiveDescendantKeyManagerFactoryService.prototype.create = function (items) {
        return new NbActiveDescendantKeyManager(items);
    };
    return NbActiveDescendantKeyManagerFactoryService;
}());
export { NbActiveDescendantKeyManagerFactoryService };
export var NbKeyManagerActiveItemMode;
(function (NbKeyManagerActiveItemMode) {
    NbKeyManagerActiveItemMode[NbKeyManagerActiveItemMode["RESET_ACTIVE"] = -1] = "RESET_ACTIVE";
    NbKeyManagerActiveItemMode[NbKeyManagerActiveItemMode["FIRST_ACTIVE"] = 0] = "FIRST_ACTIVE";
})(NbKeyManagerActiveItemMode || (NbKeyManagerActiveItemMode = {}));
//# sourceMappingURL=descendant-key-manager.js.map