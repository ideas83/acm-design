import { __decorate, __extends, __metadata } from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { ViewportRuler } from '@angular/cdk/overlay';
import { map } from 'rxjs/operators';
import { NbPlatform } from '../platform/platform-service';
import { NbLayoutRulerService } from '../../../services/ruler.service';
import { NbLayoutScrollService } from '../../../services/scroll.service';
var NbViewportRulerAdapter = /** @class */ (function (_super) {
    __extends(NbViewportRulerAdapter, _super);
    function NbViewportRulerAdapter(platform, ngZone, ruler, scroll) {
        var _this = _super.call(this, platform, ngZone) || this;
        _this.ruler = ruler;
        _this.scroll = scroll;
        return _this;
    }
    NbViewportRulerAdapter.prototype.getViewportSize = function () {
        var res;
        /*
        * getDimensions call is really synchronous operation.
        * And we have to conform with the interface of the original service.
        * */
        this.ruler.getDimensions()
            .pipe(map(function (dimensions) { return ({ width: dimensions.clientWidth, height: dimensions.clientHeight }); }))
            .subscribe(function (rect) { return res = rect; });
        return res;
    };
    NbViewportRulerAdapter.prototype.getViewportScrollPosition = function () {
        var res;
        /*
        * getPosition call is really synchronous operation.
        * And we have to conform with the interface of the original service.
        * */
        this.scroll.getPosition()
            .pipe(map(function (position) { return ({ top: position.y, left: position.x }); }))
            .subscribe(function (position) { return res = position; });
        return res;
    };
    NbViewportRulerAdapter = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [NbPlatform, NgZone,
            NbLayoutRulerService,
            NbLayoutScrollService])
    ], NbViewportRulerAdapter);
    return NbViewportRulerAdapter;
}(ViewportRuler));
export { NbViewportRulerAdapter };
//# sourceMappingURL=viewport-ruler-adapter.js.map