import { __decorate, __extends, __metadata, __param } from "tslib";
import { Inject, Injectable, NgZone } from '@angular/core';
import { BlockScrollStrategy, ScrollDispatcher, ScrollStrategyOptions } from '@angular/cdk/overlay';
import { NbLayoutScrollService } from '../../../services/scroll.service';
import { NB_DOCUMENT } from '../../../theme.options';
import { NbViewportRulerAdapter } from './viewport-ruler-adapter';
/**
 * Overrides default block scroll strategy because default strategy blocks scrolling on the body only.
 * But Nebular has its own scrollable container - nb-layout. So, we need to block scrolling in it to.
 * */
var NbBlockScrollStrategyAdapter = /** @class */ (function (_super) {
    __extends(NbBlockScrollStrategyAdapter, _super);
    function NbBlockScrollStrategyAdapter(document, viewportRuler, scrollService) {
        var _this = _super.call(this, viewportRuler, document) || this;
        _this.scrollService = scrollService;
        return _this;
    }
    NbBlockScrollStrategyAdapter.prototype.enable = function () {
        _super.prototype.enable.call(this);
        this.scrollService.scrollable(false);
    };
    NbBlockScrollStrategyAdapter.prototype.disable = function () {
        _super.prototype.disable.call(this);
        this.scrollService.scrollable(true);
    };
    NbBlockScrollStrategyAdapter = __decorate([
        Injectable(),
        __param(0, Inject(NB_DOCUMENT)),
        __metadata("design:paramtypes", [Object, NbViewportRulerAdapter,
            NbLayoutScrollService])
    ], NbBlockScrollStrategyAdapter);
    return NbBlockScrollStrategyAdapter;
}(BlockScrollStrategy));
export { NbBlockScrollStrategyAdapter };
var NbScrollStrategyOptions = /** @class */ (function (_super) {
    __extends(NbScrollStrategyOptions, _super);
    function NbScrollStrategyOptions(scrollService, scrollDispatcher, viewportRuler, ngZone, document) {
        var _this = _super.call(this, scrollDispatcher, viewportRuler, ngZone, document) || this;
        _this.scrollService = scrollService;
        _this.scrollDispatcher = scrollDispatcher;
        _this.viewportRuler = viewportRuler;
        _this.ngZone = ngZone;
        _this.document = document;
        _this.block = function () { return new NbBlockScrollStrategyAdapter(_this.document, _this.viewportRuler, _this.scrollService); };
        return _this;
    }
    NbScrollStrategyOptions = __decorate([
        Injectable(),
        __param(4, Inject(NB_DOCUMENT)),
        __metadata("design:paramtypes", [NbLayoutScrollService,
            ScrollDispatcher,
            NbViewportRulerAdapter,
            NgZone, Object])
    ], NbScrollStrategyOptions);
    return NbScrollStrategyOptions;
}(ScrollStrategyOptions));
export { NbScrollStrategyOptions };
//# sourceMappingURL=block-scroll-strategy-adapter.js.map