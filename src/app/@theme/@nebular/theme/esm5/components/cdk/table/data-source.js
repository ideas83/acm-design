import { __extends } from "tslib";
import { DataSource } from '@angular/cdk/table';
var NbDataSource = /** @class */ (function (_super) {
    __extends(NbDataSource, _super);
    function NbDataSource() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbDataSource;
}(DataSource));
export { NbDataSource };
//# sourceMappingURL=data-source.js.map