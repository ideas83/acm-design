import { __decorate, __metadata, __param } from "tslib";
import { InjectionToken, Optional, Inject, Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { share } from 'rxjs/operators';
/**
 * Layout direction.
 * */
export var NbLayoutDirection;
(function (NbLayoutDirection) {
    NbLayoutDirection["LTR"] = "ltr";
    NbLayoutDirection["RTL"] = "rtl";
})(NbLayoutDirection || (NbLayoutDirection = {}));
;
/**
 * Layout direction setting injection token.
 * */
export var NB_LAYOUT_DIRECTION = new InjectionToken('Layout direction');
/**
 * Layout Direction Service.
 * Allows to set or get layout direction and listen to its changes
 */
var NbLayoutDirectionService = /** @class */ (function () {
    function NbLayoutDirectionService(direction) {
        if (direction === void 0) { direction = NbLayoutDirection.LTR; }
        this.direction = direction;
        this.$directionChange = new ReplaySubject(1);
        this.setDirection(direction);
    }
    /**
     * Returns true if layout direction set to left to right.
     * @returns boolean.
     * */
    NbLayoutDirectionService.prototype.isLtr = function () {
        return this.direction === NbLayoutDirection.LTR;
    };
    /**
     * Returns true if layout direction set to right to left.
     * @returns boolean.
     * */
    NbLayoutDirectionService.prototype.isRtl = function () {
        return this.direction === NbLayoutDirection.RTL;
    };
    /**
     * Returns current layout direction.
     * @returns NbLayoutDirection.
     * */
    NbLayoutDirectionService.prototype.getDirection = function () {
        return this.direction;
    };
    /**
     * Sets layout direction
     * @param {NbLayoutDirection} direction
     */
    NbLayoutDirectionService.prototype.setDirection = function (direction) {
        this.direction = direction;
        this.$directionChange.next(direction);
    };
    /**
     * Triggered when direction was changed.
     * @returns Observable<NbLayoutDirection>.
     */
    NbLayoutDirectionService.prototype.onDirectionChange = function () {
        return this.$directionChange.pipe(share());
    };
    NbLayoutDirectionService = __decorate([
        Injectable(),
        __param(0, Optional()), __param(0, Inject(NB_LAYOUT_DIRECTION)),
        __metadata("design:paramtypes", [Object])
    ], NbLayoutDirectionService);
    return NbLayoutDirectionService;
}());
export { NbLayoutDirectionService };
//# sourceMappingURL=direction.service.js.map