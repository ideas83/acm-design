import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
/**
 * Simple helper service to return Layout dimensions
 * Depending of current Layout scroll mode (default or `withScroll` when scroll is moved to an element
 * inside of the layout) corresponding dimensions will be returns  - of `documentElement` in first case and
 * `.scrollable-container` in the second.
 */
let NbLayoutRulerService = class NbLayoutRulerService {
    constructor() {
        this.contentDimensionsReq$ = new Subject();
    }
    /**
     * Content dimensions
     * @returns {Observable<NbLayoutDimensions>}
     */
    getDimensions() {
        return Observable.create((observer) => {
            const listener = new Subject();
            listener.subscribe(observer);
            this.contentDimensionsReq$.next({ listener });
            return () => listener.complete();
        });
    }
    /**
     * @private
     * @returns {Subject<any>}
     */
    onGetDimensions() {
        return this.contentDimensionsReq$;
    }
};
NbLayoutRulerService = __decorate([
    Injectable()
], NbLayoutRulerService);
export { NbLayoutRulerService };
//# sourceMappingURL=ruler.service.js.map