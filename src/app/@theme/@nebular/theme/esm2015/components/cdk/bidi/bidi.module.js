import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { BidiModule, Directionality } from '@angular/cdk/bidi';
import { NbDirectionality } from './bidi-service';
let NbBidiModule = class NbBidiModule extends BidiModule {
};
NbBidiModule = __decorate([
    NgModule({
        providers: [
            { provide: NbDirectionality, useExisting: Directionality },
        ],
    })
], NbBidiModule);
export { NbBidiModule };
//# sourceMappingURL=bidi.module.js.map