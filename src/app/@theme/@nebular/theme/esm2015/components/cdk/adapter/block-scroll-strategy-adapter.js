import { __decorate, __metadata, __param } from "tslib";
import { Inject, Injectable, NgZone } from '@angular/core';
import { BlockScrollStrategy, ScrollDispatcher, ScrollStrategyOptions } from '@angular/cdk/overlay';
import { NbLayoutScrollService } from '../../../services/scroll.service';
import { NB_DOCUMENT } from '../../../theme.options';
import { NbViewportRulerAdapter } from './viewport-ruler-adapter';
/**
 * Overrides default block scroll strategy because default strategy blocks scrolling on the body only.
 * But Nebular has its own scrollable container - nb-layout. So, we need to block scrolling in it to.
 * */
let NbBlockScrollStrategyAdapter = class NbBlockScrollStrategyAdapter extends BlockScrollStrategy {
    constructor(document, viewportRuler, scrollService) {
        super(viewportRuler, document);
        this.scrollService = scrollService;
    }
    enable() {
        super.enable();
        this.scrollService.scrollable(false);
    }
    disable() {
        super.disable();
        this.scrollService.scrollable(true);
    }
};
NbBlockScrollStrategyAdapter = __decorate([
    Injectable(),
    __param(0, Inject(NB_DOCUMENT)),
    __metadata("design:paramtypes", [Object, NbViewportRulerAdapter,
        NbLayoutScrollService])
], NbBlockScrollStrategyAdapter);
export { NbBlockScrollStrategyAdapter };
let NbScrollStrategyOptions = class NbScrollStrategyOptions extends ScrollStrategyOptions {
    constructor(scrollService, scrollDispatcher, viewportRuler, ngZone, document) {
        super(scrollDispatcher, viewportRuler, ngZone, document);
        this.scrollService = scrollService;
        this.scrollDispatcher = scrollDispatcher;
        this.viewportRuler = viewportRuler;
        this.ngZone = ngZone;
        this.document = document;
        this.block = () => new NbBlockScrollStrategyAdapter(this.document, this.viewportRuler, this.scrollService);
    }
};
NbScrollStrategyOptions = __decorate([
    Injectable(),
    __param(4, Inject(NB_DOCUMENT)),
    __metadata("design:paramtypes", [NbLayoutScrollService,
        ScrollDispatcher,
        NbViewportRulerAdapter,
        NgZone, Object])
], NbScrollStrategyOptions);
export { NbScrollStrategyOptions };
//# sourceMappingURL=block-scroll-strategy-adapter.js.map