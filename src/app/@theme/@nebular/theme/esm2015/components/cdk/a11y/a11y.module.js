var NbA11yModule_1;
import { __decorate } from "tslib";
import { NgModule, Injectable } from '@angular/core';
import { NbFocusTrapFactoryService } from './focus-trap';
import { NbFocusKeyManagerFactoryService } from './focus-key-manager';
import { NbActiveDescendantKeyManagerFactoryService } from './descendant-key-manager';
import { FocusMonitor } from '@angular/cdk/a11y';
let NbFocusMonitor = class NbFocusMonitor extends FocusMonitor {
};
NbFocusMonitor = __decorate([
    Injectable()
], NbFocusMonitor);
export { NbFocusMonitor };
let NbA11yModule = NbA11yModule_1 = class NbA11yModule {
    static forRoot() {
        return {
            ngModule: NbA11yModule_1,
            providers: [
                NbFocusTrapFactoryService,
                NbFocusKeyManagerFactoryService,
                NbActiveDescendantKeyManagerFactoryService,
                { provide: NbFocusMonitor, useClass: FocusMonitor },
            ],
        };
    }
};
NbA11yModule = NbA11yModule_1 = __decorate([
    NgModule({})
], NbA11yModule);
export { NbA11yModule };
//# sourceMappingURL=a11y.module.js.map