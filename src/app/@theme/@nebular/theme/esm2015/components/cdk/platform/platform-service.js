import { __decorate } from "tslib";
import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/platform";
let NbPlatform = class NbPlatform extends Platform {
};
NbPlatform.ɵprov = i0.ɵɵdefineInjectable({ factory: function NbPlatform_Factory() { return new i1.Platform(i0.ɵɵinject(i0.PLATFORM_ID, 8)); }, token: NbPlatform, providedIn: "root" });
NbPlatform = __decorate([
    Injectable({
        providedIn: 'root',
        useClass: Platform,
    })
], NbPlatform);
export { NbPlatform };
//# sourceMappingURL=platform-service.js.map