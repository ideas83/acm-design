var NbCdkMappingModule_1;
import { __decorate } from "tslib";
import { Directive, Injectable, NgModule, } from '@angular/core';
import { CdkPortal, CdkPortalOutlet, ComponentPortal, PortalInjector, PortalModule, TemplatePortal, } from '@angular/cdk/portal';
import { FlexibleConnectedPositionStrategy, Overlay, OverlayContainer, OverlayModule, OverlayPositionBuilder, } from '@angular/cdk/overlay';
let NbPortalDirective = class NbPortalDirective extends CdkPortal {
};
NbPortalDirective = __decorate([
    Directive({ selector: '[nbPortal]' })
], NbPortalDirective);
export { NbPortalDirective };
let NbPortalOutletDirective = class NbPortalOutletDirective extends CdkPortalOutlet {
};
NbPortalOutletDirective = __decorate([
    Directive({ selector: '[nbPortalOutlet]' })
], NbPortalOutletDirective);
export { NbPortalOutletDirective };
export class NbComponentPortal extends ComponentPortal {
}
let NbOverlay = class NbOverlay extends Overlay {
};
NbOverlay = __decorate([
    Injectable()
], NbOverlay);
export { NbOverlay };
let NbOverlayPositionBuilder = class NbOverlayPositionBuilder extends OverlayPositionBuilder {
};
NbOverlayPositionBuilder = __decorate([
    Injectable()
], NbOverlayPositionBuilder);
export { NbOverlayPositionBuilder };
export class NbTemplatePortal extends TemplatePortal {
    constructor(template, viewContainerRef, context) {
        super(template, viewContainerRef, context);
    }
}
let NbOverlayContainer = class NbOverlayContainer extends OverlayContainer {
};
NbOverlayContainer = __decorate([
    Injectable()
], NbOverlayContainer);
export { NbOverlayContainer };
export class NbFlexibleConnectedPositionStrategy extends FlexibleConnectedPositionStrategy {
}
export class NbPortalInjector extends PortalInjector {
}
const CDK_MODULES = [OverlayModule, PortalModule];
/**
 * This module helps us to keep all angular/cdk deps inside our cdk module via providing aliases.
 * Approach will help us move cdk in separate npm package and refactor nebular/theme code.
 * */
let NbCdkMappingModule = NbCdkMappingModule_1 = class NbCdkMappingModule {
    static forRoot() {
        return {
            ngModule: NbCdkMappingModule_1,
            providers: [
                NbOverlay,
                NbOverlayPositionBuilder,
            ],
        };
    }
};
NbCdkMappingModule = NbCdkMappingModule_1 = __decorate([
    NgModule({
        imports: [...CDK_MODULES],
        exports: [
            ...CDK_MODULES,
            NbPortalDirective,
            NbPortalOutletDirective,
        ],
        declarations: [NbPortalDirective, NbPortalOutletDirective],
    })
], NbCdkMappingModule);
export { NbCdkMappingModule };
//# sourceMappingURL=mapping.js.map