import { __decorate, __metadata, __param } from "tslib";
import { Attribute, ChangeDetectorRef, ElementRef, Inject, IterableDiffers, NgModule, Component } from '@angular/core';
import { CdkTable, CdkTableModule } from '@angular/cdk/table';
import { NbBidiModule } from '../bidi/bidi.module';
import { NbDirectionality } from '../bidi/bidi-service';
import { NbPlatform } from '../platform/platform-service';
import { NB_DOCUMENT } from '../../../theme.options';
import { NbCellDefDirective, NbCellDirective, NbColumnDefDirective, NbFooterCellDefDirective, NbFooterCellDirective, NbHeaderCellDefDirective, NbHeaderCellDirective, } from './cell';
import { NbCellOutletDirective, NbDataRowOutletDirective, NbFooterRowOutletDirective, NbHeaderRowOutletDirective, NbFooterRowComponent, NbFooterRowDefDirective, NbHeaderRowComponent, NbHeaderRowDefDirective, NbRowComponent, NbRowDefDirective, } from './row';
export const NB_TABLE_TEMPLATE = `
  <ng-container nbHeaderRowOutlet></ng-container>
  <ng-container nbRowOutlet></ng-container>
  <ng-container nbFooterRowOutlet></ng-container>`;
let NbTable = 
// tslint:disable-next-line:component-class-suffix
class NbTable extends CdkTable {
    constructor(differs, changeDetectorRef, elementRef, role, dir, document, platform) {
        super(differs, changeDetectorRef, elementRef, role, dir, document, platform);
    }
};
NbTable = __decorate([
    Component({
        selector: 'nb-table-not-implemented',
        template: ``
    })
    // tslint:disable-next-line:component-class-suffix
    ,
    __param(3, Attribute('role')),
    __param(5, Inject(NB_DOCUMENT)),
    __metadata("design:paramtypes", [IterableDiffers,
        ChangeDetectorRef,
        ElementRef, String, NbDirectionality, Object, NbPlatform])
], NbTable);
export { NbTable };
const COMPONENTS = [
    NbTable,
    // Template defs
    NbHeaderCellDefDirective,
    NbHeaderRowDefDirective,
    NbColumnDefDirective,
    NbCellDefDirective,
    NbRowDefDirective,
    NbFooterCellDefDirective,
    NbFooterRowDefDirective,
    // Outlets
    NbDataRowOutletDirective,
    NbHeaderRowOutletDirective,
    NbFooterRowOutletDirective,
    NbCellOutletDirective,
    // Cell directives
    NbHeaderCellDirective,
    NbCellDirective,
    NbFooterCellDirective,
    // Row directives
    NbHeaderRowComponent,
    NbRowComponent,
    NbFooterRowComponent,
];
let NbTableModule = class NbTableModule extends CdkTableModule {
};
NbTableModule = __decorate([
    NgModule({
        imports: [NbBidiModule],
        declarations: [...COMPONENTS],
        exports: [...COMPONENTS],
    })
], NbTableModule);
export { NbTableModule };
//# sourceMappingURL=table.module.js.map