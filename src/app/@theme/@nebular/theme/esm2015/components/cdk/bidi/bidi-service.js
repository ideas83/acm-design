import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Directionality } from '@angular/cdk/bidi';
let NbDirectionality = class NbDirectionality extends Directionality {
};
NbDirectionality = __decorate([
    Injectable()
], NbDirectionality);
export { NbDirectionality };
//# sourceMappingURL=bidi-service.js.map