import { __decorate, __metadata } from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { NbPlatform } from '../platform/platform-service';
import { NbLayoutScrollService } from '../../../services/scroll.service';
let NbScrollDispatcherAdapter = class NbScrollDispatcherAdapter extends ScrollDispatcher {
    constructor(ngZone, platform, scrollService) {
        super(ngZone, platform);
        this.scrollService = scrollService;
    }
    scrolled(auditTimeInMs) {
        return this.scrollService.onScroll();
    }
};
NbScrollDispatcherAdapter = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [NgZone, NbPlatform, NbLayoutScrollService])
], NbScrollDispatcherAdapter);
export { NbScrollDispatcherAdapter };
//# sourceMappingURL=scroll-dispatcher-adapter.js.map