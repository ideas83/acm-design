/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, EventEmitter, Output } from '@angular/core';
import { NbLayoutDirectionService } from '../../../../services/direction.service';
let NbCalendarPageableNavigationComponent = class NbCalendarPageableNavigationComponent {
    constructor(directionService) {
        this.directionService = directionService;
        this.next = new EventEmitter();
        this.prev = new EventEmitter();
    }
    get isLtr() {
        return this.directionService.isLtr();
    }
};
__decorate([
    Output(),
    __metadata("design:type", Object)
], NbCalendarPageableNavigationComponent.prototype, "next", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], NbCalendarPageableNavigationComponent.prototype, "prev", void 0);
NbCalendarPageableNavigationComponent = __decorate([
    Component({
        selector: 'nb-calendar-pageable-navigation',
        template: `
    <button nbButton (click)="prev.emit()" ghost status="basic" class="prev-month">
      <nb-icon [icon]="isLtr ? 'chevron-left-outline' : 'chevron-right-outline'" pack="nebular-essentials"></nb-icon>
    </button>
    <button nbButton (click)="next.emit()" ghost status="basic" class="next-month">
      <nb-icon [icon]="isLtr ? 'chevron-right-outline' : 'chevron-left-outline'" pack="nebular-essentials"></nb-icon>
    </button>
  `,
        styles: [":host{display:flex;align-items:center;justify-content:flex-start}\n"]
    }),
    __metadata("design:paramtypes", [NbLayoutDirectionService])
], NbCalendarPageableNavigationComponent);
export { NbCalendarPageableNavigationComponent };
//# sourceMappingURL=calendar-pageable-navigation.component.js.map