/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, Type, } from '@angular/core';
import { batch } from '../../helpers';
import { NbCalendarSize } from '../../model';
import { NbCalendarMonthCellComponent } from './calendar-month-cell.component';
import { NbDateService } from '../../services/date.service';
export const MONTHS_IN_VIEW = 12;
export const MONTHS_IN_COLUMN = 4;
let NbCalendarMonthPickerComponent = class NbCalendarMonthPickerComponent {
    constructor(dateService) {
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.monthChange = new EventEmitter();
        this.cellComponent = NbCalendarMonthCellComponent;
    }
    set _cellComponent(cellComponent) {
        if (cellComponent) {
            this.cellComponent = cellComponent;
        }
    }
    get large() {
        return this.size === NbCalendarSize.LARGE;
    }
    ngOnChanges(changes) {
        if (changes.month) {
            this.initMonths();
        }
    }
    initMonths() {
        const date = this.dateService.getDate(this.month);
        const year = this.dateService.getYear(this.month);
        const firstMonth = this.dateService.createDate(year, 0, date);
        const months = [firstMonth];
        for (let monthIndex = 1; monthIndex < MONTHS_IN_VIEW; monthIndex++) {
            months.push(this.dateService.addMonth(firstMonth, monthIndex));
        }
        this.months = batch(months, MONTHS_IN_COLUMN);
    }
    onSelect(month) {
        this.monthChange.emit(month);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarMonthPickerComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarMonthPickerComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], NbCalendarMonthPickerComponent.prototype, "filter", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarMonthPickerComponent.prototype, "size", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarMonthPickerComponent.prototype, "month", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarMonthPickerComponent.prototype, "date", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarMonthPickerComponent.prototype, "monthChange", void 0);
__decorate([
    Input('cellComponent'),
    __metadata("design:type", Type),
    __metadata("design:paramtypes", [Type])
], NbCalendarMonthPickerComponent.prototype, "_cellComponent", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [])
], NbCalendarMonthPickerComponent.prototype, "large", null);
NbCalendarMonthPickerComponent = __decorate([
    Component({
        selector: 'nb-calendar-month-picker',
        template: `
    <nb-calendar-picker
      [data]="months"
      [min]="min"
      [max]="max"
      [filter]="filter"
      [selectedValue]="date"
      [visibleDate]="month"
      [cellComponent]="cellComponent"
      [size]="size"
      (select)="onSelect($event)">
    </nb-calendar-picker>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbDateService])
], NbCalendarMonthPickerComponent);
export { NbCalendarMonthPickerComponent };
//# sourceMappingURL=calendar-month-picker.component.js.map