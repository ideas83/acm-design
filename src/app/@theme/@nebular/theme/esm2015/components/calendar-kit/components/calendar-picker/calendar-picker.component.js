/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, Type, HostBinding } from '@angular/core';
import { NbCalendarSize } from '../../model';
let NbCalendarPickerComponent = class NbCalendarPickerComponent {
    constructor() {
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter();
    }
    get isLarge() {
        return this.size === NbCalendarSize.LARGE;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Array)
], NbCalendarPickerComponent.prototype, "data", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerComponent.prototype, "visibleDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", Type)
], NbCalendarPickerComponent.prototype, "cellComponent", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], NbCalendarPickerComponent.prototype, "filter", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarPickerComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarPickerComponent.prototype, "select", void 0);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarPickerComponent.prototype, "isLarge", null);
NbCalendarPickerComponent = __decorate([
    Component({
        selector: 'nb-calendar-picker',
        template: `
    <nb-calendar-picker-row
      *ngFor="let row of data"
      [row]="row"
      [visibleDate]="visibleDate"
      [selectedValue]="selectedValue"
      [component]="cellComponent"
      [min]="min"
      [max]="max"
      [filter]="filter"
      [size]="size"
      (select)="select.emit($event)">
    </nb-calendar-picker-row>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    })
], NbCalendarPickerComponent);
export { NbCalendarPickerComponent };
//# sourceMappingURL=calendar-picker.component.js.map