/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbDateService } from '../../services/date.service';
import { NbCalendarSize } from '../../model';
let NbCalendarYearCellComponent = class NbCalendarYearCellComponent {
    constructor(dateService) {
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter(true);
        this.yearCellClass = true;
    }
    get selected() {
        return this.dateService.isSameYearSafe(this.date, this.selectedValue);
    }
    get today() {
        return this.dateService.isSameYearSafe(this.date, this.dateService.today());
    }
    get disabled() {
        return this.smallerThanMin() || this.greaterThanMax();
    }
    get isLarge() {
        return this.size === NbCalendarSize.LARGE;
    }
    get year() {
        return this.dateService.getYear(this.date);
    }
    onClick() {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    }
    smallerThanMin() {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    }
    greaterThanMax() {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    }
    yearStart() {
        return this.dateService.getYearStart(this.date);
    }
    yearEnd() {
        return this.dateService.getYearEnd(this.date);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarYearCellComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarYearCellComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarYearCellComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarYearCellComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarYearCellComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarYearCellComponent.prototype, "select", void 0);
__decorate([
    HostBinding('class.selected'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarYearCellComponent.prototype, "selected", null);
__decorate([
    HostBinding('class.today'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarYearCellComponent.prototype, "today", null);
__decorate([
    HostBinding('class.disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarYearCellComponent.prototype, "disabled", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarYearCellComponent.prototype, "isLarge", null);
__decorate([
    HostBinding('class.year-cell'),
    __metadata("design:type", Object)
], NbCalendarYearCellComponent.prototype, "yearCellClass", void 0);
__decorate([
    HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], NbCalendarYearCellComponent.prototype, "onClick", null);
NbCalendarYearCellComponent = __decorate([
    Component({
        selector: 'nb-calendar-year-cell',
        template: `
    <div class="cell-content">
      {{ year }}
    </div>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbDateService])
], NbCalendarYearCellComponent);
export { NbCalendarYearCellComponent };
//# sourceMappingURL=calendar-year-cell.component.js.map