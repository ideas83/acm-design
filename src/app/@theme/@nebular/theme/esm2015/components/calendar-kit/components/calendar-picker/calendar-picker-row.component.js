/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, EventEmitter, Input, Output, TemplateRef, Type, ViewChild, ViewContainerRef, } from '@angular/core';
import { NbCalendarSize } from '../../model';
let NbCalendarPickerRowComponent = class NbCalendarPickerRowComponent {
    constructor(cfr) {
        this.cfr = cfr;
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter();
    }
    ngOnChanges() {
        const factory = this.cfr.resolveComponentFactory(this.component);
        this.containerRef.clear();
        this.row.forEach((date) => {
            const component = this.containerRef.createComponent(factory);
            this.patchWithContext(component.instance, date);
            component.changeDetectorRef.detectChanges();
        });
    }
    patchWithContext(component, date) {
        component.visibleDate = this.visibleDate;
        component.selectedValue = this.selectedValue;
        component.date = date;
        component.min = this.min;
        component.max = this.max;
        component.filter = this.filter;
        component.size = this.size;
        component.select.subscribe(this.select.emit.bind(this.select));
    }
};
__decorate([
    Input(),
    __metadata("design:type", Array)
], NbCalendarPickerRowComponent.prototype, "row", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerRowComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerRowComponent.prototype, "visibleDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Type)
], NbCalendarPickerRowComponent.prototype, "component", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerRowComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarPickerRowComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], NbCalendarPickerRowComponent.prototype, "filter", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarPickerRowComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarPickerRowComponent.prototype, "select", void 0);
__decorate([
    ViewChild(TemplateRef, { read: ViewContainerRef, static: true }),
    __metadata("design:type", ViewContainerRef)
], NbCalendarPickerRowComponent.prototype, "containerRef", void 0);
NbCalendarPickerRowComponent = __decorate([
    Component({
        selector: 'nb-calendar-picker-row',
        template: '<ng-template></ng-template>',
        changeDetection: ChangeDetectionStrategy.OnPush,
        styles: [`
    :host {
      display: flex;
      justify-content: space-between;
    }
  `]
    }),
    __metadata("design:paramtypes", [ComponentFactoryResolver])
], NbCalendarPickerRowComponent);
export { NbCalendarPickerRowComponent };
//# sourceMappingURL=calendar-picker-row.component.js.map