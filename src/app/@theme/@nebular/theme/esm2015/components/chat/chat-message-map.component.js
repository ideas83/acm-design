/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NbChatOptions } from './chat.options';
/**
 * Chat message component.
 */
let NbChatMessageMapComponent = class NbChatMessageMapComponent {
    constructor(options) {
        /**
         * Message send date format, default 'shortTime'
         * @type {string}
         */
        this.dateFormat = 'shortTime';
        this.mapKey = options.messageGoogleMapKey;
    }
    get file() {
        return {
            // tslint:disable-next-line:max-line-length
            url: `https://maps.googleapis.com/maps/api/staticmap?center=${this.latitude},${this.longitude}&zoom=12&size=400x400&key=${this.mapKey}`,
            type: 'image/png',
            icon: 'location',
        };
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageMapComponent.prototype, "message", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageMapComponent.prototype, "sender", void 0);
__decorate([
    Input(),
    __metadata("design:type", Date)
], NbChatMessageMapComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageMapComponent.prototype, "dateFormat", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], NbChatMessageMapComponent.prototype, "latitude", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], NbChatMessageMapComponent.prototype, "longitude", void 0);
NbChatMessageMapComponent = __decorate([
    Component({
        selector: 'nb-chat-message-map',
        template: `
    <nb-chat-message-file [files]="[file]" [message]="message" [sender]="sender" [date]="date"
     [dateFormat]="dateFormat"></nb-chat-message-file>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbChatOptions])
], NbChatMessageMapComponent);
export { NbChatMessageMapComponent };
//# sourceMappingURL=chat-message-map.component.js.map