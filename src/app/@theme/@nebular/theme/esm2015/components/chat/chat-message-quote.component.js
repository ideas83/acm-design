/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
/**
 * Chat message component.
 */
let NbChatMessageQuoteComponent = class NbChatMessageQuoteComponent {
    constructor() {
        /**
          * Message send date format, default 'shortTime'
          * @type {string}
          */
        this.dateFormat = 'shortTime';
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageQuoteComponent.prototype, "message", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageQuoteComponent.prototype, "sender", void 0);
__decorate([
    Input(),
    __metadata("design:type", Date)
], NbChatMessageQuoteComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageQuoteComponent.prototype, "dateFormat", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbChatMessageQuoteComponent.prototype, "quote", void 0);
NbChatMessageQuoteComponent = __decorate([
    Component({
        selector: 'nb-chat-message-quote',
        template: `
    <p class="sender" *ngIf="sender || date">{{ sender }} <time>{{ date | date: dateFormat }}</time></p>
    <p class="quote">
      {{ quote }}
    </p>
    <nb-chat-message-text [message]="message">
      {{ message }}
    </nb-chat-message-text>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    })
], NbChatMessageQuoteComponent);
export { NbChatMessageQuoteComponent };
//# sourceMappingURL=chat-message-quote.component.js.map