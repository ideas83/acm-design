/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbAlertComponent } from './alert.component';
let NbAlertModule = class NbAlertModule {
};
NbAlertModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
        ],
        declarations: [
            NbAlertComponent,
        ],
        exports: [
            NbAlertComponent,
        ],
    })
], NbAlertModule);
export { NbAlertModule };
//# sourceMappingURL=alert.module.js.map