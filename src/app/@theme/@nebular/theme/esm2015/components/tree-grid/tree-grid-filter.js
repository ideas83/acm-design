/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbFilterInputDirective_1;
import { __decorate, __metadata } from "tslib";
import { Directive, HostListener, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
let NbFilterDirective = class NbFilterDirective {
    filter(filterRequest) {
        this.filterable.filter(filterRequest);
    }
};
__decorate([
    Input('nbFilter'),
    __metadata("design:type", Object)
], NbFilterDirective.prototype, "filterable", void 0);
NbFilterDirective = __decorate([
    Directive({ selector: '[nbFilter]' })
], NbFilterDirective);
export { NbFilterDirective };
/**
 * Helper directive to trigger data source's filter method when user types in input
 */
let NbFilterInputDirective = NbFilterInputDirective_1 = class NbFilterInputDirective extends NbFilterDirective {
    constructor() {
        super(...arguments);
        this.search$ = new Subject();
        this.destroy$ = new Subject();
        /**
         * Debounce time before triggering filter method. Set in milliseconds.
         * Default 200.
         */
        this.debounceTime = 200;
    }
    ngOnInit() {
        this.search$
            .pipe(debounceTime(this.debounceTime), takeUntil(this.destroy$))
            .subscribe((query) => {
            super.filter(query);
        });
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this.search$.complete();
    }
    filter(event) {
        this.search$.next(event.target.value);
    }
};
__decorate([
    Input('nbFilterInput'),
    __metadata("design:type", Object)
], NbFilterInputDirective.prototype, "filterable", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], NbFilterInputDirective.prototype, "debounceTime", void 0);
__decorate([
    HostListener('input', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], NbFilterInputDirective.prototype, "filter", null);
NbFilterInputDirective = NbFilterInputDirective_1 = __decorate([
    Directive({
        selector: '[nbFilterInput]',
        providers: [{ provide: NbFilterDirective, useExisting: NbFilterInputDirective_1 }],
    })
], NbFilterInputDirective);
export { NbFilterInputDirective };
//# sourceMappingURL=tree-grid-filter.js.map