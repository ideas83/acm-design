/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Directive, HostListener } from '@angular/core';
import { NbTreeGridCellDirective } from './tree-grid-cell.component';
/**
 * When using custom row toggle, apply this directive on your toggle to toggle row on element click.
 */
let NbTreeGridRowToggleDirective = class NbTreeGridRowToggleDirective {
    constructor(cell) {
        this.cell = cell;
    }
    toggleRow($event) {
        this.cell.toggleRow();
        $event.stopPropagation();
    }
};
__decorate([
    HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], NbTreeGridRowToggleDirective.prototype, "toggleRow", null);
NbTreeGridRowToggleDirective = __decorate([
    Directive({
        selector: '[nbTreeGridRowToggle]',
    }),
    __metadata("design:paramtypes", [NbTreeGridCellDirective])
], NbTreeGridRowToggleDirective);
export { NbTreeGridRowToggleDirective };
//# sourceMappingURL=tree-grid-row-toggle.directive.js.map