/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, HostListener, Input } from '@angular/core';
import { NbTreeGridCellDirective } from './tree-grid-cell.component';
/**
 * NbTreeGridRowToggleComponent
 */
let NbTreeGridRowToggleComponent = class NbTreeGridRowToggleComponent {
    constructor(cell) {
        this.cell = cell;
    }
    set expanded(value) {
        this.expandedValue = value;
    }
    get expanded() {
        return this.expandedValue;
    }
    toggleRow($event) {
        this.cell.toggleRow();
        $event.stopPropagation();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], NbTreeGridRowToggleComponent.prototype, "expanded", null);
__decorate([
    HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], NbTreeGridRowToggleComponent.prototype, "toggleRow", null);
NbTreeGridRowToggleComponent = __decorate([
    Component({
        selector: 'nb-tree-grid-row-toggle',
        template: `
    <button class="row-toggle-button" [attr.aria-label]="expanded ? 'collapse' : 'expand'">
      <nb-icon [icon]="expanded ? 'chevron-down-outline' : 'chevron-right-outline'"
               pack="nebular-essentials"
               aria-hidden="true">
      </nb-icon>
    </button>
  `,
        styles: [`
    button {
      background: transparent;
      border: none;
      padding: 0;
    }
  `]
    }),
    __metadata("design:paramtypes", [NbTreeGridCellDirective])
], NbTreeGridRowToggleComponent);
export { NbTreeGridRowToggleComponent };
//# sourceMappingURL=tree-grid-row-toggle.component.js.map