/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbOverlayModule } from '../cdk/overlay/overlay.module';
import { NbContextMenuDirective } from './context-menu.directive';
import { NbContextMenuComponent } from './context-menu.component';
import { NbMenuModule } from '../menu/menu.module';
let NbContextMenuModule = class NbContextMenuModule {
};
NbContextMenuModule = __decorate([
    NgModule({
        imports: [CommonModule, NbOverlayModule, NbMenuModule],
        exports: [NbContextMenuDirective],
        declarations: [NbContextMenuDirective, NbContextMenuComponent],
        entryComponents: [NbContextMenuComponent],
    })
], NbContextMenuModule);
export { NbContextMenuModule };
//# sourceMappingURL=context-menu.module.js.map