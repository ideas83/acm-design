/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbMenuModule_1;
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbMenuComponent, NbMenuItemComponent } from './menu.component';
import { NbMenuService, NbMenuInternalService } from './menu.service';
import { NbIconModule } from '../icon/icon.module';
const nbMenuComponents = [NbMenuComponent, NbMenuItemComponent];
const NB_MENU_PROVIDERS = [NbMenuService, NbMenuInternalService];
let NbMenuModule = NbMenuModule_1 = class NbMenuModule {
    static forRoot() {
        return {
            ngModule: NbMenuModule_1,
            providers: [
                ...NB_MENU_PROVIDERS,
            ],
        };
    }
};
NbMenuModule = NbMenuModule_1 = __decorate([
    NgModule({
        imports: [NbSharedModule, NbIconModule],
        declarations: [...nbMenuComponents],
        exports: [...nbMenuComponents],
    })
], NbMenuModule);
export { NbMenuModule };
//# sourceMappingURL=menu.module.js.map