/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbSpinnerComponent } from './spinner.component';
import { NbSpinnerDirective } from './spinner.directive';
let NbSpinnerModule = class NbSpinnerModule {
};
NbSpinnerModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
        ],
        exports: [NbSpinnerComponent, NbSpinnerDirective],
        declarations: [NbSpinnerComponent, NbSpinnerDirective],
        entryComponents: [NbSpinnerComponent],
    })
], NbSpinnerModule);
export { NbSpinnerModule };
//# sourceMappingURL=spinner.module.js.map