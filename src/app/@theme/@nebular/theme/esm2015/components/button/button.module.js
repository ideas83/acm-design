/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbButtonComponent } from './button.component';
const NB_BUTTON_COMPONENTS = [
    NbButtonComponent,
];
let NbButtonModule = class NbButtonModule {
};
NbButtonModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
        ],
        declarations: [
            ...NB_BUTTON_COMPONENTS,
        ],
        exports: [
            ...NB_BUTTON_COMPONENTS,
        ],
    })
], NbButtonModule);
export { NbButtonModule };
//# sourceMappingURL=button.module.js.map