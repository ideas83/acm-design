/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbFormFieldComponent } from './form-field.component';
import { NbPrefixDirective } from './prefix.directive';
import { NbSuffixDirective } from './suffix.directive';
const COMPONENTS = [
    NbFormFieldComponent,
    NbPrefixDirective,
    NbSuffixDirective,
];
let NbFormFieldModule = class NbFormFieldModule {
};
NbFormFieldModule = __decorate([
    NgModule({
        imports: [CommonModule],
        declarations: [...COMPONENTS],
        exports: [...COMPONENTS],
    })
], NbFormFieldModule);
export { NbFormFieldModule };
//# sourceMappingURL=form-field.module.js.map