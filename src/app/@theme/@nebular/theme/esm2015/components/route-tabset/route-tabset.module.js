/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbRouteTabsetComponent } from './route-tabset.component';
import { NbIconModule } from '../icon/icon.module';
let NbRouteTabsetModule = class NbRouteTabsetModule {
};
NbRouteTabsetModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
            NbIconModule,
        ],
        declarations: [
            NbRouteTabsetComponent,
        ],
        exports: [
            NbRouteTabsetComponent,
        ],
    })
], NbRouteTabsetModule);
export { NbRouteTabsetModule };
//# sourceMappingURL=route-tabset.module.js.map