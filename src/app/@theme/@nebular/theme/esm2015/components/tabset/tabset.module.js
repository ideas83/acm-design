/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbTabsetComponent, NbTabComponent } from './tabset.component';
import { NbBadgeModule } from '../badge/badge.module';
import { NbIconModule } from '../icon/icon.module';
const NB_TABSET_COMPONENTS = [
    NbTabsetComponent,
    NbTabComponent,
];
let NbTabsetModule = class NbTabsetModule {
};
NbTabsetModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
            NbBadgeModule,
            NbIconModule,
        ],
        declarations: [
            ...NB_TABSET_COMPONENTS,
        ],
        exports: [
            ...NB_TABSET_COMPONENTS,
        ],
    })
], NbTabsetModule);
export { NbTabsetModule };
//# sourceMappingURL=tabset.module.js.map