import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, Input, HostBinding } from '@angular/core';
import { NbPosition } from '../cdk/overlay/overlay-position';
/**
 * The `NbOptionListComponent` is container component for `NbOptionGroupComponent` and`NbOptionComponent` list.
 *
 * @styles
 *
 * option-list-max-height:
 * option-list-shadow:
 * option-list-background-color:
 * option-list-border-style:
 * option-list-border-width:
 * option-list-border-color:
 * option-list-border-radius:
 * option-list-adjacent-border-color:
 * option-list-adjacent-border-style:
 * option-list-adjacent-border-width:
 * */
let NbOptionListComponent = class NbOptionListComponent {
    constructor() {
        this.size = 'medium';
    }
    get positionTop() {
        return this.position === NbPosition.TOP;
    }
    get positionBottom() {
        return this.position === NbPosition.BOTTOM;
    }
    get sizeTiny() {
        return this.size === 'tiny';
    }
    get sizeSmall() {
        return this.size === 'small';
    }
    get sizeMedium() {
        return this.size === 'medium';
    }
    get sizeLarge() {
        return this.size === 'large';
    }
    get sizeGiant() {
        return this.size === 'giant';
    }
};
__decorate([
    Input(),
    __metadata("design:type", String)
], NbOptionListComponent.prototype, "size", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbOptionListComponent.prototype, "position", void 0);
__decorate([
    HostBinding('class.position-top'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "positionTop", null);
__decorate([
    HostBinding('class.position-bottom'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "positionBottom", null);
__decorate([
    HostBinding('class.size-tiny'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "sizeTiny", null);
__decorate([
    HostBinding('class.size-small'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "sizeSmall", null);
__decorate([
    HostBinding('class.size-medium'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "sizeMedium", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "sizeLarge", null);
__decorate([
    HostBinding('class.size-giant'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbOptionListComponent.prototype, "sizeGiant", null);
NbOptionListComponent = __decorate([
    Component({
        selector: 'nb-option-list',
        template: `
    <ul class="option-list">
      <ng-content></ng-content>
    </ul>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    })
], NbOptionListComponent);
export { NbOptionListComponent };
//# sourceMappingURL=option-list.component.js.map