/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { NbOverlayContainerComponent, NbPositionedContainer } from '../cdk/overlay/overlay-container';
let NbDatepickerContainerComponent = class NbDatepickerContainerComponent extends NbPositionedContainer {
    attach(portal) {
        return this.overlayContainer.attachComponentPortal(portal);
    }
};
__decorate([
    ViewChild(NbOverlayContainerComponent, { static: true }),
    __metadata("design:type", NbOverlayContainerComponent)
], NbDatepickerContainerComponent.prototype, "overlayContainer", void 0);
NbDatepickerContainerComponent = __decorate([
    Component({
        selector: 'nb-datepicker-container',
        template: `
    <nb-overlay-container></nb-overlay-container>
  `
    })
], NbDatepickerContainerComponent);
export { NbDatepickerContainerComponent };
//# sourceMappingURL=datepicker-container.component.js.map