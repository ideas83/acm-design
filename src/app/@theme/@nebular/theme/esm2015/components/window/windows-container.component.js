import { __decorate, __metadata } from "tslib";
import { Component, ViewContainerRef, ViewChild } from '@angular/core';
let NbWindowsContainerComponent = class NbWindowsContainerComponent {
};
__decorate([
    ViewChild('viewContainerRef', { read: ViewContainerRef, static: true }),
    __metadata("design:type", ViewContainerRef)
], NbWindowsContainerComponent.prototype, "viewContainerRef", void 0);
NbWindowsContainerComponent = __decorate([
    Component({
        selector: 'nb-windows-container',
        template: `<ng-container #viewContainerRef></ng-container>`,
        styles: [":host{display:flex;align-items:flex-end;overflow-x:auto}:host ::ng-deep nb-window:not(.full-screen){margin:0 2rem}\n"]
    })
], NbWindowsContainerComponent);
export { NbWindowsContainerComponent };
//# sourceMappingURL=windows-container.component.js.map