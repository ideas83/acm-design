/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList, ViewChild, } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NbOptionComponent } from '../option/option.component';
import { NbPortalDirective } from '../cdk/overlay/mapping';
// Component class scoped counter for aria attributes.
let lastAutocompleteId = 0;
/**
 * The `NbAutocompleteComponent` overlay component.
 * Provides an `NbOptionList` overlay component.
 * */
let NbAutocompleteComponent = class NbAutocompleteComponent {
    constructor(cd) {
        this.cd = cd;
        this.destroy$ = new Subject();
        /**
         * Component scoped id for aria attributes.
         * */
        this.id = `nb-autocomplete-${lastAutocompleteId++}`;
        /**
         * @docs-private
         * Current overlay position because of we have to toggle overlayPosition
         * in [ngClass] direction.
         */
        this._overlayPosition = '';
        /**
         * Autocomplete size, available sizes:
         * `tiny`, `small`, `medium` (default), `large`, `giant`
         */
        this.size = 'medium';
        /**
         * Flag passed as input to always make first option active.
         * */
        this.activeFirst = false;
        /**
         * Will be emitted when selected value changes.
         * */
        this.selectedChange = new EventEmitter();
    }
    get overlayPosition() {
        return this._overlayPosition;
    }
    set overlayPosition(value) {
        this._overlayPosition = value;
        // Need run change detection after first set from NbAutocompleteDirective
        this.cd.detectChanges();
    }
    /**
     * Returns width of the input.
     * */
    get hostWidth() {
        return this.hostRef.nativeElement.getBoundingClientRect().width;
    }
    ngAfterContentInit() {
        this.options.changes
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.cd.detectChanges());
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
    /**
     * Autocomplete knows nothing about host html input element.
     * So, attach method set input hostRef for styling.
     * */
    setHost(hostRef) {
        this.hostRef = hostRef;
    }
    /**
     * Propagate selected value.
     * */
    emitSelected(selected) {
        this.selectedChange.emit(selected);
    }
    get tiny() {
        return this.size === 'tiny';
    }
    get small() {
        return this.size === 'small';
    }
    get medium() {
        return this.size === 'medium';
    }
    get large() {
        return this.size === 'large';
    }
    get giant() {
        return this.size === 'giant';
    }
};
__decorate([
    Input(),
    __metadata("design:type", Function)
], NbAutocompleteComponent.prototype, "handleDisplayFn", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbAutocompleteComponent.prototype, "size", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], NbAutocompleteComponent.prototype, "activeFirst", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbAutocompleteComponent.prototype, "selectedChange", void 0);
__decorate([
    ContentChildren(NbOptionComponent, { descendants: true }),
    __metadata("design:type", QueryList)
], NbAutocompleteComponent.prototype, "options", void 0);
__decorate([
    ViewChild(NbPortalDirective),
    __metadata("design:type", NbPortalDirective)
], NbAutocompleteComponent.prototype, "portal", void 0);
__decorate([
    HostBinding('class.size-tiny'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAutocompleteComponent.prototype, "tiny", null);
__decorate([
    HostBinding('class.size-small'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAutocompleteComponent.prototype, "small", null);
__decorate([
    HostBinding('class.size-medium'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAutocompleteComponent.prototype, "medium", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAutocompleteComponent.prototype, "large", null);
__decorate([
    HostBinding('class.size-giant'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAutocompleteComponent.prototype, "giant", null);
NbAutocompleteComponent = __decorate([
    Component({
        selector: 'nb-autocomplete',
        template: "<nb-option-list *nbPortal\n                [size]=\"size\"\n                [position]=\"overlayPosition\"\n                [style.width.px]=\"hostWidth\"\n                role=\"listbox\"\n                [id]=\"id\"\n                [class.empty]=\"!options?.length\">\n  <ng-content select=\"nb-option, nb-option-group\"></ng-content>\n</nb-option-list>\n",
        changeDetection: ChangeDetectionStrategy.OnPush,
        styles: [":host(:hover){cursor:pointer}nb-option-list.empty{border:none}\n"]
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef])
], NbAutocompleteComponent);
export { NbAutocompleteComponent };
//# sourceMappingURL=autocomplete.component.js.map