/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbProgressBarComponent } from './progress-bar.component';
let NbProgressBarModule = class NbProgressBarModule {
};
NbProgressBarModule = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
        ],
        declarations: [NbProgressBarComponent],
        exports: [NbProgressBarComponent],
    })
], NbProgressBarModule);
export { NbProgressBarModule };
//# sourceMappingURL=progress-bar.module.js.map