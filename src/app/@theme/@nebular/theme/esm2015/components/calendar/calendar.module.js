/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbCalendarComponent } from './calendar.component';
import { NbBaseCalendarModule } from './base-calendar.module';
let NbCalendarModule = class NbCalendarModule {
};
NbCalendarModule = __decorate([
    NgModule({
        imports: [NbBaseCalendarModule],
        exports: [NbCalendarComponent],
        declarations: [NbCalendarComponent],
    })
], NbCalendarModule);
export { NbCalendarModule };
//# sourceMappingURL=calendar.module.js.map