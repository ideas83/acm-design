/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
let NbCalendarRangeMonthCellComponent = class NbCalendarRangeMonthCellComponent extends NbBaseCalendarRangeCell {
    constructor(dateService) {
        super();
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter(true);
        this.monthCellClass = true;
        this.rangeCellClass = true;
    }
    get month() {
        return this.dateService.getMonthName(this.date);
    }
    get selected() {
        if (this.inRange) {
            return true;
        }
        if (this.selectedValue) {
            return this.dateService.isSameMonth(this.date, this.selectedValue.start);
        }
    }
    get inRange() {
        if (this.hasRange) {
            return this.isInRage(this.date, this.selectedValue);
        }
    }
    get rangeStart() {
        if (this.hasRange) {
            return this.dateService.isSameMonth(this.date, this.selectedValue.start);
        }
    }
    get rangeEnd() {
        if (this.hasRange) {
            return this.dateService.isSameMonth(this.date, this.selectedValue.end);
        }
    }
    get today() {
        return this.dateService.isSameMonthSafe(this.date, this.dateService.today());
    }
    get disabled() {
        return this.smallerThanMin() || this.greaterThanMax();
    }
    get isLarge() {
        return this.size === NbCalendarSize.LARGE;
    }
    onClick() {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    }
    smallerThanMin() {
        return this.date && this.min && this.dateService.compareDates(this.monthEnd(), this.min) < 0;
    }
    greaterThanMax() {
        return this.date && this.max && this.dateService.compareDates(this.monthStart(), this.max) > 0;
    }
    monthStart() {
        return this.dateService.getMonthStart(this.date);
    }
    monthEnd() {
        return this.dateService.getMonthEnd(this.date);
    }
    isInRage(date, range) {
        if (range.start && range.end) {
            const cellDate = this.dateService.getMonthStart(date);
            const start = this.dateService.getMonthStart(range.start);
            const end = this.dateService.getMonthStart(range.end);
            const isGreaterThanStart = this.dateService.compareDates(cellDate, start) >= 0;
            const isLessThanEnd = this.dateService.compareDates(cellDate, end) <= 0;
            return isGreaterThanStart && isLessThanEnd;
        }
        return this.dateService.isSameMonth(date, range.start);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "visibleDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarRangeMonthCellComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarRangeMonthCellComponent.prototype, "select", void 0);
__decorate([
    HostBinding('class.month-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "monthCellClass", void 0);
__decorate([
    HostBinding('class.range-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeMonthCellComponent.prototype, "rangeCellClass", void 0);
__decorate([
    HostBinding('class.selected'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "selected", null);
__decorate([
    HostBinding('class.in-range'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "inRange", null);
__decorate([
    HostBinding('class.start'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "rangeStart", null);
__decorate([
    HostBinding('class.end'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "rangeEnd", null);
__decorate([
    HostBinding('class.today'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "today", null);
__decorate([
    HostBinding('class.disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "disabled", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeMonthCellComponent.prototype, "isLarge", null);
__decorate([
    HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], NbCalendarRangeMonthCellComponent.prototype, "onClick", null);
NbCalendarRangeMonthCellComponent = __decorate([
    Component({
        selector: 'nb-calendar-range-month-cell',
        template: `
    <div class="cell-content">
      {{ month }}
    </div>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbDateService])
], NbCalendarRangeMonthCellComponent);
export { NbCalendarRangeMonthCellComponent };
//# sourceMappingURL=calendar-range-month-cell.component.js.map