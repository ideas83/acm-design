/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
let NbCalendarRangeDayCellComponent = class NbCalendarRangeDayCellComponent extends NbBaseCalendarRangeCell {
    constructor(dateService) {
        super();
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter(true);
        this.rangeCellClass = true;
        this.dayCellClass = true;
    }
    get inRange() {
        if (this.date && this.hasRange) {
            return this.isInRange(this.date, this.selectedValue);
        }
        return false;
    }
    get start() {
        return this.date && this.hasRange && this.dateService.isSameDay(this.date, this.selectedValue.start);
    }
    get end() {
        return this.date && this.hasRange && this.dateService.isSameDay(this.date, this.selectedValue.end);
    }
    get today() {
        return this.date && this.dateService.isSameDay(this.date, this.dateService.today());
    }
    get boundingMonth() {
        return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
    }
    get selected() {
        if (this.inRange) {
            return true;
        }
        if (this.selectedValue) {
            return this.dateService.isSameDay(this.date, this.selectedValue.start);
        }
    }
    get empty() {
        return !this.date;
    }
    get disabled() {
        return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
    }
    get isLarge() {
        return this.size === NbCalendarSize.LARGE;
    }
    get day() {
        return this.date && this.dateService.getDate(this.date);
    }
    onClick() {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    }
    smallerThanMin() {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    }
    greaterThanMax() {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    }
    dontFitFilter() {
        return this.date && this.filter && !this.filter(this.date);
    }
    isInRange(date, { start, end }) {
        const isGreaterThanStart = this.dateService.compareDates(this.date, start) >= 0;
        const isLessThanEnd = this.dateService.compareDates(this.date, end) <= 0;
        return isGreaterThanStart && isLessThanEnd;
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "visibleDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], NbCalendarRangeDayCellComponent.prototype, "filter", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarRangeDayCellComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarRangeDayCellComponent.prototype, "select", void 0);
__decorate([
    HostBinding('class.in-range'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "inRange", null);
__decorate([
    HostBinding('class.start'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "start", null);
__decorate([
    HostBinding('class.end'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "end", null);
__decorate([
    HostBinding('class.range-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "rangeCellClass", void 0);
__decorate([
    HostBinding('class.day-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeDayCellComponent.prototype, "dayCellClass", void 0);
__decorate([
    HostBinding('class.today'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "today", null);
__decorate([
    HostBinding('class.bounding-month'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "boundingMonth", null);
__decorate([
    HostBinding('class.selected'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "selected", null);
__decorate([
    HostBinding('class.empty'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "empty", null);
__decorate([
    HostBinding('class.disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "disabled", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeDayCellComponent.prototype, "isLarge", null);
__decorate([
    HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], NbCalendarRangeDayCellComponent.prototype, "onClick", null);
NbCalendarRangeDayCellComponent = __decorate([
    Component({
        selector: 'nb-calendar-range-day-cell',
        template: `
    <div class="cell-content">{{ day }}</div>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbDateService])
], NbCalendarRangeDayCellComponent);
export { NbCalendarRangeDayCellComponent };
//# sourceMappingURL=calendar-range-day-cell.component.js.map