/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata } from "tslib";
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, HostBinding, HostListener, } from '@angular/core';
import { NbCalendarSize } from '../calendar-kit/model';
import { NbDateService } from '../calendar-kit/services/date.service';
import { NbBaseCalendarRangeCell } from './base-calendar-range-cell';
let NbCalendarRangeYearCellComponent = class NbCalendarRangeYearCellComponent extends NbBaseCalendarRangeCell {
    constructor(dateService) {
        super();
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.select = new EventEmitter(true);
        this.yearCellClass = true;
        this.rangeCellClass = true;
    }
    get inRange() {
        return this.hasRange && this.isInRange(this.date, this.selectedValue);
    }
    get rangeStart() {
        return this.hasRange && this.dateService.isSameYear(this.date, this.selectedValue.start);
    }
    get rangeEnd() {
        return this.hasRange && this.dateService.isSameYear(this.date, this.selectedValue.end);
    }
    get selected() {
        if (this.inRange) {
            return true;
        }
        if (this.selectedValue) {
            return this.dateService.isSameYear(this.date, this.selectedValue.start);
        }
    }
    get today() {
        return this.dateService.isSameYear(this.date, this.dateService.today());
    }
    get disabled() {
        return this.smallerThanMin() || this.greaterThanMax();
    }
    get isLarge() {
        return this.size === NbCalendarSize.LARGE;
    }
    get year() {
        return this.dateService.getYear(this.date);
    }
    onClick() {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    }
    smallerThanMin() {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    }
    greaterThanMax() {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    }
    yearStart() {
        return this.dateService.getYearStart(this.date);
    }
    yearEnd() {
        return this.dateService.getYearEnd(this.date);
    }
    isInRange(date, { start, end }) {
        if (start && end) {
            const cellYear = this.dateService.getYear(date);
            const startYear = this.dateService.getYear(start);
            const endYear = this.dateService.getYear(end);
            return cellYear >= startYear && cellYear <= endYear;
        }
        return this.dateService.isSameYear(date, start);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "date", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "min", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "max", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "selectedValue", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], NbCalendarRangeYearCellComponent.prototype, "size", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], NbCalendarRangeYearCellComponent.prototype, "select", void 0);
__decorate([
    HostBinding('class.in-range'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "inRange", null);
__decorate([
    HostBinding('class.start'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "rangeStart", null);
__decorate([
    HostBinding('class.end'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "rangeEnd", null);
__decorate([
    HostBinding('class.selected'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "selected", null);
__decorate([
    HostBinding('class.today'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "today", null);
__decorate([
    HostBinding('class.disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "disabled", null);
__decorate([
    HostBinding('class.size-large'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbCalendarRangeYearCellComponent.prototype, "isLarge", null);
__decorate([
    HostBinding('class.year-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "yearCellClass", void 0);
__decorate([
    HostBinding('class.range-cell'),
    __metadata("design:type", Object)
], NbCalendarRangeYearCellComponent.prototype, "rangeCellClass", void 0);
__decorate([
    HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], NbCalendarRangeYearCellComponent.prototype, "onClick", null);
NbCalendarRangeYearCellComponent = __decorate([
    Component({
        selector: 'nb-calendar-range-year-cell',
        template: `
    <div class="cell-content">
      {{ year }}
    </div>
  `,
        changeDetection: ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [NbDateService])
], NbCalendarRangeYearCellComponent);
export { NbCalendarRangeYearCellComponent };
//# sourceMappingURL=calendar-range-year-cell.component.js.map