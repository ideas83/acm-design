import { __decorate, __metadata, __param } from "tslib";
import { Component, Inject, Input, TemplateRef, ViewChild } from '@angular/core';
import { NB_STEPPER } from './stepper-tokens';
import { convertToBoolProperty } from '../helpers';
/**
 * Component intended to be used within  the `<nb-stepper>` component.
 * Container for a step
 */
let NbStepComponent = class NbStepComponent {
    constructor(stepper) {
        this._hidden = false;
        this._completed = false;
        this.interacted = false;
        this.stepper = stepper;
    }
    /**
     * Whether step will be displayed in wizard
     *
     * @type {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    set hidden(value) {
        this._hidden = convertToBoolProperty(value);
    }
    /**
     * Check that label is a TemplateRef.
     *
     * @return boolean
     * */
    get isLabelTemplate() {
        return this.label instanceof TemplateRef;
    }
    /**
     * Whether step is marked as completed.
     *
     * @type {boolean}
     */
    get completed() {
        return this._completed || this.isCompleted;
    }
    set completed(value) {
        this._completed = convertToBoolProperty(value);
    }
    get isCompleted() {
        return this.stepControl ? this.stepControl.valid && this.interacted : this.interacted;
    }
    /**
     * Mark step as selected
     * */
    select() {
        this.stepper.selected = this;
    }
    /**
     * Reset step and stepControl state
     * */
    reset() {
        this.interacted = false;
        if (this.stepControl) {
            this.stepControl.reset();
        }
    }
};
__decorate([
    ViewChild(TemplateRef, { static: true }),
    __metadata("design:type", TemplateRef)
], NbStepComponent.prototype, "content", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbStepComponent.prototype, "stepControl", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], NbStepComponent.prototype, "label", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], NbStepComponent.prototype, "hidden", null);
__decorate([
    Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], NbStepComponent.prototype, "completed", null);
NbStepComponent = __decorate([
    Component({
        selector: 'nb-step',
        template: `
    <ng-template>
      <ng-content></ng-content>
    </ng-template>
  `
    }),
    __param(0, Inject(NB_STEPPER)),
    __metadata("design:paramtypes", [Object])
], NbStepComponent);
export { NbStepComponent };
//# sourceMappingURL=step.component.js.map