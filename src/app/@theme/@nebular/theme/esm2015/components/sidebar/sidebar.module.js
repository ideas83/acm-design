/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbSidebarModule_1;
import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbSidebarComponent, NbSidebarFooterComponent, NbSidebarHeaderComponent, } from './sidebar.component';
import { NbSidebarService } from './sidebar.service';
const NB_SIDEBAR_COMPONENTS = [
    NbSidebarComponent,
    NbSidebarFooterComponent,
    NbSidebarHeaderComponent,
];
const NB_SIDEBAR_PROVIDERS = [
    NbSidebarService,
];
let NbSidebarModule = NbSidebarModule_1 = class NbSidebarModule {
    static forRoot() {
        return {
            ngModule: NbSidebarModule_1,
            providers: [
                ...NB_SIDEBAR_PROVIDERS,
            ],
        };
    }
};
NbSidebarModule = NbSidebarModule_1 = __decorate([
    NgModule({
        imports: [
            NbSharedModule,
        ],
        declarations: [
            ...NB_SIDEBAR_COMPONENTS,
        ],
        exports: [
            ...NB_SIDEBAR_COMPONENTS,
        ],
    })
], NbSidebarModule);
export { NbSidebarModule };
//# sourceMappingURL=sidebar.module.js.map