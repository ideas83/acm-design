import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NbListComponent, NbListItemComponent } from './list.component';
import { NbListPageTrackerDirective } from './list-page-tracker.directive';
import { NbInfiniteListDirective } from './infinite-list.directive';
const components = [
    NbListComponent,
    NbListItemComponent,
    NbListPageTrackerDirective,
    NbInfiniteListDirective,
];
let NbListModule = class NbListModule {
};
NbListModule = __decorate([
    NgModule({
        declarations: components,
        exports: components,
    })
], NbListModule);
export { NbListModule };
//# sourceMappingURL=list.module.js.map