/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbIconModule } from '../icon/icon.module';
import { NbToggleComponent } from './toggle.component';
let NbToggleModule = class NbToggleModule {
};
NbToggleModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            NbIconModule,
        ],
        declarations: [NbToggleComponent],
        exports: [NbToggleComponent],
    })
], NbToggleModule);
export { NbToggleModule };
//# sourceMappingURL=toggle.module.js.map