import { __decorate } from "tslib";
import { Component } from '@angular/core';
/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the front card.
 */
let NbCardFrontComponent = class NbCardFrontComponent {
};
NbCardFrontComponent = __decorate([
    Component({
        selector: 'nb-card-front',
        template: '<ng-content select="nb-card"></ng-content>'
    })
], NbCardFrontComponent);
export { NbCardFrontComponent };
/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the back card.
 */
let NbCardBackComponent = class NbCardBackComponent {
};
NbCardBackComponent = __decorate([
    Component({
        selector: 'nb-card-back',
        template: '<ng-content select="nb-card"></ng-content>'
    })
], NbCardBackComponent);
export { NbCardBackComponent };
//# sourceMappingURL=shared.component.js.map