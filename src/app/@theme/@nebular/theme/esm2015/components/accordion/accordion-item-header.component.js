/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { __decorate, __metadata, __param } from "tslib";
import { Component, ChangeDetectionStrategy, Host, HostBinding, HostListener, ChangeDetectorRef, } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NbAccordionItemComponent } from './accordion-item.component';
/**
 * Component intended to be used within `<nb-accordion-item>` component
 */
let NbAccordionItemHeaderComponent = class NbAccordionItemHeaderComponent {
    constructor(accordionItem, cd) {
        this.accordionItem = accordionItem;
        this.cd = cd;
        this.destroy$ = new Subject();
    }
    get isCollapsed() {
        return this.accordionItem.collapsed;
    }
    get expanded() {
        return !this.accordionItem.collapsed;
    }
    // issue #794
    get tabbable() {
        return this.accordionItem.disabled ? '-1' : '0';
    }
    get disabled() {
        return this.accordionItem.disabled;
    }
    toggle() {
        this.accordionItem.toggle();
    }
    get state() {
        if (this.isCollapsed) {
            return 'collapsed';
        }
        if (this.expanded) {
            return 'expanded';
        }
    }
    ngOnInit() {
        this.accordionItem.accordionItemInvalidate
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.cd.markForCheck());
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
};
__decorate([
    HostBinding('class.accordion-item-header-collapsed'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAccordionItemHeaderComponent.prototype, "isCollapsed", null);
__decorate([
    HostBinding('class.accordion-item-header-expanded'),
    HostBinding('attr.aria-expanded'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAccordionItemHeaderComponent.prototype, "expanded", null);
__decorate([
    HostBinding('attr.tabindex'),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], NbAccordionItemHeaderComponent.prototype, "tabbable", null);
__decorate([
    HostBinding('attr.aria-disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [])
], NbAccordionItemHeaderComponent.prototype, "disabled", null);
__decorate([
    HostListener('click'),
    HostListener('keydown.space'),
    HostListener('keydown.enter'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], NbAccordionItemHeaderComponent.prototype, "toggle", null);
NbAccordionItemHeaderComponent = __decorate([
    Component({
        selector: 'nb-accordion-item-header',
        template: `
    <ng-content select="nb-accordion-item-title"></ng-content>
    <ng-content select="nb-accordion-item-description"></ng-content>
    <ng-content></ng-content>
    <nb-icon icon="chevron-down-outline"
             pack="nebular-essentials"
             [@expansionIndicator]="state"
             *ngIf="!disabled"
             class="expansion-indicator">
    </nb-icon>
  `,
        animations: [
            trigger('expansionIndicator', [
                state('expanded', style({
                    transform: 'rotate(180deg)',
                })),
                transition('collapsed => expanded', animate('100ms ease-in')),
                transition('expanded => collapsed', animate('100ms ease-out')),
            ]),
        ],
        changeDetection: ChangeDetectionStrategy.OnPush,
        styles: [":host{display:flex;align-items:center;cursor:pointer}:host:focus{outline:0}\n"]
    }),
    __param(0, Host()),
    __metadata("design:paramtypes", [NbAccordionItemComponent, ChangeDetectorRef])
], NbAccordionItemHeaderComponent);
export { NbAccordionItemHeaderComponent };
//# sourceMappingURL=accordion-item-header.component.js.map