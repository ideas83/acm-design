/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ModuleWithProviders } from '@angular/core';
import { NbChatOptions } from './chat.options';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './chat.component';
import * as ɵngcc2 from './chat-message.component';
import * as ɵngcc3 from './chat-form.component';
import * as ɵngcc4 from './chat-message-text.component';
import * as ɵngcc5 from './chat-message-file.component';
import * as ɵngcc6 from './chat-message-quote.component';
import * as ɵngcc7 from './chat-message-map.component';
import * as ɵngcc8 from '../shared/shared.module';
import * as ɵngcc9 from '../icon/icon.module';
import * as ɵngcc10 from '../input/input.module';
import * as ɵngcc11 from '../button/button.module';
export declare class NbChatModule {
    static forRoot(options?: NbChatOptions): ModuleWithProviders<NbChatModule>;
    static forChild(options?: NbChatOptions): ModuleWithProviders<NbChatModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbChatModule, [typeof ɵngcc1.NbChatComponent, typeof ɵngcc2.NbChatMessageComponent, typeof ɵngcc3.NbChatFormComponent, typeof ɵngcc4.NbChatMessageTextComponent, typeof ɵngcc5.NbChatMessageFileComponent, typeof ɵngcc6.NbChatMessageQuoteComponent, typeof ɵngcc7.NbChatMessageMapComponent], [typeof ɵngcc8.NbSharedModule, typeof ɵngcc9.NbIconModule, typeof ɵngcc10.NbInputModule, typeof ɵngcc11.NbButtonModule], [typeof ɵngcc1.NbChatComponent, typeof ɵngcc2.NbChatMessageComponent, typeof ɵngcc3.NbChatFormComponent, typeof ɵngcc4.NbChatMessageTextComponent, typeof ɵngcc5.NbChatMessageFileComponent, typeof ɵngcc6.NbChatMessageQuoteComponent, typeof ɵngcc7.NbChatMessageMapComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbChatModule>;
}

//# sourceMappingURL=chat.module.d.ts.map