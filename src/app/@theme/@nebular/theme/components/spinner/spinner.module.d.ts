/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './spinner.component';
import * as ɵngcc2 from './spinner.directive';
import * as ɵngcc3 from '../shared/shared.module';
export declare class NbSpinnerModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbSpinnerModule, [typeof ɵngcc1.NbSpinnerComponent, typeof ɵngcc2.NbSpinnerDirective], [typeof ɵngcc3.NbSharedModule], [typeof ɵngcc1.NbSpinnerComponent, typeof ɵngcc2.NbSpinnerDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbSpinnerModule>;
}

//# sourceMappingURL=spinner.module.d.ts.map