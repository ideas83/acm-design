/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * `NbCalendarKitModule` is a module that contains multiple useful components for building custom calendars.
 * So if you think our calendars is not enough powerful for you just use calendar-kit and build your own calendar!
 *
 * Available components:
 * - `NbCalendarDayPicker`
 * - `NbCalendarDayCell`
 * - `NbCalendarMonthPicker`
 * - `NbCalendarMonthCell`
 * - `NbCalendarYearPicker`
 * - `NbCalendarYearCell`
 * - `NbCalendarViewModeComponent`
 * - `NbCalendarPageableNavigation`
 *
 * For example you can easily build full calendar:
 * @stacked-example(Full calendar, calendar-kit/calendar-kit-full-calendar.component)
 * */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './components/calendar-navigation/calendar-view-mode.component';
import * as ɵngcc2 from './components/calendar-navigation/calendar-pageable-navigation.component';
import * as ɵngcc3 from './components/calendar-days-names/calendar-days-names.component';
import * as ɵngcc4 from './components/calendar-year-picker/calendar-year-picker.component';
import * as ɵngcc5 from './components/calendar-month-picker/calendar-month-picker.component';
import * as ɵngcc6 from './components/calendar-day-picker/calendar-day-picker.component';
import * as ɵngcc7 from './components/calendar-day-picker/calendar-day-cell.component';
import * as ɵngcc8 from './components/calendar-month-picker/calendar-month-cell.component';
import * as ɵngcc9 from './components/calendar-year-picker/calendar-year-cell.component';
import * as ɵngcc10 from './components/calendar-picker/calendar-picker-row.component';
import * as ɵngcc11 from './components/calendar-picker/calendar-picker.component';
import * as ɵngcc12 from './components/calendar-week-number/calendar-week-number.component';
import * as ɵngcc13 from '../shared/shared.module';
import * as ɵngcc14 from '../button/button.module';
import * as ɵngcc15 from '../icon/icon.module';
export declare class NbCalendarKitModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbCalendarKitModule, [typeof ɵngcc1.NbCalendarViewModeComponent, typeof ɵngcc2.NbCalendarPageableNavigationComponent, typeof ɵngcc3.NbCalendarDaysNamesComponent, typeof ɵngcc4.NbCalendarYearPickerComponent, typeof ɵngcc5.NbCalendarMonthPickerComponent, typeof ɵngcc6.NbCalendarDayPickerComponent, typeof ɵngcc7.NbCalendarDayCellComponent, typeof ɵngcc8.NbCalendarMonthCellComponent, typeof ɵngcc9.NbCalendarYearCellComponent, typeof ɵngcc10.NbCalendarPickerRowComponent, typeof ɵngcc11.NbCalendarPickerComponent, typeof ɵngcc12.NbCalendarWeekNumberComponent], [typeof ɵngcc13.NbSharedModule, typeof ɵngcc14.NbButtonModule, typeof ɵngcc15.NbIconModule], [typeof ɵngcc1.NbCalendarViewModeComponent, typeof ɵngcc2.NbCalendarPageableNavigationComponent, typeof ɵngcc3.NbCalendarDaysNamesComponent, typeof ɵngcc4.NbCalendarYearPickerComponent, typeof ɵngcc5.NbCalendarMonthPickerComponent, typeof ɵngcc6.NbCalendarDayPickerComponent, typeof ɵngcc7.NbCalendarDayCellComponent, typeof ɵngcc8.NbCalendarMonthCellComponent, typeof ɵngcc9.NbCalendarYearCellComponent, typeof ɵngcc10.NbCalendarPickerRowComponent, typeof ɵngcc11.NbCalendarPickerComponent, typeof ɵngcc12.NbCalendarWeekNumberComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbCalendarKitModule>;
}

//# sourceMappingURL=calendar-kit.module.d.ts.map