import { ViewContainerRef } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class NbWindowsContainerComponent {
    viewContainerRef: ViewContainerRef;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbWindowsContainerComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NbWindowsContainerComponent, "nb-windows-container", never, {}, {}, never, never>;
}

//# sourceMappingURL=windows-container.component.d.ts.map