/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the front card.
 */
import * as ɵngcc0 from '@angular/core';
export declare class NbCardFrontComponent {
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbCardFrontComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NbCardFrontComponent, "nb-card-front", never, {}, {}, never, ["nb-card"]>;
}
/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the back card.
 */
export declare class NbCardBackComponent {
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbCardBackComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NbCardBackComponent, "nb-card-back", never, {}, {}, never, ["nb-card"]>;
}

//# sourceMappingURL=shared.component.d.ts.map