/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './calendar.component';
import * as ɵngcc2 from './base-calendar.module';
export declare class NbCalendarModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbCalendarModule, [typeof ɵngcc1.NbCalendarComponent], [typeof ɵngcc2.NbBaseCalendarModule], [typeof ɵngcc1.NbCalendarComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbCalendarModule>;
}

//# sourceMappingURL=calendar.module.d.ts.map