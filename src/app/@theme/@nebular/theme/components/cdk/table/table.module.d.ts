import { ChangeDetectorRef, ElementRef, IterableDiffers } from '@angular/core';
import { CdkTable, CdkTableModule } from '@angular/cdk/table';
import { NbDirectionality } from '../bidi/bidi-service';
import { NbPlatform } from '../platform/platform-service';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './cell';
import * as ɵngcc2 from './row';
import * as ɵngcc3 from '../bidi/bidi.module';
export declare const NB_TABLE_TEMPLATE = "\n  <ng-container nbHeaderRowOutlet></ng-container>\n  <ng-container nbRowOutlet></ng-container>\n  <ng-container nbFooterRowOutlet></ng-container>";
export declare class NbTable<T> extends CdkTable<T> {
    constructor(differs: IterableDiffers, changeDetectorRef: ChangeDetectorRef, elementRef: ElementRef, role: string, dir: NbDirectionality, document: any, platform: NbPlatform);
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbTable<any>, [null, null, null, { attribute: "role"; }, null, null, null]>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NbTable<any>, "nb-table-not-implemented", never, {}, {}, never, never>;
}
export declare class NbTableModule extends CdkTableModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbTableModule, [typeof NbTable, typeof ɵngcc1.NbHeaderCellDefDirective, typeof ɵngcc2.NbHeaderRowDefDirective, typeof ɵngcc1.NbColumnDefDirective, typeof ɵngcc1.NbCellDefDirective, typeof ɵngcc2.NbRowDefDirective, typeof ɵngcc1.NbFooterCellDefDirective, typeof ɵngcc2.NbFooterRowDefDirective, typeof ɵngcc2.NbDataRowOutletDirective, typeof ɵngcc2.NbHeaderRowOutletDirective, typeof ɵngcc2.NbFooterRowOutletDirective, typeof ɵngcc2.NbCellOutletDirective, typeof ɵngcc1.NbHeaderCellDirective, typeof ɵngcc1.NbCellDirective, typeof ɵngcc1.NbFooterCellDirective, typeof ɵngcc2.NbHeaderRowComponent, typeof ɵngcc2.NbRowComponent, typeof ɵngcc2.NbFooterRowComponent], [typeof ɵngcc3.NbBidiModule], [typeof NbTable, typeof ɵngcc1.NbHeaderCellDefDirective, typeof ɵngcc2.NbHeaderRowDefDirective, typeof ɵngcc1.NbColumnDefDirective, typeof ɵngcc1.NbCellDefDirective, typeof ɵngcc2.NbRowDefDirective, typeof ɵngcc1.NbFooterCellDefDirective, typeof ɵngcc2.NbFooterRowDefDirective, typeof ɵngcc2.NbDataRowOutletDirective, typeof ɵngcc2.NbHeaderRowOutletDirective, typeof ɵngcc2.NbFooterRowOutletDirective, typeof ɵngcc2.NbCellOutletDirective, typeof ɵngcc1.NbHeaderCellDirective, typeof ɵngcc1.NbCellDirective, typeof ɵngcc1.NbFooterCellDirective, typeof ɵngcc2.NbHeaderRowComponent, typeof ɵngcc2.NbRowComponent, typeof ɵngcc2.NbFooterRowComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbTableModule>;
}

//# sourceMappingURL=table.module.d.ts.map