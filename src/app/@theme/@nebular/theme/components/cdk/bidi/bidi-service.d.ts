import { Directionality } from '@angular/cdk/bidi';
import * as ɵngcc0 from '@angular/core';
export declare class NbDirectionality extends Directionality {
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbDirectionality, never>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<NbDirectionality>;
}

//# sourceMappingURL=bidi-service.d.ts.map