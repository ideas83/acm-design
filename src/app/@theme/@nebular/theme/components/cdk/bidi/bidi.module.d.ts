import { BidiModule } from '@angular/cdk/bidi';
import * as ɵngcc0 from '@angular/core';
export declare class NbBidiModule extends BidiModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbBidiModule, never, never, never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbBidiModule>;
}

//# sourceMappingURL=bidi.module.d.ts.map