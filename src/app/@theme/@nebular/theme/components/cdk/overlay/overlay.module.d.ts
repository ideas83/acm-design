import { ModuleWithProviders } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './overlay-container';
import * as ɵngcc2 from './mapping';
import * as ɵngcc3 from '../../shared/shared.module';
import * as ɵngcc4 from '../adapter/adapter.module';
export declare class NbOverlayModule {
    static forRoot(): ModuleWithProviders<NbOverlayModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbOverlayModule, [typeof ɵngcc1.NbPositionedContainer, typeof ɵngcc1.NbOverlayContainerComponent], [typeof ɵngcc2.NbCdkMappingModule, typeof ɵngcc3.NbSharedModule], [typeof ɵngcc2.NbCdkMappingModule, typeof ɵngcc4.NbCdkAdapterModule, typeof ɵngcc1.NbOverlayContainerComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbOverlayModule>;
}

//# sourceMappingURL=overlay.module.d.ts.map