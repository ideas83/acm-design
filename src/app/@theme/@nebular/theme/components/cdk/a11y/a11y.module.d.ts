import { ModuleWithProviders } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import * as ɵngcc0 from '@angular/core';
export declare class NbFocusMonitor extends FocusMonitor {
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbFocusMonitor, never>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<NbFocusMonitor>;
}
export declare class NbA11yModule {
    static forRoot(): ModuleWithProviders<NbA11yModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbA11yModule, never, never, never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbA11yModule>;
}

//# sourceMappingURL=a11y.module.d.ts.map