import { ModuleWithProviders } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class NbCdkAdapterModule {
    static forRoot(): ModuleWithProviders<NbCdkAdapterModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbCdkAdapterModule, never, never, never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbCdkAdapterModule>;
}

//# sourceMappingURL=adapter.module.d.ts.map