/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './user.component';
import * as ɵngcc2 from '../shared/shared.module';
import * as ɵngcc3 from '../badge/badge.module';
export declare class NbUserModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbUserModule, [typeof ɵngcc1.NbUserComponent], [typeof ɵngcc2.NbSharedModule, typeof ɵngcc3.NbBadgeModule], [typeof ɵngcc1.NbUserComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbUserModule>;
}

//# sourceMappingURL=user.module.d.ts.map