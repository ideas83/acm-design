import { NbStepperComponent } from './stepper.component';
import * as ɵngcc0 from '@angular/core';
export declare class NbStepperNextDirective {
    protected stepper: NbStepperComponent;
    type: string;
    constructor(stepper: NbStepperComponent);
    onClick(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbStepperNextDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<NbStepperNextDirective, "button[nbStepperNext]", never, { "type": "type"; }, {}, never>;
}
export declare class NbStepperPreviousDirective {
    protected stepper: NbStepperComponent;
    type: string;
    constructor(stepper: NbStepperComponent);
    onClick(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbStepperPreviousDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<NbStepperPreviousDirective, "button[nbStepperPrevious]", never, { "type": "type"; }, {}, never>;
}

//# sourceMappingURL=stepper-button.directive.d.ts.map