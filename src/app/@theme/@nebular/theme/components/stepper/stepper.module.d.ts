/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './stepper.component';
import * as ɵngcc2 from './step.component';
import * as ɵngcc3 from './stepper-button.directive';
import * as ɵngcc4 from '../shared/shared.module';
import * as ɵngcc5 from '../icon/icon.module';
export declare class NbStepperModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbStepperModule, [typeof ɵngcc1.NbStepperComponent, typeof ɵngcc2.NbStepComponent, typeof ɵngcc3.NbStepperNextDirective, typeof ɵngcc3.NbStepperPreviousDirective], [typeof ɵngcc4.NbSharedModule, typeof ɵngcc5.NbIconModule], [typeof ɵngcc1.NbStepperComponent, typeof ɵngcc2.NbStepComponent, typeof ɵngcc3.NbStepperNextDirective, typeof ɵngcc3.NbStepperPreviousDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbStepperModule>;
}

//# sourceMappingURL=stepper.module.d.ts.map