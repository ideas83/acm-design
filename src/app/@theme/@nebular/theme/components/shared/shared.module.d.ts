/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/common';
import * as ɵngcc2 from '@angular/forms';
import * as ɵngcc3 from '@angular/router';
export declare class NbSharedModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbSharedModule, never, never, [typeof ɵngcc1.CommonModule, typeof ɵngcc2.FormsModule, typeof ɵngcc3.RouterModule]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbSharedModule>;
}

//# sourceMappingURL=shared.module.d.ts.map