/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './popover.directive';
import * as ɵngcc2 from './popover.component';
import * as ɵngcc3 from '../cdk/overlay/overlay.module';
export declare class NbPopoverModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbPopoverModule, [typeof ɵngcc1.NbPopoverDirective, typeof ɵngcc2.NbPopoverComponent], [typeof ɵngcc3.NbOverlayModule], [typeof ɵngcc1.NbPopoverDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbPopoverModule>;
}

//# sourceMappingURL=popover.module.d.ts.map