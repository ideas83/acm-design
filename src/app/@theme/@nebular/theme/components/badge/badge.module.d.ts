/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './badge.component';
export declare class NbBadgeModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbBadgeModule, [typeof ɵngcc1.NbBadgeComponent], never, [typeof ɵngcc1.NbBadgeComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbBadgeModule>;
}

//# sourceMappingURL=badge.module.d.ts.map