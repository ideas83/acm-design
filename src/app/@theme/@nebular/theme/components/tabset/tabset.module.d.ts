/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './tabset.component';
import * as ɵngcc2 from '../shared/shared.module';
import * as ɵngcc3 from '../badge/badge.module';
import * as ɵngcc4 from '../icon/icon.module';
export declare class NbTabsetModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbTabsetModule, [typeof ɵngcc1.NbTabsetComponent, typeof ɵngcc1.NbTabComponent], [typeof ɵngcc2.NbSharedModule, typeof ɵngcc3.NbBadgeModule, typeof ɵngcc4.NbIconModule], [typeof ɵngcc1.NbTabsetComponent, typeof ɵngcc1.NbTabComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbTabsetModule>;
}

//# sourceMappingURL=tabset.module.d.ts.map