/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './input.directive';
import * as ɵngcc2 from '../shared/shared.module';
export declare class NbInputModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbInputModule, [typeof ɵngcc1.NbInputDirective], [typeof ɵngcc2.NbSharedModule], [typeof ɵngcc1.NbInputDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbInputModule>;
}

//# sourceMappingURL=input.module.d.ts.map