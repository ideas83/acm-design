import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './list.component';
import * as ɵngcc2 from './list-page-tracker.directive';
import * as ɵngcc3 from './infinite-list.directive';
export declare class NbListModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbListModule, [typeof ɵngcc1.NbListComponent, typeof ɵngcc1.NbListItemComponent, typeof ɵngcc2.NbListPageTrackerDirective, typeof ɵngcc3.NbInfiniteListDirective], never, [typeof ɵngcc1.NbListComponent, typeof ɵngcc1.NbListItemComponent, typeof ɵngcc2.NbListPageTrackerDirective, typeof ɵngcc3.NbInfiniteListDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbListModule>;
}

//# sourceMappingURL=list.module.d.ts.map