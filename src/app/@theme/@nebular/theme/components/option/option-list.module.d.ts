import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './option-list.component';
import * as ɵngcc2 from './option.component';
import * as ɵngcc3 from './option-group.component';
import * as ɵngcc4 from '@angular/common';
import * as ɵngcc5 from '../checkbox/checkbox.module';
export declare class NbOptionModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbOptionModule, [typeof ɵngcc1.NbOptionListComponent, typeof ɵngcc2.NbOptionComponent, typeof ɵngcc3.NbOptionGroupComponent], [typeof ɵngcc4.CommonModule, typeof ɵngcc5.NbCheckboxModule], [typeof ɵngcc1.NbOptionListComponent, typeof ɵngcc2.NbOptionComponent, typeof ɵngcc3.NbOptionGroupComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbOptionModule>;
}

//# sourceMappingURL=option-list.module.d.ts.map