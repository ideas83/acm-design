/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './actions.component';
import * as ɵngcc2 from '../shared/shared.module';
import * as ɵngcc3 from '../badge/badge.module';
import * as ɵngcc4 from '../icon/icon.module';
export declare class NbActionsModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbActionsModule, [typeof ɵngcc1.NbActionComponent, typeof ɵngcc1.NbActionsComponent], [typeof ɵngcc2.NbSharedModule, typeof ɵngcc3.NbBadgeModule, typeof ɵngcc4.NbIconModule], [typeof ɵngcc1.NbActionComponent, typeof ɵngcc1.NbActionsComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbActionsModule>;
}

//# sourceMappingURL=actions.module.d.ts.map