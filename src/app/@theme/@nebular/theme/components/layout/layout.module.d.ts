import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './layout.component';
import * as ɵngcc2 from '../shared/shared.module';
export declare class NbLayoutModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbLayoutModule, [typeof ɵngcc1.NbLayoutComponent, typeof ɵngcc1.NbLayoutColumnComponent, typeof ɵngcc1.NbLayoutFooterComponent, typeof ɵngcc1.NbLayoutHeaderComponent], [typeof ɵngcc2.NbSharedModule], [typeof ɵngcc1.NbLayoutComponent, typeof ɵngcc1.NbLayoutColumnComponent, typeof ɵngcc1.NbLayoutFooterComponent, typeof ɵngcc1.NbLayoutHeaderComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbLayoutModule>;
}

//# sourceMappingURL=layout.module.d.ts.map