import { ComponentRef } from '@angular/core';
import { NbComponentPortal } from '../cdk/overlay/mapping';
import { NbOverlayContainerComponent, NbPositionedContainer } from '../cdk/overlay/overlay-container';
import * as ɵngcc0 from '@angular/core';
export declare class NbDatepickerContainerComponent extends NbPositionedContainer {
    overlayContainer: NbOverlayContainerComponent;
    attach<T>(portal: NbComponentPortal<T>): ComponentRef<T>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NbDatepickerContainerComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NbDatepickerContainerComponent, "nb-datepicker-container", never, {}, {}, never, never>;
}

//# sourceMappingURL=datepicker-container.component.d.ts.map