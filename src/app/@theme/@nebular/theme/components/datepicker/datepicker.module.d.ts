import { ModuleWithProviders } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './datepicker.directive';
import * as ɵngcc2 from './datepicker-container.component';
import * as ɵngcc3 from './datepicker.component';
import * as ɵngcc4 from '../cdk/overlay/overlay.module';
import * as ɵngcc5 from '../calendar/calendar.module';
import * as ɵngcc6 from '../calendar/calendar-range.module';
export declare class NbDatepickerModule {
    static forRoot(): ModuleWithProviders<NbDatepickerModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbDatepickerModule, [typeof ɵngcc1.NbDatepickerDirective, typeof ɵngcc2.NbDatepickerContainerComponent, typeof ɵngcc3.NbDatepickerComponent, typeof ɵngcc3.NbRangepickerComponent, typeof ɵngcc3.NbBasePickerComponent], [typeof ɵngcc4.NbOverlayModule, typeof ɵngcc5.NbCalendarModule, typeof ɵngcc6.NbCalendarRangeModule], [typeof ɵngcc1.NbDatepickerDirective, typeof ɵngcc3.NbDatepickerComponent, typeof ɵngcc3.NbRangepickerComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbDatepickerModule>;
}

//# sourceMappingURL=datepicker.module.d.ts.map