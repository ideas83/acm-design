import { Directive, Inject, Injectable, InjectionToken, Input, NgModule, Optional, TemplateRef, ViewContainerRef } from '@angular/core';
import { __decorate, __metadata, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import * as ɵngcc0 from '@angular/core';
const NB_SECURITY_OPTIONS_TOKEN = new InjectionToken('Nebular Security Options');

var NbAclService_1;
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
const shallowObjectClone = (o) => Object.assign({}, o);
const ɵ0 = shallowObjectClone;
const shallowArrayClone = (a) => Object.assign([], a);
const ɵ1 = shallowArrayClone;
const popParent = (abilities) => {
    const parent = abilities['parent'];
    delete abilities['parent'];
    return parent;
};
const ɵ2 = popParent;
/**
 * Common acl service.
 */
let NbAclService = NbAclService_1 = class NbAclService {
    constructor(settings = {}) {
        this.settings = settings;
        this.state = {};
        if (settings.accessControl) {
            this.setAccessControl(settings.accessControl);
        }
    }
    /**
     * Set/Reset ACL list
     * @param {NbAccessControl} list
     */
    setAccessControl(list) {
        for (const [role, value] of Object.entries(list)) {
            const abilities = shallowObjectClone(value);
            const parent = popParent(abilities);
            this.register(role, parent, abilities);
        }
    }
    /**
     * Register a new role with a list of abilities (permission/resources combinations)
     * @param {string} role
     * @param {string} parent
     * @param {[permission: string]: string|string[]} abilities
     */
    register(role, parent = null, abilities = {}) {
        this.validateRole(role);
        this.state[role] = {
            parent: parent,
        };
        for (const [permission, value] of Object.entries(abilities)) {
            const resources = typeof value === 'string' ? [value] : value;
            this.allow(role, permission, shallowArrayClone(resources));
        }
    }
    /**
     * Allow a permission for specific resources to a role
     * @param {string} role
     * @param {string} permission
     * @param {string | string[]} resource
     */
    allow(role, permission, resource) {
        this.validateRole(role);
        if (!this.getRole(role)) {
            this.register(role, null, {});
        }
        resource = typeof resource === 'string' ? [resource] : resource;
        let resources = shallowArrayClone(this.getRoleResources(role, permission));
        resources = resources.concat(resource);
        this.state[role][permission] = resources
            .filter((item, pos) => resources.indexOf(item) === pos);
    }
    /**
     * Check whether the role has a permission to a resource
     * @param {string} role
     * @param {string} permission
     * @param {string} resource
     * @returns {boolean}
     */
    can(role, permission, resource) {
        this.validateResource(resource);
        const parentRole = this.getRoleParent(role);
        const parentCan = parentRole && this.can(this.getRoleParent(role), permission, resource);
        return parentCan || this.exactCan(role, permission, resource);
    }
    getRole(role) {
        return this.state[role];
    }
    validateRole(role) {
        if (!role) {
            throw new Error('NbAclService: role name cannot be empty');
        }
    }
    validateResource(resource) {
        if (!resource || [NbAclService_1.ANY_RESOURCE].includes(resource)) {
            throw new Error(`NbAclService: cannot use empty or bulk '*' resource placeholder with 'can' method`);
        }
    }
    exactCan(role, permission, resource) {
        const resources = this.getRoleResources(role, permission);
        return resources.includes(resource) || resources.includes(NbAclService_1.ANY_RESOURCE);
    }
    getRoleResources(role, permission) {
        return this.getRoleAbilities(role)[permission] || [];
    }
    getRoleAbilities(role) {
        const abilities = shallowObjectClone(this.state[role] || {});
        popParent(shallowObjectClone(this.state[role] || {}));
        return abilities;
    }
    getRoleParent(role) {
        return this.state[role] ? this.state[role]['parent'] : null;
    }
};
NbAclService.ɵfac = function NbAclService_Factory(t) { return new (t || NbAclService)(ɵngcc0.ɵɵinject(NB_SECURITY_OPTIONS_TOKEN, 8)); };
NbAclService.ɵprov = ɵngcc0.ɵɵdefineInjectable({ token: NbAclService, factory: function (t) { return NbAclService.ɵfac(t); } });
NbAclService.ANY_RESOURCE = '*';
NbAclService = NbAclService_1 = __decorate([ __param(0, Optional()), __param(0, Inject(NB_SECURITY_OPTIONS_TOKEN)),
    __metadata("design:paramtypes", [Object])
], NbAclService);

class NbRoleProvider {
}

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Access checker service.
 *
 * Injects `NbRoleProvider` to determine current user role, and checks access permissions using `NbAclService`
 */
let NbAccessChecker = class NbAccessChecker {
    constructor(roleProvider, acl) {
        this.roleProvider = roleProvider;
        this.acl = acl;
    }
    /**
     * Checks whether access is granted or not
     *
     * @param {string} permission
     * @param {string} resource
     * @returns {Observable<boolean>}
     */
    isGranted(permission, resource) {
        return this.roleProvider.getRole()
            .pipe(map((role) => Array.isArray(role) ? role : [role]), map((roles) => {
            return roles.some(role => this.acl.can(role, permission, resource));
        }));
    }
};
NbAccessChecker.ɵfac = function NbAccessChecker_Factory(t) { return new (t || NbAccessChecker)(ɵngcc0.ɵɵinject(NbRoleProvider), ɵngcc0.ɵɵinject(NbAclService)); };
NbAccessChecker.ɵprov = ɵngcc0.ɵɵdefineInjectable({ token: NbAccessChecker, factory: function (t) { return NbAccessChecker.ɵfac(t); } });
NbAccessChecker = __decorate([ __metadata("design:paramtypes", [NbRoleProvider, NbAclService])
], NbAccessChecker);

let NbIsGrantedDirective = class NbIsGrantedDirective {
    constructor(templateRef, viewContainer, accessChecker) {
        this.templateRef = templateRef;
        this.viewContainer = viewContainer;
        this.accessChecker = accessChecker;
        this.destroy$ = new Subject();
        this.hasView = false;
    }
    set nbIsGranted([permission, resource]) {
        this.accessChecker.isGranted(permission, resource)
            .pipe(takeUntil(this.destroy$))
            .subscribe((can) => {
            if (can && !this.hasView) {
                this.viewContainer.createEmbeddedView(this.templateRef);
                this.hasView = true;
            }
            else if (!can && this.hasView) {
                this.viewContainer.clear();
                this.hasView = false;
            }
        });
    }
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
};
NbIsGrantedDirective.ɵfac = function NbIsGrantedDirective_Factory(t) { return new (t || NbIsGrantedDirective)(ɵngcc0.ɵɵdirectiveInject(ɵngcc0.TemplateRef), ɵngcc0.ɵɵdirectiveInject(ɵngcc0.ViewContainerRef), ɵngcc0.ɵɵdirectiveInject(NbAccessChecker)); };
NbIsGrantedDirective.ɵdir = ɵngcc0.ɵɵdefineDirective({ type: NbIsGrantedDirective, selectors: [["", "nbIsGranted", ""]], inputs: { nbIsGranted: "nbIsGranted" } });
__decorate([
    Input(),
    __metadata("design:type", Array),
    __metadata("design:paramtypes", [Array])
], NbIsGrantedDirective.prototype, "nbIsGranted", null);
NbIsGrantedDirective = __decorate([ __metadata("design:paramtypes", [TemplateRef,
        ViewContainerRef,
        NbAccessChecker])
], NbIsGrantedDirective);

var NbSecurityModule_1;
let NbSecurityModule = NbSecurityModule_1 = class NbSecurityModule {
    static forRoot(nbSecurityOptions) {
        return {
            ngModule: NbSecurityModule_1,
            providers: [
                { provide: NB_SECURITY_OPTIONS_TOKEN, useValue: nbSecurityOptions },
                NbAclService,
                NbAccessChecker,
            ],
        };
    }
};
NbSecurityModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: NbSecurityModule });
NbSecurityModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function NbSecurityModule_Factory(t) { return new (t || NbSecurityModule)(); }, imports: [[
            CommonModule,
        ]] });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(NbAclService, [{
        type: Injectable
    }], function () { return [{ type: Object, decorators: [{
                type: Optional
            }, {
                type: Inject,
                args: [NB_SECURITY_OPTIONS_TOKEN]
            }] }]; }, null); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(NbAccessChecker, [{
        type: Injectable
    }], function () { return [{ type: NbRoleProvider }, { type: NbAclService }]; }, null); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(NbIsGrantedDirective, [{
        type: Directive,
        args: [{ selector: '[nbIsGranted]' }]
    }], function () { return [{ type: ɵngcc0.TemplateRef }, { type: ɵngcc0.ViewContainerRef }, { type: NbAccessChecker }]; }, { nbIsGranted: [{
            type: Input
        }] }); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵngcc0.ɵɵsetNgModuleScope(NbSecurityModule, { declarations: function () { return [NbIsGrantedDirective]; }, imports: function () { return [CommonModule]; }, exports: function () { return [NbIsGrantedDirective]; } }); })();
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(NbSecurityModule, [{
        type: NgModule,
        args: [{
                imports: [
                    CommonModule,
                ],
                declarations: [
                    NbIsGrantedDirective,
                ],
                exports: [
                    NbIsGrantedDirective,
                ]
            }]
    }], null, null); })();

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * Generated bundle index. Do not edit.
 */

export { NB_SECURITY_OPTIONS_TOKEN, NbSecurityModule, NbAclService, ɵ0, ɵ1, ɵ2, NbAccessChecker, NbRoleProvider, NbIsGrantedDirective };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Z0lBQUU7QUFDRjtBQUNBLDRDQUVHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7eUlBQUU7QUFDRiw4QkFFRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O21LQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUVHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzBCQWFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEluamVjdCwgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4sIElucHV0LCBOZ01vZHVsZSwgT3B0aW9uYWwsIFRlbXBsYXRlUmVmLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBfX2RlY29yYXRlLCBfX21ldGFkYXRhLCBfX3BhcmFtIH0gZnJvbSAndHNsaWInO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IG1hcCwgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5jb25zdCBOQl9TRUNVUklUWV9PUFRJT05TX1RPS0VOID0gbmV3IEluamVjdGlvblRva2VuKCdOZWJ1bGFyIFNlY3VyaXR5IE9wdGlvbnMnKTtcblxudmFyIE5iQWNsU2VydmljZV8xO1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuY29uc3Qgc2hhbGxvd09iamVjdENsb25lID0gKG8pID0+IE9iamVjdC5hc3NpZ24oe30sIG8pO1xuY29uc3QgybUwID0gc2hhbGxvd09iamVjdENsb25lO1xuY29uc3Qgc2hhbGxvd0FycmF5Q2xvbmUgPSAoYSkgPT4gT2JqZWN0LmFzc2lnbihbXSwgYSk7XG5jb25zdCDJtTEgPSBzaGFsbG93QXJyYXlDbG9uZTtcbmNvbnN0IHBvcFBhcmVudCA9IChhYmlsaXRpZXMpID0+IHtcbiAgICBjb25zdCBwYXJlbnQgPSBhYmlsaXRpZXNbJ3BhcmVudCddO1xuICAgIGRlbGV0ZSBhYmlsaXRpZXNbJ3BhcmVudCddO1xuICAgIHJldHVybiBwYXJlbnQ7XG59O1xuY29uc3QgybUyID0gcG9wUGFyZW50O1xuLyoqXG4gKiBDb21tb24gYWNsIHNlcnZpY2UuXG4gKi9cbmxldCBOYkFjbFNlcnZpY2UgPSBOYkFjbFNlcnZpY2VfMSA9IGNsYXNzIE5iQWNsU2VydmljZSB7XG4gICAgY29uc3RydWN0b3Ioc2V0dGluZ3MgPSB7fSkge1xuICAgICAgICB0aGlzLnNldHRpbmdzID0gc2V0dGluZ3M7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgICAgICAgaWYgKHNldHRpbmdzLmFjY2Vzc0NvbnRyb2wpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0QWNjZXNzQ29udHJvbChzZXR0aW5ncy5hY2Nlc3NDb250cm9sKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBTZXQvUmVzZXQgQUNMIGxpc3RcbiAgICAgKiBAcGFyYW0ge05iQWNjZXNzQ29udHJvbH0gbGlzdFxuICAgICAqL1xuICAgIHNldEFjY2Vzc0NvbnRyb2wobGlzdCkge1xuICAgICAgICBmb3IgKGNvbnN0IFtyb2xlLCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMobGlzdCkpIHtcbiAgICAgICAgICAgIGNvbnN0IGFiaWxpdGllcyA9IHNoYWxsb3dPYmplY3RDbG9uZSh2YWx1ZSk7XG4gICAgICAgICAgICBjb25zdCBwYXJlbnQgPSBwb3BQYXJlbnQoYWJpbGl0aWVzKTtcbiAgICAgICAgICAgIHRoaXMucmVnaXN0ZXIocm9sZSwgcGFyZW50LCBhYmlsaXRpZXMpO1xuICAgICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJlZ2lzdGVyIGEgbmV3IHJvbGUgd2l0aCBhIGxpc3Qgb2YgYWJpbGl0aWVzIChwZXJtaXNzaW9uL3Jlc291cmNlcyBjb21iaW5hdGlvbnMpXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJvbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGFyZW50XG4gICAgICogQHBhcmFtIHtbcGVybWlzc2lvbjogc3RyaW5nXTogc3RyaW5nfHN0cmluZ1tdfSBhYmlsaXRpZXNcbiAgICAgKi9cbiAgICByZWdpc3Rlcihyb2xlLCBwYXJlbnQgPSBudWxsLCBhYmlsaXRpZXMgPSB7fSkge1xuICAgICAgICB0aGlzLnZhbGlkYXRlUm9sZShyb2xlKTtcbiAgICAgICAgdGhpcy5zdGF0ZVtyb2xlXSA9IHtcbiAgICAgICAgICAgIHBhcmVudDogcGFyZW50LFxuICAgICAgICB9O1xuICAgICAgICBmb3IgKGNvbnN0IFtwZXJtaXNzaW9uLCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMoYWJpbGl0aWVzKSkge1xuICAgICAgICAgICAgY29uc3QgcmVzb3VyY2VzID0gdHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJyA/IFt2YWx1ZV0gOiB2YWx1ZTtcbiAgICAgICAgICAgIHRoaXMuYWxsb3cocm9sZSwgcGVybWlzc2lvbiwgc2hhbGxvd0FycmF5Q2xvbmUocmVzb3VyY2VzKSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLyoqXG4gICAgICogQWxsb3cgYSBwZXJtaXNzaW9uIGZvciBzcGVjaWZpYyByZXNvdXJjZXMgdG8gYSByb2xlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJvbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGVybWlzc2lvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nIHwgc3RyaW5nW119IHJlc291cmNlXG4gICAgICovXG4gICAgYWxsb3cocm9sZSwgcGVybWlzc2lvbiwgcmVzb3VyY2UpIHtcbiAgICAgICAgdGhpcy52YWxpZGF0ZVJvbGUocm9sZSk7XG4gICAgICAgIGlmICghdGhpcy5nZXRSb2xlKHJvbGUpKSB7XG4gICAgICAgICAgICB0aGlzLnJlZ2lzdGVyKHJvbGUsIG51bGwsIHt9KTtcbiAgICAgICAgfVxuICAgICAgICByZXNvdXJjZSA9IHR5cGVvZiByZXNvdXJjZSA9PT0gJ3N0cmluZycgPyBbcmVzb3VyY2VdIDogcmVzb3VyY2U7XG4gICAgICAgIGxldCByZXNvdXJjZXMgPSBzaGFsbG93QXJyYXlDbG9uZSh0aGlzLmdldFJvbGVSZXNvdXJjZXMocm9sZSwgcGVybWlzc2lvbikpO1xuICAgICAgICByZXNvdXJjZXMgPSByZXNvdXJjZXMuY29uY2F0KHJlc291cmNlKTtcbiAgICAgICAgdGhpcy5zdGF0ZVtyb2xlXVtwZXJtaXNzaW9uXSA9IHJlc291cmNlc1xuICAgICAgICAgICAgLmZpbHRlcigoaXRlbSwgcG9zKSA9PiByZXNvdXJjZXMuaW5kZXhPZihpdGVtKSA9PT0gcG9zKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ2hlY2sgd2hldGhlciB0aGUgcm9sZSBoYXMgYSBwZXJtaXNzaW9uIHRvIGEgcmVzb3VyY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcm9sZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwZXJtaXNzaW9uXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlc291cmNlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgY2FuKHJvbGUsIHBlcm1pc3Npb24sIHJlc291cmNlKSB7XG4gICAgICAgIHRoaXMudmFsaWRhdGVSZXNvdXJjZShyZXNvdXJjZSk7XG4gICAgICAgIGNvbnN0IHBhcmVudFJvbGUgPSB0aGlzLmdldFJvbGVQYXJlbnQocm9sZSk7XG4gICAgICAgIGNvbnN0IHBhcmVudENhbiA9IHBhcmVudFJvbGUgJiYgdGhpcy5jYW4odGhpcy5nZXRSb2xlUGFyZW50KHJvbGUpLCBwZXJtaXNzaW9uLCByZXNvdXJjZSk7XG4gICAgICAgIHJldHVybiBwYXJlbnRDYW4gfHwgdGhpcy5leGFjdENhbihyb2xlLCBwZXJtaXNzaW9uLCByZXNvdXJjZSk7XG4gICAgfVxuICAgIGdldFJvbGUocm9sZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZVtyb2xlXTtcbiAgICB9XG4gICAgdmFsaWRhdGVSb2xlKHJvbGUpIHtcbiAgICAgICAgaWYgKCFyb2xlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05iQWNsU2VydmljZTogcm9sZSBuYW1lIGNhbm5vdCBiZSBlbXB0eScpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHZhbGlkYXRlUmVzb3VyY2UocmVzb3VyY2UpIHtcbiAgICAgICAgaWYgKCFyZXNvdXJjZSB8fCBbTmJBY2xTZXJ2aWNlXzEuQU5ZX1JFU09VUkNFXS5pbmNsdWRlcyhyZXNvdXJjZSkpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgTmJBY2xTZXJ2aWNlOiBjYW5ub3QgdXNlIGVtcHR5IG9yIGJ1bGsgJyonIHJlc291cmNlIHBsYWNlaG9sZGVyIHdpdGggJ2NhbicgbWV0aG9kYCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgZXhhY3RDYW4ocm9sZSwgcGVybWlzc2lvbiwgcmVzb3VyY2UpIHtcbiAgICAgICAgY29uc3QgcmVzb3VyY2VzID0gdGhpcy5nZXRSb2xlUmVzb3VyY2VzKHJvbGUsIHBlcm1pc3Npb24pO1xuICAgICAgICByZXR1cm4gcmVzb3VyY2VzLmluY2x1ZGVzKHJlc291cmNlKSB8fCByZXNvdXJjZXMuaW5jbHVkZXMoTmJBY2xTZXJ2aWNlXzEuQU5ZX1JFU09VUkNFKTtcbiAgICB9XG4gICAgZ2V0Um9sZVJlc291cmNlcyhyb2xlLCBwZXJtaXNzaW9uKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFJvbGVBYmlsaXRpZXMocm9sZSlbcGVybWlzc2lvbl0gfHwgW107XG4gICAgfVxuICAgIGdldFJvbGVBYmlsaXRpZXMocm9sZSkge1xuICAgICAgICBjb25zdCBhYmlsaXRpZXMgPSBzaGFsbG93T2JqZWN0Q2xvbmUodGhpcy5zdGF0ZVtyb2xlXSB8fCB7fSk7XG4gICAgICAgIHBvcFBhcmVudChzaGFsbG93T2JqZWN0Q2xvbmUodGhpcy5zdGF0ZVtyb2xlXSB8fCB7fSkpO1xuICAgICAgICByZXR1cm4gYWJpbGl0aWVzO1xuICAgIH1cbiAgICBnZXRSb2xlUGFyZW50KHJvbGUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGVbcm9sZV0gPyB0aGlzLnN0YXRlW3JvbGVdWydwYXJlbnQnXSA6IG51bGw7XG4gICAgfVxufTtcbk5iQWNsU2VydmljZS5BTllfUkVTT1VSQ0UgPSAnKic7XG5OYkFjbFNlcnZpY2UgPSBOYkFjbFNlcnZpY2VfMSA9IF9fZGVjb3JhdGUoW1xuICAgIEluamVjdGFibGUoKSxcbiAgICBfX3BhcmFtKDAsIE9wdGlvbmFsKCkpLCBfX3BhcmFtKDAsIEluamVjdChOQl9TRUNVUklUWV9PUFRJT05TX1RPS0VOKSksXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtPYmplY3RdKVxuXSwgTmJBY2xTZXJ2aWNlKTtcblxuY2xhc3MgTmJSb2xlUHJvdmlkZXIge1xufVxuXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEFjY2VzcyBjaGVja2VyIHNlcnZpY2UuXG4gKlxuICogSW5qZWN0cyBgTmJSb2xlUHJvdmlkZXJgIHRvIGRldGVybWluZSBjdXJyZW50IHVzZXIgcm9sZSwgYW5kIGNoZWNrcyBhY2Nlc3MgcGVybWlzc2lvbnMgdXNpbmcgYE5iQWNsU2VydmljZWBcbiAqL1xubGV0IE5iQWNjZXNzQ2hlY2tlciA9IGNsYXNzIE5iQWNjZXNzQ2hlY2tlciB7XG4gICAgY29uc3RydWN0b3Iocm9sZVByb3ZpZGVyLCBhY2wpIHtcbiAgICAgICAgdGhpcy5yb2xlUHJvdmlkZXIgPSByb2xlUHJvdmlkZXI7XG4gICAgICAgIHRoaXMuYWNsID0gYWNsO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDaGVja3Mgd2hldGhlciBhY2Nlc3MgaXMgZ3JhbnRlZCBvciBub3RcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwZXJtaXNzaW9uXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlc291cmNlXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Ym9vbGVhbj59XG4gICAgICovXG4gICAgaXNHcmFudGVkKHBlcm1pc3Npb24sIHJlc291cmNlKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJvbGVQcm92aWRlci5nZXRSb2xlKClcbiAgICAgICAgICAgIC5waXBlKG1hcCgocm9sZSkgPT4gQXJyYXkuaXNBcnJheShyb2xlKSA/IHJvbGUgOiBbcm9sZV0pLCBtYXAoKHJvbGVzKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gcm9sZXMuc29tZShyb2xlID0+IHRoaXMuYWNsLmNhbihyb2xlLCBwZXJtaXNzaW9uLCByZXNvdXJjZSkpO1xuICAgICAgICB9KSk7XG4gICAgfVxufTtcbk5iQWNjZXNzQ2hlY2tlciA9IF9fZGVjb3JhdGUoW1xuICAgIEluamVjdGFibGUoKSxcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW05iUm9sZVByb3ZpZGVyLCBOYkFjbFNlcnZpY2VdKVxuXSwgTmJBY2Nlc3NDaGVja2VyKTtcblxubGV0IE5iSXNHcmFudGVkRGlyZWN0aXZlID0gY2xhc3MgTmJJc0dyYW50ZWREaXJlY3RpdmUge1xuICAgIGNvbnN0cnVjdG9yKHRlbXBsYXRlUmVmLCB2aWV3Q29udGFpbmVyLCBhY2Nlc3NDaGVja2VyKSB7XG4gICAgICAgIHRoaXMudGVtcGxhdGVSZWYgPSB0ZW1wbGF0ZVJlZjtcbiAgICAgICAgdGhpcy52aWV3Q29udGFpbmVyID0gdmlld0NvbnRhaW5lcjtcbiAgICAgICAgdGhpcy5hY2Nlc3NDaGVja2VyID0gYWNjZXNzQ2hlY2tlcjtcbiAgICAgICAgdGhpcy5kZXN0cm95JCA9IG5ldyBTdWJqZWN0KCk7XG4gICAgICAgIHRoaXMuaGFzVmlldyA9IGZhbHNlO1xuICAgIH1cbiAgICBzZXQgbmJJc0dyYW50ZWQoW3Blcm1pc3Npb24sIHJlc291cmNlXSkge1xuICAgICAgICB0aGlzLmFjY2Vzc0NoZWNrZXIuaXNHcmFudGVkKHBlcm1pc3Npb24sIHJlc291cmNlKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveSQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZSgoY2FuKSA9PiB7XG4gICAgICAgICAgICBpZiAoY2FuICYmICF0aGlzLmhhc1ZpZXcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb250YWluZXIuY3JlYXRlRW1iZWRkZWRWaWV3KHRoaXMudGVtcGxhdGVSZWYpO1xuICAgICAgICAgICAgICAgIHRoaXMuaGFzVmlldyA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICghY2FuICYmIHRoaXMuaGFzVmlldykge1xuICAgICAgICAgICAgICAgIHRoaXMudmlld0NvbnRhaW5lci5jbGVhcigpO1xuICAgICAgICAgICAgICAgIHRoaXMuaGFzVmlldyA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveSQubmV4dCgpO1xuICAgICAgICB0aGlzLmRlc3Ryb3kkLmNvbXBsZXRlKCk7XG4gICAgfVxufTtcbl9fZGVjb3JhdGUoW1xuICAgIElucHV0KCksXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjp0eXBlXCIsIEFycmF5KSxcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW0FycmF5XSlcbl0sIE5iSXNHcmFudGVkRGlyZWN0aXZlLnByb3RvdHlwZSwgXCJuYklzR3JhbnRlZFwiLCBudWxsKTtcbk5iSXNHcmFudGVkRGlyZWN0aXZlID0gX19kZWNvcmF0ZShbXG4gICAgRGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbbmJJc0dyYW50ZWRdJyB9KSxcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW1RlbXBsYXRlUmVmLFxuICAgICAgICBWaWV3Q29udGFpbmVyUmVmLFxuICAgICAgICBOYkFjY2Vzc0NoZWNrZXJdKVxuXSwgTmJJc0dyYW50ZWREaXJlY3RpdmUpO1xuXG52YXIgTmJTZWN1cml0eU1vZHVsZV8xO1xubGV0IE5iU2VjdXJpdHlNb2R1bGUgPSBOYlNlY3VyaXR5TW9kdWxlXzEgPSBjbGFzcyBOYlNlY3VyaXR5TW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChuYlNlY3VyaXR5T3B0aW9ucykge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IE5iU2VjdXJpdHlNb2R1bGVfMSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogTkJfU0VDVVJJVFlfT1BUSU9OU19UT0tFTiwgdXNlVmFsdWU6IG5iU2VjdXJpdHlPcHRpb25zIH0sXG4gICAgICAgICAgICAgICAgTmJBY2xTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIE5iQWNjZXNzQ2hlY2tlcixcbiAgICAgICAgICAgIF0sXG4gICAgICAgIH07XG4gICAgfVxufTtcbk5iU2VjdXJpdHlNb2R1bGUgPSBOYlNlY3VyaXR5TW9kdWxlXzEgPSBfX2RlY29yYXRlKFtcbiAgICBOZ01vZHVsZSh7XG4gICAgICAgIGltcG9ydHM6IFtcbiAgICAgICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgXSxcbiAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICBOYklzR3JhbnRlZERpcmVjdGl2ZSxcbiAgICAgICAgXSxcbiAgICAgICAgZXhwb3J0czogW1xuICAgICAgICAgICAgTmJJc0dyYW50ZWREaXJlY3RpdmUsXG4gICAgICAgIF0sXG4gICAgfSlcbl0sIE5iU2VjdXJpdHlNb2R1bGUpO1xuXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbi8qKlxuICogR2VuZXJhdGVkIGJ1bmRsZSBpbmRleC4gRG8gbm90IGVkaXQuXG4gKi9cblxuZXhwb3J0IHsgTkJfU0VDVVJJVFlfT1BUSU9OU19UT0tFTiwgTmJTZWN1cml0eU1vZHVsZSwgTmJBY2xTZXJ2aWNlLCDJtTAsIMm1MSwgybUyLCBOYkFjY2Vzc0NoZWNrZXIsIE5iUm9sZVByb3ZpZGVyLCBOYklzR3JhbnRlZERpcmVjdGl2ZSB9O1xuIl19