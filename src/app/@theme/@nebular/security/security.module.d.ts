import { ModuleWithProviders } from '@angular/core';
import { NbAclOptions } from './security.options';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './directives/is-granted.directive';
import * as ɵngcc2 from '@angular/common';
export declare class NbSecurityModule {
    static forRoot(nbSecurityOptions?: NbAclOptions): ModuleWithProviders<NbSecurityModule>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<NbSecurityModule, [typeof ɵngcc1.NbIsGrantedDirective], [typeof ɵngcc2.CommonModule], [typeof ɵngcc1.NbIsGrantedDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<NbSecurityModule>;
}

//# sourceMappingURL=security.module.d.ts.map