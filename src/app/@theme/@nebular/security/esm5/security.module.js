import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NB_SECURITY_OPTIONS_TOKEN } from './security.options';
import { NbAclService } from './services/acl.service';
import { NbAccessChecker } from './services/access-checker.service';
import { NbIsGrantedDirective } from './directives/is-granted.directive';
var NbSecurityModule = /** @class */ (function () {
    function NbSecurityModule() {
    }
    NbSecurityModule_1 = NbSecurityModule;
    NbSecurityModule.forRoot = function (nbSecurityOptions) {
        return {
            ngModule: NbSecurityModule_1,
            providers: [
                { provide: NB_SECURITY_OPTIONS_TOKEN, useValue: nbSecurityOptions },
                NbAclService,
                NbAccessChecker,
            ],
        };
    };
    var NbSecurityModule_1;
    NbSecurityModule = NbSecurityModule_1 = __decorate([
        NgModule({
            imports: [
                CommonModule,
            ],
            declarations: [
                NbIsGrantedDirective,
            ],
            exports: [
                NbIsGrantedDirective,
            ],
        })
    ], NbSecurityModule);
    return NbSecurityModule;
}());
export { NbSecurityModule };
//# sourceMappingURL=security.module.js.map