import { __decorate, __metadata } from 'tslib';
import { NgModule } from '@angular/core';
import { NbIconLibraries, NbSvgIcon } from '@nebular/theme';
import { icons } from 'eva-icons';

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@nebular/theme';
class NbEvaSvgIcon extends NbSvgIcon {
    constructor(name, content) {
        super(name, '');
        this.name = name;
        this.content = content;
    }
    getContent(options) {
        return this.content.toSvg(Object.assign({ width: '100%', height: '100%', fill: 'currentColor' }, options));
    }
}
let NbEvaIconsModule = class NbEvaIconsModule {
    constructor(iconLibrary) {
        this.NAME = 'eva';
        iconLibrary.registerSvgPack(this.NAME, this.createIcons());
        iconLibrary.setDefaultPack(this.NAME);
    }
    createIcons() {
        return Object
            .entries(icons)
            .map(([name, icon]) => {
            return [name, new NbEvaSvgIcon(name, icon)];
        })
            .reduce((newIcons, [name, icon]) => {
            newIcons[name] = icon;
            return newIcons;
        }, {});
    }
};
NbEvaIconsModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: NbEvaIconsModule });
NbEvaIconsModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function NbEvaIconsModule_Factory(t) { return new (t || NbEvaIconsModule)(ɵngcc0.ɵɵinject(ɵngcc1.NbIconLibraries)); } });
NbEvaIconsModule = __decorate([ __metadata("design:paramtypes", [NbIconLibraries])
], NbEvaIconsModule);
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(NbEvaIconsModule, [{
        type: NgModule,
        args: [{}]
    }], function () { return [{ type: ɵngcc1.NbIconLibraries }]; }, null); })();

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * Generated bundle index. Do not edit.
 */

export { NbEvaSvgIcon, NbEvaIconsModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7b0xBQUU7QUFDRiwrQkFFRztBQUNIOzs7O2dGQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBfX2RlY29yYXRlLCBfX21ldGFkYXRhIH0gZnJvbSAndHNsaWInO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5iSWNvbkxpYnJhcmllcywgTmJTdmdJY29uIH0gZnJvbSAnQG5lYnVsYXIvdGhlbWUnO1xuaW1wb3J0IHsgaWNvbnMgfSBmcm9tICdldmEtaWNvbnMnO1xuXG4vKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbmNsYXNzIE5iRXZhU3ZnSWNvbiBleHRlbmRzIE5iU3ZnSWNvbiB7XG4gICAgY29uc3RydWN0b3IobmFtZSwgY29udGVudCkge1xuICAgICAgICBzdXBlcihuYW1lLCAnJyk7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuY29udGVudCA9IGNvbnRlbnQ7XG4gICAgfVxuICAgIGdldENvbnRlbnQob3B0aW9ucykge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50LnRvU3ZnKE9iamVjdC5hc3NpZ24oeyB3aWR0aDogJzEwMCUnLCBoZWlnaHQ6ICcxMDAlJywgZmlsbDogJ2N1cnJlbnRDb2xvcicgfSwgb3B0aW9ucykpO1xuICAgIH1cbn1cbmxldCBOYkV2YUljb25zTW9kdWxlID0gY2xhc3MgTmJFdmFJY29uc01vZHVsZSB7XG4gICAgY29uc3RydWN0b3IoaWNvbkxpYnJhcnkpIHtcbiAgICAgICAgdGhpcy5OQU1FID0gJ2V2YSc7XG4gICAgICAgIGljb25MaWJyYXJ5LnJlZ2lzdGVyU3ZnUGFjayh0aGlzLk5BTUUsIHRoaXMuY3JlYXRlSWNvbnMoKSk7XG4gICAgICAgIGljb25MaWJyYXJ5LnNldERlZmF1bHRQYWNrKHRoaXMuTkFNRSk7XG4gICAgfVxuICAgIGNyZWF0ZUljb25zKCkge1xuICAgICAgICByZXR1cm4gT2JqZWN0XG4gICAgICAgICAgICAuZW50cmllcyhpY29ucylcbiAgICAgICAgICAgIC5tYXAoKFtuYW1lLCBpY29uXSkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIFtuYW1lLCBuZXcgTmJFdmFTdmdJY29uKG5hbWUsIGljb24pXTtcbiAgICAgICAgfSlcbiAgICAgICAgICAgIC5yZWR1Y2UoKG5ld0ljb25zLCBbbmFtZSwgaWNvbl0pID0+IHtcbiAgICAgICAgICAgIG5ld0ljb25zW25hbWVdID0gaWNvbjtcbiAgICAgICAgICAgIHJldHVybiBuZXdJY29ucztcbiAgICAgICAgfSwge30pO1xuICAgIH1cbn07XG5OYkV2YUljb25zTW9kdWxlID0gX19kZWNvcmF0ZShbXG4gICAgTmdNb2R1bGUoe30pLFxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbTmJJY29uTGlicmFyaWVzXSlcbl0sIE5iRXZhSWNvbnNNb2R1bGUpO1xuXG4vKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuLyoqXG4gKiBHZW5lcmF0ZWQgYnVuZGxlIGluZGV4LiBEbyBub3QgZWRpdC5cbiAqL1xuXG5leHBvcnQgeyBOYkV2YVN2Z0ljb24sIE5iRXZhSWNvbnNNb2R1bGUgfTtcbiJdfQ==