import { __assign, __decorate, __extends, __metadata } from 'tslib';
import { NgModule } from '@angular/core';
import { NbIconLibraries, NbSvgIcon } from '@nebular/theme';
import { icons } from 'eva-icons';

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbEvaSvgIcon = /** @class */ (function (_super) {
    __extends(NbEvaSvgIcon, _super);
    function NbEvaSvgIcon(name, content) {
        var _this = _super.call(this, name, '') || this;
        _this.name = name;
        _this.content = content;
        return _this;
    }
    NbEvaSvgIcon.prototype.getContent = function (options) {
        return this.content.toSvg(__assign({ width: '100%', height: '100%', fill: 'currentColor' }, options));
    };
    return NbEvaSvgIcon;
}(NbSvgIcon));
var NbEvaIconsModule = /** @class */ (function () {
    function NbEvaIconsModule(iconLibrary) {
        this.NAME = 'eva';
        iconLibrary.registerSvgPack(this.NAME, this.createIcons());
        iconLibrary.setDefaultPack(this.NAME);
    }
    NbEvaIconsModule.prototype.createIcons = function () {
        return Object
            .entries(icons)
            .map(function (_a) {
            var name = _a[0], icon = _a[1];
            return [name, new NbEvaSvgIcon(name, icon)];
        })
            .reduce(function (newIcons, _a) {
            var name = _a[0], icon = _a[1];
            newIcons[name] = icon;
            return newIcons;
        }, {});
    };
    NbEvaIconsModule = __decorate([
        NgModule({}),
        __metadata("design:paramtypes", [NbIconLibraries])
    ], NbEvaIconsModule);
    return NbEvaIconsModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * Generated bundle index. Do not edit.
 */

export { NbEvaSvgIcon, NbEvaIconsModule };
