import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addstage',
  templateUrl: './addstage.component.html',
  styleUrls: ['./addstage.component.scss']
})
export class AddstageComponent implements OnInit {

  public stage = {
    stream_provider : null,
    stream_key : null,
    RTMP_url : null,
    segment_backstage_link:null,
    org_id:1,
    event_id:1,
    managerid:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  stageForm: FormGroup;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.stageForm = new FormGroup({
      stream_provider: new FormControl(),
      stream_key: new FormControl(),
      RTMP_url: new FormControl(),
      segment_backstage_link: new FormControl(),
      org_id: new FormControl(1),
      event_id: new FormControl(1),
      managerid: new FormControl(1),
   });
  }
  addstage()
  {
    this.stage = this.stageForm.value;
    this.dataService.addstage(this.stage).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/stage');
    })
    
  }

}
