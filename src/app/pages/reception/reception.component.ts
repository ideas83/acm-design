import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-reception',
  templateUrl: './reception.component.html',
  styleUrls: ['./reception.component.scss']
})
export class ReceptionComponent implements OnInit {
  arreception: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      reception_data: {
        title: 'Description',
        type: 'text',
      },
      event_banner: {
        title: 'Event Banner',
        type: 'string',
      },
      announcement_feed: {
        title: 'Announcement Feed',
        type: 'number',
      },
      welcome_video_link: {
        title: 'Welcome Video',
        type: 'string',
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }

  ngOnInit(): void {
    this.showreceptions(this.userDisplayID);
  }
  showreceptions(userDisplayID){
   
    this.dataService.showreceptions( this.userDisplayID).subscribe(res=>{
      this.arreception = res;
	  console.log(this.arreception);
  })
  }
  updatereception(event) {

    var data = {"reception_data": event.newData.reception_data,
                "event_banner" : event.newData.event_banner,
                "announcement_feed" : event.newData.announcement_feed,
                "welcome_video_link" : event.newData.welcome_video_link,
                "org_id":"1",
                "event_id":"1",
                "managerid":"1"
                };
                console.log(data);
  return this.http.post('https://server.kodeshode.com/api/updatereception/'+event.newData.id, data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.showreceptions(this.userDisplayID);
      });
  }
  deletereception(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('https://server.kodeshode.com/api/deletereception/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showreceptions(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
   
 }
}
