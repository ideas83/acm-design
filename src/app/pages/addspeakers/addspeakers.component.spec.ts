import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddspeakersComponent } from './addspeakers.component';

describe('AddspeakersComponent', () => {
  let component: AddspeakersComponent;
  let fixture: ComponentFixture<AddspeakersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddspeakersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddspeakersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
