import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addorganization',
  templateUrl: './addorganization.component.html',
  styleUrls: ['./addorganization.component.css']
})
export class AddorganizationComponent implements OnInit {
  public org = {
    title : null,
    contact_person : null,
    contact_number : null,
    email_id:null,
    clientid:null,
    notes:null,
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  orgForm: FormGroup;
  
  constructor(private dataService:DataService,private Auth: AuthService,
  
    private Token: TokenService,private router: Router,) { }
    get title() { return this.orgForm.get('title'); }

  ngOnInit(): void {
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.userDisplayID = sessionStorage.getItem('loggedUserid');
    console.log(this.userDisplayID);

    this.orgForm = new FormGroup({
      title: new FormControl(),
      contact_person: new FormControl(),
      contact_number: new FormControl(),
      email_id: new FormControl(),
      clientid: new FormControl(),
   });
  }
  addorg()
  {
    this.org = this.orgForm.value;
    this.dataService.addorg(this.org).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/organizations');
    })
    
  }
  
}
