import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-updatespeakers',
  templateUrl: './updatespeakers.component.html',
  styleUrls: ['./updatespeakers.component.scss']
})
export class UpdatespeakersComponent implements OnInit {
 
  public speaker = {
    first_name : null,
    last_name : null,
    email : null,
    title:null,
    profile_image:null,
    company:null,
    biography:null,
    linkdin:null,
    twitter:null,
    facebook:null,
    event_id:13,
		managerid:5,
		session_id:7
  }
  id:any;
  data: any;
  speakersForm: FormGroup;
  files: any;

  constructor(private route:ActivatedRoute,
    private dataservice:DataService,
    private router: Router) { }

  ngOnInit(): void {
    this.id= this.route.snapshot.params.id;
    this.getspeaker();
  }
  getspeaker(){
    this.dataservice.singlespeaker(this.id).subscribe(res=>{
      this.data = res;
      console.log(this.data);
      this.speaker.first_name = this.data.first_name;
      this.speaker=this.data;
      console.log( this.id);
      console.log(this.speaker.first_name);
    })
  }
  updatespeaker(){
    this.speaker = this.speakersForm.value;
      let formdata = new FormData;
      formdata.append("first_name",this.speakersForm.get('first_name').value);
      formdata.append("last_name",this.speakersForm.get('last_name').value);
      formdata.append("email",this.speakersForm.get('email').value);
      formdata.append("title",this.speakersForm.get('title').value);
      formdata.append("profile_image",this.speakersForm.get('profile_image').value);
      formdata.append("company",this.speakersForm.get('company').value);
      formdata.append("biography",this.speakersForm.get('biography').value);
      formdata.append("linkdin",this.speakersForm.get('linkdin').value);
      formdata.append("twitter",this.speakersForm.get('twitter').value);
      formdata.append("facebook",this.speakersForm.get('facebook').value);
      formdata.append("session_id",this.speakersForm.get('session_id').value);
      formdata.append("event_id",this.speakersForm.get('event_id').value);
      formdata.append("managerid",this.speakersForm.get('managerid').value);
    this.dataservice.updatespeaker(this.id,formdata).subscribe(res=>{
      this.router.navigateByUrl('/pages/speakers');
    })
  }
  imageupload(event){
    if (event.target.files.length > 0) {
     const file = event.target.files[0];
     this.speakersForm.get('profile_image').setValue(file);
     console.log(this.speakersForm.get('profile_image').value);
   }
 }
}
