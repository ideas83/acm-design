import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  [x: string]: any;
 
  userDisplayID = '';
  public loggedIn: boolean;
  constructor(private dataService:DataService) { }
  arrspeakers: any=[];
 
  ngOnInit(): void {
    this.showspeaker(this.userDisplayID);
  }
  showspeaker(userDisplayID){
    this.dataService.showspeaker(this.userDisplayID).subscribe(res=>{
     var response2 = res;
  
      console.log( response2 );
      this.arrspeakers = response2;
  })
  }
  deletespeaker(id){
    this.dataService.deletespeaker(id).subscribe(res=>{
      this.showspeaker(this.userDisplayID);
      console.log(id);
    })
    
  }
 
}
