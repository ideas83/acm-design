import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-attendees',
  templateUrl: './attendees.component.html',
  styleUrls: ['./attendees.component.scss']
})
export class AttendeesComponent implements OnInit {

  [x: string]: any;
 
  userDisplayID = '';
  public loggedIn: boolean;
  constructor(private dataService:DataService) { }
  arrattendees: any=[];
 
  ngOnInit(): void {
    this.showattendee(this.userDisplayID);
  }
  showattendee(userDisplayID){
    this.dataService.showattendee(this.userDisplayID).subscribe(res=>{
     var response2 = res;
  
      console.log( response2 );
      this.arrattendees = response2;
  })
  }
  deleteattendee(id){
    this.dataService.deleteattendee(id).subscribe(res=>{
      this.showattendee(this.userDisplayID);
      console.log(id);
    })
    
  }
}
