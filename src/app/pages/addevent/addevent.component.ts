import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./addevent.component.scss']
})
export class AddeventComponent implements OnInit {
 
  public event = {
    title : null,
    start_date : null,
    end_date : null,
    email_id:null,
    photo:null,
    client_id:sessionStorage.getItem('loggedUserid'),
    organization_id:4,
    stage:null,
    sessions:null,
    networking:null,
    expo:null,
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
   
    this.userDisplayID = sessionStorage.getItem('loggedUserid');
    console.log(this.userDisplayID);
  }
  addevent()
  {
    this.dataService.addevent(this.event).subscribe(res=>{
        console.log(res);
    })
    
  }
  

}
