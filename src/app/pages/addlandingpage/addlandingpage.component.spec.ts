import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlandingpageComponent } from './addlandingpage.component';

describe('AddlandingpageComponent', () => {
  let component: AddlandingpageComponent;
  let fixture: ComponentFixture<AddlandingpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlandingpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlandingpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
