import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionsStageComponent } from './sessions-stage.component';

describe('SessionsStageComponent', () => {
  let component: SessionsStageComponent;
  let fixture: ComponentFixture<SessionsStageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionsStageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionsStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
