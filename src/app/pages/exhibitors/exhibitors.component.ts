import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-exhibitors',
  templateUrl: './exhibitors.component.html',
  styleUrls: ['./exhibitors.component.scss']
})
export class ExhibitorsComponent implements OnInit {

  arrexpo: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
      show:false,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      exibit_name: {
        title: 'Exhibitor Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      title: {
        title: 'Title',
        type: 'string',
      },
      stream_provider: {
        title: 'Stream Provider',
        type: 'string',
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }
   ngOnInit(): void {
    this.showexpo(this.userDisplayID);
  }
  showexpo(userDisplayID){
   
    this.dataService.showexpo( this.userDisplayID).subscribe(res=>{
      this.arrexpo = res;
	  console.log(this.arrexpo);
  })
  }
  
  deletexpo(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('http://13.244.115.67/server/api/deletexpo/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showexpo(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
    
   //event.confirm.resolve(event.source.data);

 }
}
