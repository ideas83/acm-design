import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatesponsorComponent } from './updatesponsor.component';

describe('UpdatesponsorComponent', () => {
  let component: UpdatesponsorComponent;
  let fixture: ComponentFixture<UpdatesponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatesponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatesponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
