import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddexhibitorsComponent } from './addexhibitors.component';

describe('AddexhibitorsComponent', () => {
  let component: AddexhibitorsComponent;
  let fixture: ComponentFixture<AddexhibitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddexhibitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddexhibitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
