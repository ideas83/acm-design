import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateemailattendeesComponent } from './updateemailattendees.component';

describe('UpdateemailattendeesComponent', () => {
  let component: UpdateemailattendeesComponent;
  let fixture: ComponentFixture<UpdateemailattendeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateemailattendeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateemailattendeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
