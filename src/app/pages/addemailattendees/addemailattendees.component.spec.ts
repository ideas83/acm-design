import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddemailattendeesComponent } from './addemailattendees.component';

describe('AddemailattendeesComponent', () => {
  let component: AddemailattendeesComponent;
  let fixture: ComponentFixture<AddemailattendeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddemailattendeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddemailattendeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
