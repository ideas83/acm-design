import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addsession',
  templateUrl: './addsession.component.html',
  styleUrls: ['./addsession.component.scss']
})
export class AddsessionComponent implements OnInit {

  public session = {
    title : null,
    start_date : null,
    end_date : null,
    photo:null,
    notes:null,
    event_id:1,
    client_id:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  sessionForm: FormGroup;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.sessionForm = new FormGroup({
      title: new FormControl(),
      start_date: new FormControl(),
      end_date: new FormControl(),
      photo: new FormControl(),
      notes: new FormControl(),
      event_id: new FormControl(1),
      client_id: new FormControl(1),
   });
  }
  addsession()
  {
    this.session = this.sessionForm.value;
    this.dataService.addsession(this.session).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/sessions');
    })
    
  }

}
