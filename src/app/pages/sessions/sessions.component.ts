import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  arrsessions: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      title: {
        title: 'Title',
        type: 'string',
      },
      start_date: {
        title: 'Start Date',
        type: 'string',
      },
      end_date: {
        title: 'End Date',
        type: 'number',
      },
      notes: {
        title: 'Description',
        type: 'number',
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }
   ngOnInit(): void {
    this.showsessions(this.userDisplayID);
  }
  showsessions(userDisplayID){
   
    this.dataService.showsessions( this.userDisplayID).subscribe(res=>{
      this.arrsessions = res;
	  console.log(this.arrsessions);
  })
  } 
  updatesession(event) {

    var data = {"title": event.newData.title,
                "start_date" : event.newData.start_date,
                "end_date" : event.newData.end_date,
                "notes" : event.newData.notes,
               
				"event_id":"1",
				"client_id":"1"
                };
                console.log(data);
  return this.http.post('https://server.kodeshode.com/api/updatesession/'+event.newData.id, data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.showsessions(this.userDisplayID);
      });
  }
  deletesession(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('https://server.kodeshode.com/api/deletesession/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showsessions(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
   
 }
}
