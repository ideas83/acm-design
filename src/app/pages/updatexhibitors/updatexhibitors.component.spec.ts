import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatexhibitorsComponent } from './updatexhibitors.component';

describe('UpdatexhibitorsComponent', () => {
  let component: UpdatexhibitorsComponent;
  let fixture: ComponentFixture<UpdatexhibitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatexhibitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatexhibitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
