import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-updatelanding',
  templateUrl: './updatelanding.component.html',
  styleUrls: ['./updatelanding.component.scss']
})
export class UpdatelandingComponent implements OnInit {

  public landing = {
    description : null,
    event_image : null,
    org_id: null,
    event_id: null,
    managerid: null
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  landingForm: FormGroup;
  files: any;
  id:any;
  data: any;
  constructor(private route:ActivatedRoute,private dataService:DataService,private router: Router, private http:HttpClient) { }

  ngOnInit(): void {
    this.id= this.route.snapshot.params.id;
    this.landingForm = new FormGroup({
      description : new FormControl(),
      event_image : new FormControl(),
      org_id: new FormControl(1),
      event_id: new FormControl(1),
      managerid: new FormControl(1),
   });
    this.singlelandingpage();
  }
  singlelandingpage()
  {

   
   // formdata.append("data",JSON.stringify(this.landing));
    this.dataService.singlelandingpage(this.id).subscribe(res=>{
        this.data = res;
        console.log(this.data);
        this.landing.description = this.data.description;
        console.log(this.landing.description);
        //return this.router.navigateByUrl('/pages/landingpage');
    })
     
  }
  imageupload(event){
    this.files=event.target.files[0];
    this.landingForm.get('event_image').setValue(this.files);
}

updatelandingpage()
  {
    //this.landing = this.landingForm.value;
    let formdata = new FormData;
    formdata.append("event_image",this.landingForm.get('event_image').value);
    formdata.append("description",this.landingForm.get('description').value);
    formdata.append("org_id",this.landingForm.get('org_id').value);
    formdata.append("event_id",this.landingForm.get('event_id').value);
    formdata.append("managerid",this.landingForm.get('managerid').value);
    this.dataService.updatelandingpage(this.id,formdata).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/landingpage');
    }) 
    console.log(this.data);
    console.log(this.id);
    
  }
}

