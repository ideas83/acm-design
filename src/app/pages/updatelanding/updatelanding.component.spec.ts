import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatelandingComponent } from './updatelanding.component';

describe('UpdatelandingComponent', () => {
  let component: UpdatelandingComponent;
  let fixture: ComponentFixture<UpdatelandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatelandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatelandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
