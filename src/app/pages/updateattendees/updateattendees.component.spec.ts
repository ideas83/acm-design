import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateattendeesComponent } from './updateattendees.component';

describe('UpdateattendeesComponent', () => {
  let component: UpdateattendeesComponent;
  let fixture: ComponentFixture<UpdateattendeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateattendeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateattendeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
