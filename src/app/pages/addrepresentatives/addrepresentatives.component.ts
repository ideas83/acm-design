import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addrepresentatives',
  templateUrl: './addrepresentatives.component.html',
  styleUrls: ['./addrepresentatives.component.scss']
})
export class AddrepresentativesComponent implements OnInit {

  public representative = {
    name : null,
    email : null,
    phone_number:null,
    exibit_id:null,
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  representativeForm: FormGroup;
  files: any;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.representativeForm = new FormGroup({
     
      name : new FormControl(),
      email : new FormControl(),
      phone_number:new FormControl(),
      exibit_id:new FormControl(),
   });
  }
  addrepresentative()
  {

    this.representative = this.representativeForm.value;
    this.dataService.addrepresentative(this.representative).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/exhibitors');
    })
     
  }

}
